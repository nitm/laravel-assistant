# Assistant Interaction package for Laravel 5.6+

# Installation

Install this package by adding it as a custom repository to your composer.json:

    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:nitm/laravel-assistant.git"
        },
        ...
    ],
    ...

Next require the interaction package

    composer require nitm/laravel-assistant "*"

# Publishing templates and core files

## Publishing base files to your application

    php artisan vendor:publish --provider="Nitm\Assistant\ServiceProvider" --tag="assistant"

## Publishing config to your application

    php artisan vendor:publish --provider="Nitm\Assistant\ServiceProvider" --tag="config"

## Publishing database files to your application

    php artisan vendor:publish --provider="Nitm\Assistant\ServiceProvider" --tag="database"

# Customizing the behavior

What you build is entirely dependent on your needs. This package provides a simplified way to interacting, responding to and adding functionality for AI based assistants to your application

# Extending providers and interactions

You may extend base providers, interactions and responses according to your needs

## Dialogflow setup

Make sure to get your Service Account credentials for Dialogflow and update your `.env` file by setting the `GOOGLE_SERVICE_KEY` value. See instructions here: <https://cloud.google.com/docs/authentication/production>.

You may also need to add the following to the `config/services.php` file:

```
    'dialogFlow' => [
      'serviceKeyPath' => env('GOOGLE_SERVICE_KEY', base_path().'/.private/google-service-key.json')
    ]
```

# Activity Information
You can view activity information for the assistant. By default this is enabled using the 'generic' view in the config. You can enable the Spark UI by setting the `ui` config key to 'spark'.

## UI Route
You can enable the generic activity UI by adding a link to the following route.

```
/assistant/activity
```
