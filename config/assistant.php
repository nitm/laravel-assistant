<?php

/**
 * Configuration options for the assistant
 * @var array
 */
return [
    /**
     * Enable this to only allow existing users to use the app
     */
    'onlyAllowExistingUsers' => env('ASSISTANT_ONLY_EXISTING_USERS', false),

    /**
     * For handling followup questions in situations where the interaction provider doesn't support followup responses
     */
    'simulateFollowUp' => env('ASSISTANT_SIMULATE_FOLLOWUP', false),

    /**
     * The provdier to use for the follow up response
     */
    'simulateFollowUpWith' => env('ASSISTANT_SIMULATE_FOLLOWUP_WITH', null),

    /**
     * The simulated followup delay in seconds
     */
    'simulateFollowUpDelay' => env('ASSISTANT_SIMULATE_FOLLOWUP_DELAY', 5),

    /**
     * The random user to use in cases where we cannot find the current user in dev mode
     */
    'userWhenNoneFound' => env('ASSISTANT_DEFALT_USER', 'user@app.local'),

    /**
     * Should the app respond through the interpreter?
     * This is useful for handling webhook calls
     */
    'respondThroughInterpreter' => env('ASSISTANT_RESPOND_THROUGH_INTERPRETER', true),

    /**
     * The handler to use for response parsing
     * One of: [dialogFlow, twilio, vonage]
     * @var string
     */
    'interpreter' => env('ASSISTANT_INTERPRETER', env('ASSISTANT_PROVIDER')),

    /**
     * The provider to use for interactons
     * One of: [twilio, vonage]
     * @var string
     */
    'provider' => env('ASSISTANT_PROVIDER'),

    /**
     * THe provider to use for initiating transactions
     * @var string
     */
    'queryProvider' => env('ASSISTANT_INTERPRETER', env('ASSISTANT_PROVIDER')),

    /**
     * The messaging provider to use when sending and receiving messges
     */
    'messagingPovider' => env('ASSISTANT_MESSAGING_PROVIDER', env('ASSISTANT_PROVIDER')),

    /**
     * Fulfilment providers
     */
    'fulfillment' => [
        'dialogflow' => true
    ],

    /**
     * How many interactions should there be perr session
     * @var integer
     */
    'interactionsPerSession' => env('ASSISTANT_INTERACTIONS_PER_SESSION', 5),

    /**
     * How many interactions should there be per day?
     * @var integer
     */
    "dailySessions" => env('ASSISTANT_DAILY_SESSIONs', 3),

    /**
     * The earliest time the assistant can start interacting
     */
    'startTime' => env('ASSISTANT_START_TIME', 0),

    /**
     * The latest time the assistant can start interacting
     */
    'endTime' => env('ASSISTANT_START_TIME', 23),

    /**
     * Break up thegroups into chunks of this size
     */
    'groupCount' => env('ASSISTANT_GROUP_COUNT', 20),

    /**
     * The amount of hours wiith which a goal is considered due soon
     */
    'dueSoonHourDiff' => env('ASSISTANT_DUE_SOON_HOUR_DIFF', 48),

    /**
     * The amount of time for gaal to be considered near completion
     */
    'dueNearCompletionHourDiff' => env('ASSISTANT_DUE_NEAR_COMPLETION_HOUR_DIFF', 24),

    /**
     * Countries supported by the assistant
     */
    'supportedCountries' => ['US'],

    /**
     * The assistant class to use
     */
    'assistantClass' => env('ASSISTANT_ASSISTANT_CLASS', 'App\Assistant\Assistant'),

    /**
     * The user class to use
     */
    'userClass' => env('ASSISTANT_USER_CLASS', 'App\User'),

    /**
     * The UI to enable
     * One of ['spark', 'generic']
     * Use spark for Laravel Spark and generic ofr a standard HTML UI
     */
    'ui' => env('ASSISTANT_UI', 'spark'),
];
