<?php

/**
* Router for api  assistant callback
* @var [type]
*/

Route::group([
    'prefix' => 'assistant',
], function () {
    $ui = config('assistant.ui', 'generic');
    if($ui != 'voyager') {
      $controller = $ui === 'spark' ?  'AssistantActivitySparkController' : 'AssistantActivityController';

      Route::get('/activity/all', 'Nitm\Assistant\Http\Controllers\\'.$controller.'@all');

      Route::resource(
        'activity',
        'Nitm\\Assistant\\Http\\Controllers\\'.$controller
      )->only(['index', 'show'])
      ->middleware('web');
    }

    if(config('assistant.fulfillment.dialogflow') === true) {
      Route::post(
          'dialogflow',
          'Nitm\Assistant\Http\Controllers\API\AssistantAPIController@store',
          [ 'only' => 'create']
      )->name("dialogflow.assistant");
    }
    
    Route::post('agreement/{type}/{id}', 'Nitm\Assistant\Http\Controllers\API\AssistantAPIController@agreement');
});
