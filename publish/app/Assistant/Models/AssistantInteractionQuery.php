<?php

namespace App\Assistant\Models;

/**
 * The base assistant class used to generate and interact with users
 */
class AssistantInteractionQuery extends \Nitm\Assistant\Models\AssistantInteractionQuery
{
}
