<?php

namespace App\Assistant\Models;

/**
 * The base assistant class used to generate and interact with users
 */
class AssistantSession extends \Nitm\Assistant\Models\AssistantSession
{
}
