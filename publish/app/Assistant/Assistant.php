<?php

namespace App\Assistant;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticable;
use Illuminate\Eloquent\Collection;
use Illuminate\Foundation\Auth\User;
use App\Models\AssistantSession;
use App\Models\AssistantInteraction;
use App\Models\Assistant as AssistantModel;
use App\Models\Goal;
use App\Models\Survey;
use App\Models\Question;
use App\Models\Answer;
use App\Jobs\ProcessAssistantInteraction as Job;

/**
 * The base assistant class used to generate and interact with users
 */
class Assistant extends \Nitm\Assistant\Assistant
{
    /**
     * Custom business logic for handling the current interaction
     *
     * @param [type] $interaction
     * @param [type] $provider
     * @param array $params
     * @return void
     */
    public function handleCustom($interaction = NULL, $provider = NULL, $params = []) {
        /**
         * Business logic to handle custom interactions
         * Remove the error below to continue
         */
        throw new \Error("You need to implement the custom handler");

        $interaction = $interaction = $interaction ?: $this->getCurrentInteraction();
        if ($interaction instanceof AssistantInteraction) {
            $provider = $provider ?? $this->getProvider();
            $result = [];

            /**
             * Parse the request and extract relavant information using the response $provider
             */
            $params = array_merge($params, $provider->process());

            /**
             * Find the latest sessions that hasn't been completed for the number in the
             * Rmove the placeholder information below and implement the custom logic that works for you
             */
            // $id = array_get($params, 'id', -1);
            // $attributes = array_pull($params, 'attributes', []);
            // switch ($params['action']) {
            //     case static::CONST:
            //         $result = $this->handleConst($attributes);
            //         break;

            //     /**
            //      * Most likely an unknown input
            //      * @var [type]
            //      */
            //     default:
            //         $result = [];
            //         break;
            // }

            return $result;
        } else {
            return [];
        }
    }

    /**
     * Custom interaction routing
     *
     * @param User $user
     * @param [type] $interaction
     * @param array $params
     * @return array
     */
    public function interactWithCustom($user, $interaction, $params=[]) {
        /**
         * Business logic to handle intent happens here;
         * Remove the error below to continue
         */
        throw new \Error("You need to implement the custom interaction handler");

        $interaction = $interaction ?? $this->getCurrentInteraction($user);
        ;
        $attributes = [];
        $params = $this->parseInteraction($interaction->action);
        extract($params);

        // switch ($interaction->item_type) {
        //     case Type:
        //         switch ($interaction->action) {
        //             case static::CONST:
        //                 switch ($interaction->action) {
        //                     case static::CONST2:
        //                         $messageKey = 'message-string';
        //                         break;
        //                 }
        //                 $attributes = [
        //                     'name' => $this->getUser()->name,
        //                 ];
        //                 $message = __($messageKey, $attributes);
        //                 break;
        //         }
        // }
        return [$message, $attributes, $action, $intent];
    }

    /**
     * Parse the interaction based on the custom business logic
     *
     * @param string $action
     * @return array
     */
    public function parseInteraction(string $action) {
        $message = $intent = $messageKey = null;
        $attributes = [];
        $interaction = $this->getCurrentInteraction();

        /**
         * Business logic to handle interaction intent happens here;
         * Remove the error below to continue
         */
        throw new \Error("You need to implement the custom interaction parser");

        // switch ($action) {
        //     case static::ACTION_CONST:
        //         $attributes = [
        //             'name' => $this->getUser()->name,
        //         ];
        //         $message = $messageKey ? __($messageKey, $attributes) : null;
        //         break;
        // }

        return [
            'action' => $action,
            'intent' => $intent,
            'attributes' => $attributes,
            'message' => $message
        ];
    }

    protected function continueSession(): array
    {
        return [];
    }

    protected function completeSession(): array
    {
        /**
         * TODO: Determine if we should send a response here
         * @var [type]
         */
        $this->getSession()->complete();
        return [];
    }

    protected function completeInteraction(): array
    {
        /**
         * TODO: Determine if we should send a response here
         * @var [type]
         */
        if ($this->getCurrentInteraction()) {
            $this->getCurrentInteraction()->complete();
            unset($this->interaction);
            if ($this->getCurrentInteraction()) {
                $this->nextInteraction = $this->interaction;
            }
        }

        $next = $this->getCurrentInteraction();

        if ($next instanceof AssistantInteraction) {
            return [
                'interaction ' => $next
            ];
        } else {
            return [
                'endSession' => true
            ];
        }
    }
}
