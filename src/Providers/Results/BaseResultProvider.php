<?php

namespace Nitm\Assistant\Providers\Results;

use Nitm\Assistant\AssistantObject;
use Nitm\Assistant\Contracts\Result;

/**
 * [Description BaseResponseProvider]
 */
abstract class BaseResultProvider extends AssistantObject implements Result
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $to;

    /**
     * @var string
     */
    public $from;

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $error;

    /**
     * @var string
     */
    public $errorCode;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $accountId;

    /**
     * @var string
     */
    public $serviceId;

    /**
     * @var string
     */
    public $sessionId;

    /**
     * @var array
     */
    public $payload = [];

    /**
     * @var array
     */
    protected $queryParams = [];

    /**
     * @var array
     */
    protected $localParams = [];

    /**
     * The original response
     */
    protected $original;

    /**
     * The user for this response
     * @var [type]
     */
    protected $user;

    /**
     * @param string $message
     * @param array $attributes
     *
     * @return [type]
     */
    public function prepare($message = '', $attributes = [])
    {
        $response = array_map(function ($attribute) {
            return $this->$attribute;
        }, static::responseMap());

        $this->queryParams = array_filter(array_combine(static::responseMap(), $response));

        $this->localParams = $attributes;
    }

    /**
     * @param mixed $key
     *
     * @return [type]
     */
    public function get($key)
    {
        return $this->__get($key);
    }

    /**
     * @return mixed
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * Get the raw response
     * @return array
     */
    public function getResponse()
    {
        return $this->result;
    }

    /**
     * Get the local parameters
     * @return array
     */
    public function getLocalParams()
    {
        return $this->localParams;
    }

    /**
     * Get the query parameters
     * @return array
     */
    public function getQueryParams()
    {
        return $this->queryParams;
    }

    /**
     * Get the message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get the media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Get the to address
     *
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Get the from address
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Get the to id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the error
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Get the error code
     *
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * Get the status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the status
     *
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Get the status
     *
     * @return string
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Append a string to the end of the current message
     *
     * @param string $message
     * @param string $glue
     *
     * @return string
     */
    public function appendMessage(string $message, $glue = '.'): string
    {
        $this->setMessage("{$this->getMessage()}$glue $message");
        return $this->getMessage();
    }
}
