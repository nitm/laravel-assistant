<?php

namespace Nitm\Assistant\Providers\Interpreters;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Nitm\Assistant\Providers\Interpreters\BaseInterpretationProvider;

class TwilioInterpretation extends BaseInterpretationProvider
{
    /**
     * @var [type]
     */
    public $from;

    /**
     * @var [type]
     */
    public $messageSid;

    /**
     * @var [type]
     */
    public $input;

    /**
     * @var array
     */
    protected $userIdSources = [
        'from',
        'From'
    ];

    /**
     * @return [type]
     */
    public function getProvider()
    {
        return $this->assistant->loadProvider(
            'twilio', [
            'interpreter' => $this
            ]
        );
    }

    /**
     * @param bool $filter
     *
     * @return [type]
     */
    public function getParsedData($filter = false)
    {
        $data = array_merge(
            $this->original, [
            'id' => $this->id,
            'to' => $this->to,
            'from' => $this->from,
            'message' => $this->message,
            'accountId' => $this->accountId,
            'errorCode' => $this->errorCode,
            'error' => $this->error,
            'status' => $this->status,
            'serviceId' => $this->serviceId
            ]
        );

        $data =  $filter ? array_filter($data) : $data;

        return $this->getProvider()->newResponse($data);
    }

    /**
     * Parse the request
     *
     * @return [type] [description]
     */
    public function parse(Request $request = null)
    {
        $request = $request ?? $this->request;
        $this->id = $request->input('sid') ?? $request->input('SmsSid');
        $this->to = $request->input('to') ?? $request->input('To');
        $this->from = $request->input('from') ?? $request->input('From');
        $this->message = $request->input('body') ?? $request->input('Body');
        $this->accountId = $request->input('account_sid') ?? $request->input('AccountSid');
        $this->errorCode = $request->input('error_code') ?? $request->input('ErrorCode');
        $this->error = $request->input('error_message') ?? $request->input('ErrorMessage');
        $this->serviceId = $request->input('messaging_service_sid') ?? $request->input('MessageSid');
        $this->status = $request->input('status') ?? $request->input('Status') ?? $request->input('SmsStatus');
        $this->original = $request->all();
        foreach ($this->original as $k => $v) {
            $this->$k = $v;
        }
        return $this->getParsedData();
    }

    /**
     * Find the user
     *
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function findUser($data = null)
    {
        $class = config('assistant.userClass', 'App\User');
        $data  = $data ?: $this->request->all();
        if (is_array($data) && !empty($data)) {
            $params = $this->getUserQueryParams($data);
            if ($params['phone']) {
                return $class::where($params)->first();
            }
        }
    }

    /**
     * @param array $data
     *
     * @return [type]
     */
    public function getUserQueryParams($data = null): array
    {
        $data = empty($data) ? $this->getParsedData(true) : $data;
        $data = empty($data) ? $this->request->input() : $data;

        $phone = $this->from ?: Arr::get(array_change_key_case($data), "from");
        return ['phone' => phone($phone, config('assistant.supportedCountries'), 'e164')];
    }
}