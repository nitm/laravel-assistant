<?php

namespace Nitm\Assistant\Providers\Interpreters;

use Illuminate\Http\Request;
use Nitm\Assistant\AssistantObject;
use Nitm\Assistant\Contracts\Assistant;
use Nitm\Assistant\Contracts\Interaction;
use Nitm\Assistant\Contracts\Interpretation;
use Nitm\Assistant\Providers\Responses\BaseResponseProvider;

/**
 * [Description BaseInterpretationProvider]
 */
abstract class BaseInterpretationProvider extends AssistantObject implements Interpretation
{
    /**
     * The message id
     *
     * @var string
     */
    public $id;

    /**
     * The to value
     *
     * @var string
     */
    public $to;

    /**
     * The from value
     *
     * @var string
     */
    public $from;

    /**
     * @var string
     */
    public $error;

    /**
     * @var string
     */
    public $errorCode;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $accountId;

    /**
     * @var string
     */
    public $serviceId;

    /**
     * @var string
     */
    public $sessionId;

    /**
     * The action
     *
     * @var string
     */
    public $action;

    /**
     * The interaction
     *
     * @var AssistantInteraction
     */
    protected $interaction;
    /**
     * @var [type]
     */
    public $original;

    /**
     * Is this a webhook call?
     *
     * @var boolean
     */
    protected $isWebhookCall = true;

    /**
     * @param array          $attributes
     * @param Assistant|null $assistant
     * @param Request|null   $request
     */
    public function __construct($attributes = [], Assistant $assistant = null, Request $request = null)
    {
        $this->request = $request ?? $this->request ?? request();
        parent::__construct($attributes, $assistant);
    }

    /**
     * Run the interaction and return the response
     *
     * @return BaseResponseProvider
     */
    public function run()
    {
        $this->process();
        return $this->getResponse();
    }

    /**
     * @return Interaction
     */
    public function getInteraction(): Interaction
    {
        return $this->interaction;
    }

    /**
     * @param mixed $value
     *
     * @return [type]
     */
    public function setIsWebhookCall($value)
    {
        $this->isWebhookCall = $value === true ? true : false;
    }

    /**
     * @return array
     */
    public function getUserQueryParams($data = null): array
    {
        return $data ?? ['email' => 'assistant@app.local'];
    }
}