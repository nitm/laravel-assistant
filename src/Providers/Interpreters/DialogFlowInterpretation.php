<?php

namespace Nitm\Assistant\Providers\Interpreters;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Nitm\Assistant\Providers\Interpreters\BaseInterpretationProvider;

class DialogFlowInterpretation extends BaseInterpretationProvider
{
    /**
     * The project id for the DialogFlow account
     *
     * @var string
     */
    public $projectId;

    /**
     * THe unique response ID
     *
     * @var string
     */
    public $responseId;

    /**
     * The session ID for thisteraction
     *
     * @var string
     */
    public $sessionId;

    /**
     * @var [type]
     */
    public $query;

    /**
     * @var [type]
     */
    public $intent;

    /**
     * @var [type]
     */
    public $queryText;

    /**
     * @var [type]
     */
    public $fulfillmentText;

    /**
     * @var [type]
     */
    public $fulfillmentMessages;

    /**
     * @var [type]
     */
    public $payload;

    /**
     * @var [type]
     */
    public $parameters;

    /**
     * @var [type]
     */
    public $originalDetectIntentRequest;

    /**
     * @var [type]
     */
    public $outputContexts;

    protected $userIdSources = [
        'queryResult.parameters.from',
        'outputContexts.0.parameters.twilio_sender_id',
        'originalDetectIntentRequest.payload.From',
        'originalDetectIntentRequest.payload.twilio_sender_id',
    ];

    /**
     * @var [type]
     */
    private $clientFormattedSession;

    /**
     * @var [type]
     */
    private $clientSessionId;

    /**
     * @var [type]
     */
    private $cloud;

    /**
     * @return [type]
     */
    public function getProvider()
    {
        return $this->assistant->loadProvider(
            'dialogFlow', [
            'interpreter' => $this
            ]
        );
    }

    /**
     * @return [type]
     */
    public function getClientSessionId()
    {
        if (!isset($this->clientSessionId)) {
            $this->clientSessionId = implode(
                '-', [
                $this->getUser()->username ?: md5($this->getUser()->email),
                $this->interaction->action,
                Carbon::now()->format('Y-m-d-H')
                ]
            );
        }
        return $this->clientSessionId;
    }

    /**
     * Find the user
     *
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function findUser($data = null)
    {
        $class = config('assistant.userClass', 'App\User');
        $data = $data ?: $this->parsedData();
        $data = empty($data) ? $this->request->input() : $data;
        if (is_array($data)) {
            // This is where third party fulfillment information is passed
            $originalRequest = Arr::get($data, "originalDetectIntentRequest");
            $source = Arr::get($originalRequest, "source");
            if ($this->assistant->hasInterpreter($source)) {
                return $this->assistant->loadInterpreter($source)->findUser(Arr::get($originalRequest, "payload.data"));
            } else {
                $where = $this->getUserQueryParams($data);
                //  Try a secondary potential location
                if ($phone) {
                    return $class::where($where)->first();
                }
            }
        }
    }

    /**
     * @param Request|null $request
     *
     * @return [type]
     */
    public function parse(Request $request = null)
    {
        $this->request = $request ?: $this->request;
        $this->responseId = $this->request->input('responseId');
        $this->sessionId = $this->request->input('session');
        $this->payload = $this->request->input('payload');
        $this->intent = $this->request->input('queryResult.intent');
        $this->action = $this->request->input('queryResult.action');
        $this->queryText = $this->request->input('queryResult.queryText');
        $this->fulfillmentText = $this->request->input('queryResult.fulfillmentText');
        $this->fulfillmentMessages = $this->request->input('queryResult.fulfillmentMessages');
        $this->outputContexts = $this->request->input('queryResult.outputContexts');
        $this->originalDetectIntentRequest = $this->request->input('originalDetectIntentRequest');
        $this->payload = $this->payload ?? Arr::get($this->originalDetectIntentRequest, 'payload');
        $this->parameters = array_merge(
            (array)$this->request->input('queryResult.parameters', []),
            (array)Arr::get($this->outputContexts, '0.parameters', [])
        );
        if (is_array($this->outputContexts) && count($this->outputContexts)) {
            $firstContext = current($this->outputContexts);
            $parts = explode('/', $firstContext['name']);
            $this->projectId = array_pop($parts);
        }

        return $this->getParsedData();
    }

    /**
     * @param bool $filter
     *
     * @return [type]
     */
    public function getParsedData($filter = false)
    {
        $data = [
            'queryText' => $this->queryText,
            'responseId' => $this->responseId,
            'sessionId' => $this->sessionId,
            'projectId' => $this->projectId,
            'payload' => $this->payload,
            'parameters' => $this->parameters,
            'action' => $this->action,
            'fulfillmentMessages' => $this->fulfillmentMessages,
            'fulfillmentText' => $this->fulfillmentText,
            'outputContexts' => $this->outputContexts,
            'originalDetectIntentRequest' => $this->originalDetectIntentRequest,
            'intent' => $this->intent
        ];

        return $this->getProvider()->newResponse($data);
    }

    /**
     * [getSessionId description]
     *
     * @return string [description]
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * Format an output context for Dialogflow
     *
     * @param  [type] $params [description]
     * @return array         [description]
     */
    public function getOutputContext(array $params)
    {
        $parameters = Arr::get($params, 'parameters', $params);
        $lifespanCount = Arr::get($params, 'lifespanCount', 5);
        $name = Arr::pull($params, 'name');
        if (!$name) {
            return null;
        }
        $name = strtolower($name);
        $isWebhookCall = preg_match('/projects\/([\-\_\w]+)\/agent\/sessions\/([\-\_\w]+)/', $this->sessionId) > 0;
        if ($this->isWebhookCall || $isWebhookCall) {
            return [
                // The name needs the project ID and session ID
                'name' => str_replace(
                    [
                    '{SESSION_ID}',
                    '{CONTEXT_NAME}'
                    ], [
                    $this->sessionId,
                    $name
                    ], '{SESSION_ID}/contexts/{CONTEXT_NAME}'
                ),
                'lifespanCount' => $lifespanCount,
                'parameters' => (array)$parameters
            ];
        } else {
            return [
                // The name needs the project ID and session ID
                'name' => str_replace(
                    [
                    '{PROJECT_ID}',
                    '{SESSION_ID}',
                    '{CONTEXT_NAME}'
                    ], [
                    $this->projectId,
                    $this->sessionId,
                    $name
                    ], 'projects/{PROJECT_ID}/agent/sessions/{SESSION_ID}/contexts/{CONTEXT_NAME}'
                ),
                'lifespanCount' => $lifespanCount,
                'parameters' => (array)$parameters
            ];
        }
    }

    /**
     * Format followup event input for Dialogflow
     *
     * @param  string $eventName the followup event
     * @param  array  $params    [description]
     * @return array         [description]
     */
    public function getFollowupEventInput($eventName, $params = [])
    {
        if (!$eventName) {
            return null;
        }
        return [
            'name' => $eventName,
            'parameters' => $params
        ];
    }

    /**
     * @param array $data
     *
     * @return [type]
     */
    public function getUserQueryParams($data = null): array
    {
        $data = empty($data) ? $this->getParsedData(true) : $data;
        $data = empty($data) ? $this->request->input() : $data;
        /**
         * Check for a phone umber
         */
        $contexts = $this->outputContexts ?: Arr::get($data, 'queryResult.outputContexts');
        $context = $contexts[0];
        $key = 'email';
        $value = Arr::get($context, 'parameters.' . Assistant::USER_CONTEXT . '.email', Arr::get($data, "originalDetectIntentRequest.payload.data.email"));
        /**
         * If empty check for an email
         */
        if (!$value) {
            $key = 'phone';
            $value = Arr::get($context, 'parameters.' . Assistant::USER_CONTEXT . '.phone', Arr::get($data, "originalDetectIntentRequest.payload.data.From"));
            $value = strlen($value) ? phone($value, config('assistant.supportedCountries'), 'e164') : '';
        }
        return [$key => $value];
    }
}