<?php

namespace Nitm\Assistant\Providers\Interpreters;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Nitm\Assistant\Providers\Interpreters\BaseInterpretationProvider;

class VonageInterpretation extends BaseInterpretationProvider
{
    /**
     * @var array
     */
    protected $userIdSources = [
        'from',
        'From'
    ];

    /**
     * @return [type]
     */
    public function getProvider()
    {
        return $this->assistant->loadProvider(
            'vonage', [
            'interpreter' => $this
            ]
        );
    }

    /**
     * @param bool $filter
     *
     * @return [type]
     */
    public function getParsedData($filter = false)
    {
        $data = array_merge(
            $this->original, [
            'id' => $this->id,
            'accountId' => $this->accountId,
            'serviceId' => $this->serviceId,
            'to' => $this->to,
            'from' => $this->from,
            'message' => $this->message,
            'media' => $this->media,
            'errorCode' => $this->errorCode,
            'error' => $this->error,
            'status' => $this->status,
            ]
        );

        $data =  $filter ? array_filter($data) : $data;

        return $this->getProvider()->newResponse($data);
    }

    /**
     * Parse the request
     *
     * @return [type] [description]
     */
    public function parse(Request $request = null)
    {
        $request = $request ?? $this->request;
        $this->id = $request->input('messageId') ?? $request->input('timestamp') ?? $request->input('Sid') ?? $request->input('SmsSid') ?? $request->input('MessageSid');
        $this->serviceId = $request->input('messageId') ?? $request->input('MessageSid');
        $this->accountId = $request->input('account_sid') ?? $request->input('AccountSid');
        $this->to = $request->input('to') ?? $request->input('To');
        $this->from = $request->input('msisdn') ?? $request->input('from') ?? $request->input('From');
        $this->message = $request->input('text') ??  $request->input('message') ??  $request->input('Message') ??  $request->input('body') ??  $request->input('Body');
        $this->media = $request->input('type') == 'image' ? $request->input('message.content.image.url') : null;
        $this->errorCode = $request->input('err-code');
        $this->error = $request->input('error.reason');
        $this->status = $request->input('status') ?? $request->input('Status') ?? $request->input('SmsStatus');
        $this->original = $request->all();
        foreach ($this->original as $k => $v) {
            $this->$k = $v;
        }
        return $this->getParsedData();
    }

    /**
     * Get Error Meaning
     *
     * @url https://developer.nexmo.com/messaging/sms/guides/delivery-receipts
     *
     * @param  mixed $key
     * @return string
     */
    protected function getErrorMeaning($key = 1): string
    {
        $messages = [
            "Delivered",
            "Unknown",
            "Absent Subscriber - Temporary",
            "Absent Subscriber - Permanent",
            "Call Barred by User",
            "Portability Error",
            "Anti-Spam Rejection",
            "Handset Busy",
            "Network Error",
            "Illegal Number",
            "Illegal Message",
            "Unroutable",
            "Destination Unreachable",
            "Subscriber Age Restriction",
            "Number Blocked by Carrier",
            "Prepaid Insufficient Funds",
            "Entity Filter",
            "Header Filter",
            "Content Filter",
            "Consent Filter",
            "Regulation Error",
            "General Error",
        ];
        return Arr::get($messages, $key, 'Unknown');
    }

    /**
     * Find the user
     *
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function findUser($data = null)
    {
        $class = config('assistant.userClass', 'Nitm\Assistant\Models\User');
        $data  = $data ?: $this->request->all();
        if (is_array($data) && !empty($data)) {
            $params = $this->getUserQueryParams($data);
            if ($params['phone']) {
                return $class::where($params)->first();
            }
        }
    }

    /**
     * @param array $data
     *
     * @return [type]
     */
    public function getUserQueryParams($data = null): array
    {
        $data = empty($data) ? $this->getParsedData(true) : $data;
        $data = empty($data) ? $this->request->input() : $data;

        $phone = $this->from ?: Arr::get(array_change_key_case($data), "from");
        return ['phone' => phone($phone, config('assistant.supportedCountries'), 'e164')];
    }
}