<?php

namespace Nitm\Assistant\Providers\Responses;

use Nitm\Assistant\AssistantObject;
use Nitm\Assistant\Contracts\Response;

/**
 * [Description BaseResponseProvider]
 */
abstract class BaseResponseProvider extends AssistantObject implements Response
{
    /**
     * The message id
     *
     * @var string
     */
    public $id;

    /**
     * The to value
     *
     * @var string
     */
    public $to;

    /**
     * The from value
     *
     * @var string
     */
    public $from;

    /**
     * @var string
     */
    public $error;

    /**
     * @var string
     */
    public $errorCode;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $accountId;

    /**
     * @var string
     */
    public $serviceId;

    /**
     * @var string
     */
    public $sessionId;

    /**
     * @var array
     */
    public $payload = [];

    /**
     * @var [type]
     */
    protected $result;

    /**
     * @var [type]
     */
    protected $interaction;

    /**
     * @var array
     */
    protected $queryParams = [];

    /**
     * @var array
     */
    protected $localParams = [];

    /**
     * The user for this response
     * @var [type]
     */
    protected $user;

    /**
     * @param string $message
     * @param array $attributes
     *
     * @return [type]
     */
    public function prepare($message = '', $attributes = [])
    {
        $response = array_map(function ($attribute) {
            return $this->$attribute;
        }, static::responseMap());

        $this->queryParams = array_filter(array_combine(static::responseMap(), $response));

        $this->localParams = $attributes;
    }

    /**
     * @param mixed $key
     *
     * @return [type]
     */
    public function get($key)
    {
        return $this->__get($key);
    }

    /**
     * Get the raw response
     * @return array
     */
    public function getResponse()
    {
        return $this->result;
    }

    /**
     * Get the local parameters
     * @return array
     */
    public function getLocalParams()
    {
        return $this->localParams;
    }

    /**
     * Get the query parameters
     * @return array
     */
    public function getQueryParams()
    {
        return $this->queryParams;
    }

    /**
     * Append a string to the end of the current message
     *
     * @param string $message
     * @param string $glue
     * @return string
     */
    public function appendMessage(string $message, $glue = '.'): string
    {
        $this->setMessage("{$this->getMessage()}$glue $message");
        return $this->getMessage();
    }
}
