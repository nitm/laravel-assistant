<?php

namespace Nitm\Assistant\Providers\Responses;

use Nitm\Assistant\Contracts\Result;
use Nitm\Assistant\Providers\Results\TwilioResult;
use Nitm\Assistant\Providers\Responses\BaseResponseProvider;

class TwilioResponse extends BaseResponseProvider
{
    /**
     * @var string
     */
    public $to;

    /**
     * @var string
     */
    public $message;

    /**
     * @var [type]
     */
    protected $result;

    /**
     * The type of data we expect to return for the response
     *
     * @return array
     */
    protected static function responseMap()
    {
        return [
            'to',
            'message',
        ];
    }

    /**
     * To Result
     *
     * @return Result
     */
    public function toResult(): Result
    {
        return new TwilioResult($this->toArray());
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @inheritDoc
     */
    public function setTo(string $to)
    {
        $this->to = $to;
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @inheritDoc
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }
}