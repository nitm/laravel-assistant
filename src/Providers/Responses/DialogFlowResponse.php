<?php

namespace Nitm\Assistant\Providers\Responses;

use Illuminate\Support\Arr;
use Nitm\Assistant\Contracts\Result;
use Nitm\Assistant\Providers\Results\DialogFlowResult;
use Nitm\Assistant\Providers\Responses\BaseResponseProvider;

class DialogFlowResponse extends BaseResponseProvider
{
    public $fulfillmentText;
    public $fulfillmentMessages = [];
    public $outputContexts = [];
    public $followupEventInput = [];
    public $payload = [];

    /**
     * The type of data we expect to return for the response
     *
     * @return array
     */
    protected static function responseMap(): array
    {
        return [
            'fulfillmentText',
            'fulfillmentMessages',
            'outputContexts',
            'followupEventInput',
            'payload'
        ];
    }

    /**
     * Need to return only non empty objects to DialogFlow
     *
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();
        return array_filter(
            $data, function ($prop) {
                return !empty($prop);
            }
        );
    }

    /**
     * To Result
     *
     * @return Result
     */
    public function toResult(): Result
    {
        return new DialogFlowResult($this->toArray());
    }

    /**
     * @inheritDoc
     */
    public function setTo(string $to)
    {
        return Arr::set($this->payload, 'data.From', $to);
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getTo(): string
    {
        return Arr::get($this->payload, 'data.From');
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): string
    {
        return $this->fulfillmentText;
    }

    /**
     * @inheritDoc
     */
    public function setMessage(string $message)
    {
        $this->fulfillmentText = $message;
    }
}