<?php

namespace Nitm\Assistant\Providers\Interactions;

use Twilio\Rest\Client;
use App\Models\SigningKey;
use Illuminate\Support\Arr;
use Nitm\Assistant\Contracts\Response;
use Twilio\Rest\Api\V2010\AccountContext;
use Nitm\Assistant\Contracts\Interpretation;
use Nitm\Assistant\Providers\Results\TwilioResult;
use Nitm\Assistant\Providers\Interactions\PKCVClient;
use Nitm\Assistant\Providers\Responses\TwilioResponse;
use Nitm\Assistant\Providers\Interpreters\TwilioInterpretation;
use Nitm\Assistant\Providers\Interactions\BaseInteractionProvider;
use Nitm\Assistant\Providers\Interpreters\BaseInterpretationProvider;

class TwilioInteraction extends BaseInteractionProvider
{
    /**
     * @var [type]
     */
    protected $query;

    /**
     * @var [type]
     */
    protected $intent;

    /**
     * @var [type]
     */
    protected $parameters;

    /**
     * @var [type]
     */
    protected $response;

    /**
     * @var BaseInterpretationProvider
     */
    protected $interpreter;

    /**
     * @var [type]
     */
    protected $input;

    /**
     * @param mixed $attributes
     *
     * @return [type]
     */
    public function newResponse($attributes = []): Response
    {
        return new TwilioResponse($attributes);
    }

    /**
     * @param string $message
     * @param bool   $force
     *
     * @return [type]
     */
    public function getResponse($message = '', $force = false)
    {
        if ($force === true || !isset($this->response)) {
            $this->response = app(TwilioResponse::class);
            $this->response->fill(
                [
                'to' => $this->user->phone,
                'message' => $message,
                'assistant' => $this->assistant,
                'user' => $this->user
                ]
            );
        }
        return $this->response;
    }

    /**
     * Get Interpreter
     *
     * @return Interpretation
     */
    public function getInterpreter()
    {
        if(!isset($this->interpreter)) {

            $this->interpreter =  new TwilioInterpretation([], $this->assistant);
        }

        return $this->interpreter;
    }

    /**
     * @param string $message
     * @param bool   $force
     *
     * @return [type]
     */
    public function getQuery($message = '', $force = false)
    {
        if ($force === true || !isset($this->response)) {
            $this->response = app(TwilioResponse::class);
            $this->response->fill(
                [
                'to' => phone($this->user->phone, config('assistant.supportedCountries'), 'e164'),
                'message' => $message,
                'assistant' => $this->assistant,
                'user' => $this->user
                ]
            );
        }
        return $this->response;
    }

    /**
     * Send a response to theAPI
     *
     * @return [type] [description]
     */
    public function send(Response $response = null)
    {
        $response = $response ?? $this->response;
        $sms = $this->getClient()->messages->create(
            $response->get('to'),
            [
                'from' => $response->from ?? config('services.twilio.from'),
                'body' => $response->get('message'),
                'mediaUrl' => $response->get('media')
            ]
        );

        $this->result = new TwilioResult(
            [
            "id" => $sms->sid,
            "to" => $sms->to,
            "from" => $sms->$request->format('html'),
            "message" => $sms->body,
            "accountId" => $sms->accountSid,
            "serviceId" => $sms->messagingServiceSid,
            'success' => $sms->errorMessage == null,
            'error' => $sms->errorMessage,
            'errorCode' => $sms->errorCode,
            "status" => $sms->status,
            'result' => $sms
            ]
        );

        return $this->result;
    }

    /**
     * Get the API client
     *
     * @return [type] [description]
     */
    public function getClient()
    {
        if (!isset($this->client)) {
            $accountSid = config('services.twilio.account_sid');
            $authToken = config('services.twilio.auth_token');
            if(!$accountSid || !$authToken) {
                throw new \Error("Unable to find credentials. Please confirm the VONAGE_API_PRIVATE_KEY and VONAGE_APPLICATION_ID environment variables are set.");
            }
            if (app()->environment('testing')) {
                $this->client = new Client($accountSid, $authToken);
            } else {
                if (app()->environment('dev')) {
                    $keyName = gethostname() . '/' . preg_replace("/[^A-Za-z0-9 ]/", '_', phpversion());
                } else {
                    $keyName = gethostname();
                }
                $signingKey = SigningKey::where('friendly_name', $keyName)->orwhereNotNull('secret')
                    ->orderBy('updated_at', 'desc')
                    ->first();
                if (!static::isValidSigningKey($signingKey)) {
                    $insecureClient = new Client($accountSid, $authToken);
                    $keys = $insecureClient->signingKeys->stream(100);
                    $secret = null;
                    while ($keys->valid()) {
                        $key = $keys->page->current();
                        if ($key->friendlyName == $keyName) {
                            $signingKey = $key;
                            $secret = SigningKey::firstOrCreate(
                                [
                                'sid' => $signingKey->sid,
                                'friendly_name' => $keyName
                                ]
                            );
                            break;
                        }
                        $keys->next();
                    }

                    if (!$signingKey) {
                        $signingKey = $insecureClient->newSigningKeys->create(
                            [
                            'friendlyName' => $keyName
                            ]
                        );

                        $secret = SigningKey::create(
                            [
                            'sid' => $signingKey->sid,
                            'secret' => $signingKey->secret,
                            'friendly_name' => $keyName
                            ]
                        );
                    }
                } else {
                    $secret = $signingKey;
                }

                $this->client = new Client($signingKey->sid, $secret->secret, $accountSid, null, null, new PKCVClient());
            }
        }
        return $this->client;
    }

    /**
     * Check to see if this is a valid signingkey
     *
     * @param mixed $key
     *
     * @return bool
     */
    protected static function isValidSigningKey($key): bool
    {
        try {
            if ($key instanceof SigningKey) {
                $accountSid = config('services.twilio.account_sid');
                $client = new Client($key->sid, $key->secret, $accountSid, null, null, new PKCVClient());
                return $client->getAccount() instanceof AccountContext;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Process the interaction
     *
     * @return array [description]
     */
    public function process(): array
    {
        $parsedInput = $this->interpreter->parse();

        $action = Arr::get($parsedInput, 'action') ?: $this->interaction->action;

        $params = $this->assistant->parseFromServer($action);
        extract($params);
        $parameters = Arr::get($parsedInput, 'parameters') ?: [];
        $attributes = array_merge($parameters, $attributes ?? []);

        // if ($intent) {
        //     $result = $this->assistant
        //         ->getInterpreter()
        //         ->query($parsedInput['input'], $intent, [
        //             Assistant::USER_CONTEXT.'.phone' => $user->phone
        //         ]);
        //     extract($result);
        // }

        return compact('user', 'action', 'message', 'messages', 'contexts', 'followUp', 'followUpEventName', 'payload', 'attributes');
    }

    /**
     * [formatResponse description]
     *
     * @param  array $result [description]
     * @param  array $params [description]
     * @return TwilioResponse         [description]
     */
    public function formatResponse(array $result, array $params): BaseResponseProvider
    {
        $this->getResponse()->message = $this->extractMessage(Arr::get($result, 'response'));
        return $this->response;
    }

    /**
     * [getSessionId description]
     *
     * @return string [description]
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * Get the activity object
     *
     * @return AssistantActivity
     */
    public function completeActivity()
    {
        $model = parent::getActivity();
        $interpreter = $this->assistant->getInterpreter();
        $model->fill(
            [
            'verb' => $this->getInteractionAction(),
            'remote_type' => $this->interaction ? $this->interaction->item_type : $this->contentType,
            'remote_id' => $this->interaction ? $this->interaction->item_id : $this->contentId,
            'object' => $this->content,
            'response' => $this->getResponse(),
            'input_contexts' => $interpreter->outputContexts,
            'output_contexts' => Arr::get($this->getResponse()->getQueryParams(), 'contexts'),
            'needs_handoff' => $this->needsHandoff
            ]
        );
        return $model;
    }
}