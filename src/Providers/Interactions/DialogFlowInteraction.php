<?php

namespace Nitm\Assistant\Providers\Interactions;

use Carbon\Carbon;
use Google\Protobuf\Value;
use Google\Protobuf\Struct;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Nitm\Assistant\Contracts\Response;
use Google\Cloud\Dialogflow\V2\Context;
use Google\Cloud\Dialogflow\V2\TextInput;
use Google\Cloud\Dialogflow\V2\EventInput;
use Google\Cloud\Dialogflow\V2\QueryInput;
use Nitm\Assistant\Contracts\Interpretation;
use Nitm\Assistant\Models\AssistantActivity;
use Google\Cloud\Dialogflow\V2\SessionsClient;
use Google\Cloud\Dialogflow\V2\QueryParameters;
use Nitm\Assistant\Providers\Queries\DialogFlowQuery;
use Nitm\Assistant\Providers\Results\DialogFlowResult;
use Nitm\Assistant\Providers\Responses\DialogFlowResponse;
use Nitm\Assistant\Providers\Interactions\BaseInteractionProvider;
use Nitm\Assistant\Providers\Interpreters\DialogFlowInterpretation;

class DialogFlowInteraction extends BaseInteractionProvider
{
    /**
     * @var [type]
     */
    protected $query;

    /**
     * @var BaseResponseProvider
     */
    protected $response;

    /**
     * @var [type]
     */
    private $clientFormattedSession;

    /**
     * @var [type]
     */
    private $clientSessionId;

    /**
     * @var [type]
     */
    private $cloud;

    /**
     * @return [type]
     */
    public function getClient()
    {
        if (!isset($this->client)) {
            $configPath = config('services.dialogFlow.serviceKeyPath');
            $params = json_decode(file_get_contents($configPath), true);

            // $this->cloud = new ServiceBuilder([
            //     'keyFile' => $params
            // ]);

            $this->client = new SessionsClient(
                [
                'credentials' => $params
                // 'credentialsLoader' => new ServiceAccountCredentials([
                //     'https://www.googleapis.com/auth/cloud-platform'
                // ], $params)
                ]
            );
            $interpreter = $this->assistant->getInterpreter();
            if ($this->interaction && $this->interaction->provider_session_id) {
                $this->clientFormattedSession = $this->interaction->provider_session_id;
            } elseif ($interpreter instanceof BaseInterpretationProvier && $interpreter->getSessionId()) {
                $this->clientFormattedSession = $this->getInterpreter()->getSessionId();
            } else {
                $this->clientFormattedSession = $this->client->sessionName($params['project_id'], $this->getClientSessionId());
            }

            if ($this->hasInteraction()) {
                $this->interaction->provider_session_id = $this->clientFormattedSession;
            }
        }
        return $this->client;
    }


    /**
     * @param mixed $attributes
     *
     * @return [type]
     */
    public function newResponse($attributes = []): Response
    {
        return new DialogFlowResponse($attributes);
    }

    /**
     * [getResponse description]
     *
     * @param  string $message [description]
     * @return [type]          [description]
     */
    public function getResponse($message = '', $force = false)
    {
        if ($force === true || !isset($this->response)) {
            $this->response = app(DialogFlowResponse::class);
            $this->response->fill(
                [
                'message' => $message,
                'assistant' => $this->assistant,
                'user' => $this->user,
                'intent' => $this->getInteractionAction(),
                'parameters' => $this->interaction
                ]
            );
        }
        return $this->response;
    }

    /**
     * Get Interpreter
     *
     * @return Interpretation
     */
    public function getInterpreter()
    {
        if(!isset($this->interpreter)) {

            $this->interpreter =  new DialogFlowInterpretation([], $this->assistant);
        }

        return $this->interpreter;
    }

    /**
     * [getQuery description]
     *
     * @param  string $message [description]
     * @param  string $force
     * @return [type]          [description]
     */
    public function getQuery($message = '', $force = false)
    {
        if ($force === true || !isset($this->query)) {
            $this->query = app(DialogFlowQuery::class);
            $this->query->fill(
                [
                'message' => $message,
                'user' => $this->user,
                'interaction' => $this->interaction
                ]
            );
        }
        return $this->query;
    }

    /**
     * @return [type]
     */
    protected function getClientSessionId()
    {
        if (!isset($this->clientSessionId)) {
            if ($this->hasInteraction() && strlen($this->interaction->provider_session_id)) {
                $this->clientSessionId = $this->interaction->provider_session_id;
            } else {
                $this->clientSessionId = implode(
                    '-', [
                    $this->getUser()->username ?: md5($this->getUser()->email),
                    $this->getInteractionAction(),
                    Carbon::now()->format('Y-m-d-H')
                    ]
                );
            }
        }
        return $this->clientSessionId;
    }

    /**
     * [query description]
     *
     * @param  [type] $input  [description]
     * @param  [type] $intent [description]
     * @param  array  $params [description]
     * @return [type]         [description]
     */
    public function query($input, $intent, $params = [], $useEvent = false)
    {
        $response = [];
        $params = collect((array)$params);
        $hasQueryParams = false;
        try {
            $input = $input ?: 'Start Session';
            $this->assistant->getInterpreter()->setIsWebhookCall(false);

            $struct = new Struct();
            // Convert params to Google Message value
            $structParams = $params->filter(
                function ($value, $key) {
                    return is_numeric($key);
                }
            );
            if ($structParams->count()) {
                foreach ($structParams as $key => $param) {
                    $structParams[$key] = (new Value)->setStringValue($param);
                }
                $struct->setFields(
                    array_merge(
                        $structParams, [
                        'any' => (new Value)->setStringValue($input)
                        ]
                    )
                );
            }

            $queryInput = new QueryInput;
            $queryParams = new QueryParameters;

            if ($useEvent) {
                $eventInput = (new EventInput)
                    ->setName(strtoupper(snake_case(str_replace(['-'], ['_'], $intent))))
                    ->setParameters($struct)
                    ->setLanguageCode($this->assistant->languageCode);
                $queryInput->setEvent($eventInput);
            } else {
                $textInput = (new TextInput())
                    ->setText($input)
                    ->setLanguageCode($this->assistant->languageCode);
                $queryInput->setText($textInput);
                if ($structParams->count()) {
                    $hasQueryParams = true;
                    $queryParams->setPayload($struct);
                }
            }

            $contexts = Arr::get($params, 'contexts');
            if (is_array($contexts)) {
                foreach ($contexts as $idx => $params) {
                    $context = new Context();
                    if (is_string($params)) {
                        $params = $this->assistant->getInterpreter()->getOutputContext(
                            [
                            'name' => $params,
                            'lifespanCount' => 1
                            ]
                        );
                    }
                    $contextName = Arr::get($params, 'name', $params);
                    if ($contextName) {
                        $context->setName($contextName);
                        $contextParameters = array_except(Arr::get((array)$params, 'parameters', []), ['lifespan', 'name']);
                        if (count($contextParameters)) {
                            $parameters = new Struct;
                            foreach ($contextParameters as $key => $param) {
                                $contextParameters[$key] = (new Value)->setStringValue($param);
                            }
                            $parameters->setFields($contextParameters);
                            $context->setParameters($parameters);
                        }
                        $contextLifespanCount = Arr::get((array)$params, 'lifespan', 1);
                        $context->setLifespanCount($contextLifespanCount);
                        $contexts[$idx] = $context;
                    }
                }
                $contexts = array_filter($contexts);
                if (!empty($contexts)) {
                    $hasQueryParams = true;
                    $queryParams->setContexts($contexts);
                }
            }

            /**
             * Manually check the intent of the text based on the given intent
             *
             * @var [type]
             */
            $client = $this->getClient();
            // print_r(json_decode($queryInput->serializeToJsonString(), true));
            // print_r(json_decode($queryParams->serializeToJsonString(), true));
            // exit;
            if ($hasQueryParams) {
                $detectIntentParams = [
                    'queryParams' => $queryParams
                ];
            } else {
                $detectIntentParams = [];
            }
            $response = $client->detectIntent($this->clientFormattedSession, $queryInput, $detectIntentParams);
        } finally {
            $this->getClient()->close();
        }

        // When protobuf coverts to json it leaves emptyvalues that cause json errors
        $json = str_replace([':,', ":}"], [': "",', ': ""}'], $response->serializeToJsonString());

        $parsedResult = json_decode($json, true);

        $parsedResult['payload'] = [
            'data' => $params
        ];
        $parsedResult['queryResult']['action'] = Arr::get($parsedResult, 'queryResult.action') ?: $intent;

        $parsedInput = $this->interpreter->parse(new Request($parsedResult));
        $parsedInput['success'] = true;
        return $parsedInput;
    }

    /**
     * [query description]
     *
     * @param  [type] $input  [description]
     * @param  [type] $intent [description]
     * @param  array  $params [description]
     * @return [type]         [description]
     */
    public function detectIntent($input, $intent, $params = [])
    {
        $response = [];
        try {
            $textInput = (new TextInput())
                ->setText($input)
                ->setLanguageCode($this->assistant->languageCode);

            $struct = new Struct();
            // Convert params to Google Message value
            foreach ($params as $key => $param) {
                $params[$key] = (new Value)->setStringValue($param);
            }
            $struct->setFields(
                array_merge(
                    $params, [
                    'any' => (new Value)->setStringValue($input)
                    ]
                )
            );


            /**
             * TODO: Webhook seems to be working now. NEeded to give action a name.
             * Now need to keep user session through responses
             *
             * @var QueryParameters
             */

            $queryParams = new QueryParameters();
            $queryParams->setPayload($struct);

            $eventInput = (new EventInput)
                ->setName($intent)
                ->setParameters($struct)
                ->setLanguageCode($this->assistant->languageCode);

            $queryInput = (new QueryInput())
                ->setEvent($eventInput);
            /**
             * Manually check the intent of the text based on the given intent
             *
             * @var [type]
             */
            $response = $this->getClient()->detectIntent(
                $this->clientFormattedSession, $queryInput, [
                'queryParams' => $queryParams
                ]
            );
        } finally {
            $this->getClient()->close();
        }

        $result = $response->getQueryResult();
        // When protobuf coverts to json it leaves emptyvalues that cause json errors
        $json = str_replace([':,', ":}"], [': "",', ': ""}'], $response->serializeToJsonString());

        $parsedResult = json_decode($json, true);

        $parsedResult['payload'] = [
            'data' => $this->request->all()
        ];

        $parsedInput = $this->interpreter->parse(new Request($parsedResult));
        return $this->process(false, $parsedInput);
    }

    /**
     * Send a response to theAPI
     *
     * @return [type] [description]
     */
    public function send(Response $response = null)
    {
        $response = $response ?? $this->response;
        $query = $this->getQuery();
        $queryParams = $query->getQueryParams();
        $localParams = $query->getLocalParams();
        // $response = $this->query(Arr::get($queryParams, 'message', $query->message), Arr::get($queryParams, 'intent'), $localParams);
        $response = $this->query($response->get('message'), Arr::get($localParams, 'followUpEventName', Arr::get($queryParams, 'intent')));
        $this->result = new DialogFlowResult(
            [
            'sessionId' => $response['sessionId'],
            'messageId' => $response['responseId'],
            'success' => true,
            'error' => Arr::get($response, 'errorMessage'),
            'result' => $response
            ]
        );

        return $this->result;
    }

    /**
     * Process the interaction
     *
     * @param  mixed $shouldParse
     * @param  mixed $parsedInput
     * @return array
     */
    public function process($shouldParse = true, array $parsedInput = null): array
    {
        // echo "Callign process\n";
        // debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10);
        // echo "\n";
        if ($shouldParse) {
            $parsedInput = $this->assistant->getInterpreter()->parse()->toArray();
        }

        if (empty($parsedInput)) {
            return [];
        }

        $action = $followUpEventName = $context =  $message = '';
        $attributes = $messages = $contexts = $followUp = $payload = [];

        extract($parsedInput);

        if (is_array($this->getResponse()->outputContexts)) {
            $attributes = Arr::get(array_shift($this->getResponse()->outputContexts), 'parameters', []);
        }

        $user = $this->user ?? $this->assistant->getProvider()->findUser(Arr::get($parsedInput, 'payload.data'));

        // $params = $this->assistant->parseFromServer(Arr::get($parsedInput, 'action', null));
        // extract($params);

        $message = strlen($message) ? $message : Arr::get($parsedInput, 'fulfillmentText');

        $this->response = app(DialogFlowResponse::class);
        $contexts = collect(
            array_merge(
                array_map(
                    [$this->getInterpreter(), 'getOutputContext'],
                    array_merge(
                        isset($contexts) ? $contexts : [],
                        Arr::get($parsedInput, 'contexts', [])
                    )
                ),
                (array)$this->interpreter->outputContexts
            )
        );

        $this->response->fill(
            array_filter(
                [
                'fulfillmentMessages' => $messages,
                'outputContexts' => $contexts->keyBy('name')->values()->toArray(),
                'payload' => $payload
                ]
            )
        );

        return compact('user', 'action', 'message', 'messages', 'contexts', 'followUp', 'followUpEventName', 'payload', 'attributes', 'queryText');
    }

    /**
     * [formatResponse description]
     *
     * @param  array $result [description]
     * @param  array $params [description]
     * @return DialogFlowResponse         [description]
     */
    public function formatResponse(array $result, array $params, $finish = false): BaseResponseProvider
    {
        $followUp = $followUpEventName = null;
        extract($result);
        $response = $this->getResponse();
        $message = $this->extractMessage(Arr::get($result, 'response', Arr::get($result, 'message')));
        $response->fulfillmentText = $message;
        $response->payload = Arr::get($result, 'payload', $response->payload);
        $response->followupEventInput = $followUpEventName ? $this->interpreter->getFollowupEventInput($followUpEventName, $followUp) : null;

        /**
         * Here we merge local contexts into the contexts in order to adjust the conversation as we need
         */
        $localContexts = [];
        foreach ((array)Arr::get($result, 'contexts', []) as $context) {
            // We merge the context information with the original parameters
            $context = is_array($context) ? $context : ['name' => (string)$context];
            $context['parameters'] = array_merge(Arr::get($context, 'parameters', $context), (array)$this->interpreter->parameters);
            $localContexts[] = $this->interpreter->getOutputContext($context);
        }
        $response->outputContexts = collect(array_merge((array)$response->outputContexts, $localContexts))->keyBy('name')->values()->toArray();


        /**
         * Finish the interaction
         */
        if ($finish) {
            $this->finish($params);
        }
        return $response;
    }

    /**
     * Get the activity object
     *
     * @return AssistantActivity
     */
    public function completeActivity()
    {
        $model = parent::getActivity();
        $interpreter = $this->assistant->getInterpreter();
        $model->fill(
            [
            'verb' => $this->getInteractionAction(),
            'remote_type' => $this->interaction ? $this->interaction->item_type : $this->contentType,
            'remote_id' => $this->interaction ? $this->interaction->item_id : $this->contentId,
            'object' => $this->interaction,
            'response' => $this->getResponse(),
            'input_contexts' => $interpreter->outputContexts,
            'output_contexts' => Arr::get($this->getResponse()->getQueryParams(), 'contexts'),
            'needs_handoff' => $this->needsHandoff,
            ]
        );
        return $model;
    }

    /**
     * @param null $data
     *
     * @return [type]
     */
    public function findUser($data = null)
    {
        $data  = $data ?: request()->all();
        // This is where Twilio fulfillment information is passed
        $originalRequest = Arr::get($data, "originalDetectIntentRequest");
        $source = Arr::get($originalRequest, "source");
        $payload = Arr::get($originalRequest, "payload.data", null);
        if ($this->assistant->hasInterpreter($source)) {
            return $this->assistant->loadInterpreter($source)->findUser($payload);
        }

        parent::findUser($payload);
    }

    /**
     * Get a formatted text payload
     *
     * @return array
     */
    protected function getPayload($payload)
    {
        return [
            'text' => $payload
        ];
    }
}