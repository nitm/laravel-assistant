<?php

namespace Nitm\Assistant\Providers\Interactions;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Nitm\Assistant\AssistantObject;
use Illuminate\Foundation\Auth\User;
use Nitm\Assistant\Contracts\Response;
use Nitm\Assistant\Contracts\Assistant;
use Nitm\Assistant\Contracts\Interaction;
use Nitm\Assistant\Jobs\DelayFollowUpJob;
use Nitm\Assistant\Models\AssistantActivity;
use Nitm\Assistant\Models\AssistantInteraction;
use Nitm\Assistant\Providers\Responses\BaseResultProvider;
use Nitm\Assistant\Providers\Responses\BaseResponseProvider;
use Nitm\Assistant\Providers\Interpreters\BaseInterpretationProvider;

/**
 * [Description BaseInteractionProvider]
 */
abstract class BaseInteractionProvider extends AssistantObject implements Interaction
{
    /**
     * @var string
     */
    protected $unknownInput = 'input.unknown';

    /**
     * @var [type]
     */
    protected $simulateFollowUpParams;

    /**
     * @var bool
     */
    protected $needsHandoff = false;

    /**
     * @var string
     */
    protected $contentType = 'none';

    /**
     * @var [type]
     */
    protected $contentId = -1;

    /**
     * @var [type]
     */
    protected $content;

    /**
     * @var [type]
     */
    protected $request;

    /**
     * The AI service used to parse the inputs
     * @var BaseInterpretationProvider
     */
    protected $interpreter;

    /**
     * The interaction result
     *
     * @var BaseResultProvider
     */
    protected $result;

    /**
     * Activty model
     *
     * @var AssistantActivity
     */
    protected $activity;

    /**
     * Previous activity
     *
     * @var AssistantActivity
     */
    protected $previousActivity;

    /**
     * The interaction
     * @var AssistantInteraction
     */
    protected $interaction;

    /**
     * The user for thsi interaction
     * @var User
     */
    protected $user;

    /**
     * The API Client for the interaction service provider
     * @var [type]
     */
    protected $client;

    /**
     * @var array
     */
    protected $userIdSources = [];

    /**
     * @param array $attributes
     * @param Assistant|null $assistant
     * @param Request|null $request
     */
    public function __construct($attributes = [], Assistant $assistant = null, Request $request = null)
    {
        $this->request = $request ?? $this->request ?? request();
        parent::__construct($attributes, $assistant);
    }

    /**
     * Run the interaction and return the response
     * @return []
     */
    public function interact($message, $attributes = [])
    {
        $this->interaction = $this->interaction ?: $this->assistant->interaction;
        $this->prepare($message, $attributes);
        /**
         * Expected response
         *[
         *  messageId: string,
         *  sessionId: string
         *]
         * @var [type]
         */
        $this->result = $this->send();

        if (Arr::get($this->result, 'success', false)) {
            $ret_val = [];
            if ($this->interaction) {
                /**
                 * Create a new interaction response
                 * @var [type]
                 */
                $this->interaction->queries()->create([
                    'provider_response_id' => $this->result['messageId'],
                    'result' => $this->result['result'],
                    'user_id' => $this->user->id,
                    'text' => $message ?: 'empty',
                    'type' => $this->assistant->getInteractionType()
                ]);

                $ret_val = $this->result;
            }

            $this->finish();
            return $ret_val;
        } else {
            \Log::error("Unable to save interaction");
            $this->finish();
            return [];
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $input
     * @return boolean
     */
    public function isUnknownInput($input): bool
    {
        return $input === $this->unknownInput;
    }

    public function getUnknownInput()
    {
        return $this->unknownInput;
    }

    /**
     * Prepare a message to be sent
     * @param  [type] $message [description]
     * @param  array  $params  [description]
     * @return BaseResponseProvider          [description]
     */
    public function prepare($message, $params = [])
    {
        $this->response = null;

        if ($this->assistant->getInteractionType() === 'response') {
            return $this->getResponse($message, true)->prepare($message, $params);
        } else {
            return $this->getQuery($message, true)->prepare($message, $params);
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $input
     * @param [type] $intent
     * @param array $params
     * @return void
     */
    public function query($input, $intent, $params = [])
    {
        return array_merge([
            'message' => $input,
            'action' => $intent,
        ], $params);
    }

    /**
     * Finish the interaction
     * @param array @params
     * @return void
     */
    public function finish(array $params = [])
    {
        $this->completeActivity()->save();
        // $response = $this->getResponse();
        if (config('assistant.simulateFollowUp') && !empty($this->simulateFollowUpParams)) {
            $this->triggerFollowUpResponse();
        }
    }

    /**
     * Trigger a direct response to the user through the interaction provider of choice
     *
     * @return void
     */
    protected function triggerFollowupResponse()
    {
        $provider = config('assistant.simulateFollowUpWith');
        $message = Arr::get($this->simulateFollowUpParams, 'message');
        if ($provider && $message) {
            // Simulate a delayed response
            // sleep(5);
            // $provider->interact($this->simulateFollowUpParams['message'], $this->getResponse()->payload);
            /**
             * Once we have interaction providers then use a queued job
             * We need to do this since a direct response will always be faster than a response going through the original provider
             */
            // $this->simulateFollowUpParams = null;
            // DelayFollowUpJob::dispatch([
            //     'userId' => $this->getUser()->id,
            //     'provider' =>  config('assistant.simulateFollowUpWith'),
            //     'message' => $message,
            //     'payload' => $this->getResponse()->payload,
            //     'previousActivityId' => $this->activity ? $this->activity->id : null,
            //     'userQueryParams' => $this->assistant->getInterpreter()->getUserQueryParams()
            // ])->delay(now()->addSeconds(5));
            DelayFollowUpJob::dispatch([
                'userId' => $this->getUser()->id,
                'provider' => config('assistant.simulateFollowUpWith'),
                'message' => $message,
                'payload' => $this->getResponse()->payload,
                'previousActivityId' => $this->activity ? $this->activity->id : null,
                'userQueryParams' => $this->assistant->getInterpreter()->getUserQueryParams()
            ]);
        }
    }

    /**
     * @return [type]
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $attributes
     *
     * @return Response
     */
    public function makeResponse($attributes = []): Response
    {
        return $this->newResponse($attributes);
    }

    /**
     * @return Interaction
     */
    public function getInteraction(): AssistantInteraction
    {
        return $this->interaction;
    }

    /**
     * @return bool
     */
    public function hasInteraction(): bool
    {
        return $this->interaction instanceof AssistantInteraction;
    }

    /**
     * @return string
     */
    public function getInteractionAction(): string
    {
        return $this->interaction ? ($this->interaction->action ?: 'none') : 'none';
    }

    /**
     * @param string $type
     *
     * @return [type]
     */
    public function setContentType(string $type)
    {
        $this->contentType = $type;
    }

    /**
     * @param mixed $id
     *
     * @return [type]
     */
    public function setContentId($id)
    {
        $this->contentId = $id;
    }

    /**
     * @param mixed $content
     *
     * @return [type]
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Find the user
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function findUser($data = null)
    {
        $class = config('assistant.userClass', 'App\User');
        $data  = $data ?: request()->input();
        if (is_array($data) && !empty($data)) {
            foreach ($this->userIdSources as $source) {
                if (($phone = Arr::get($data, $source)) !== null) {
                    break;
                }
            }
            if (isset($phone)) {
                return $class::where(['phone' => $phone])->first();
            }
        }

        if (\App::environment(['dev', 'local', 'staging'])) {
            // Lets return the first user for kicks
            // Need to select random user from developers!
            // Shoud add it to application config with user's email
            return $class::where(['email' => config('assistant.userWhenNoneFound')])->first();
        }

        return null;
    }

    /**
     * Set the previous activity for creating the activity tree
     *
     * @param AssistantActivity $activity
     * @return void
     */
    public function setPreviousActivity(AssistantActivity $activity)
    {
        $this->previousActivity = $activity;
    }

    /**
     * Create an new activity object
     *
     * @return AssistantActivity
     */
    public function newActivity(): AssistantActivity
    {
        $model = new AssistantActivity;
        $interpreter = $this->assistant->getInterpreter();
        $model->fill([
            'user_id' => $this->getUser()->id,
            'verb' => $interpreter->action,
            'input_action' => $interpreter->action,
            'input_intent' => $interpreter->intent,
            'user' => $this->getUser(),
            'request' => request()->all(),
            'previous_activity_id' => $this->previousActivity instanceof AssistantActivity ? $this->previousActivity->id : null
        ]);
        return $model;
    }

    /**
     * Get the activity object
     *
     * @return AssistantActivity
     */
    public function getActivity()
    {
        if (!isset($this->activity)) {
            $this->activity = $this->newActivity();
        }
        return $this->activity;
    }

    /**
     * THese are the parameters to use for simulating a followup response in the case that the current interaction provdier doesn't support it
     *
     * @param [type] $params
     * @return void
     */
    public function setParamsForSimulatedFollowUp($params)
    {
        $this->simulateFollowUpParams = $params;
    }

    /**
     * Cancel teh simulated followup
     *
     * @return boolean
     */
    public function cancelSimulatedFollowUp()
    {
        $this->simulateFollowUpParams = [
            'canceled' => true
        ];
    }

    /**
     * Support random messages if an array is passed
     *
     * @param mixed $from
     * @return string
     */
    protected function extractMessage($from): string
    {
        $message = $from;
        if (is_array($from)) {
            $message = $from[rand(0, count($from) - 1)];
        }
        return $message;
    }
}
