<?php


namespace Nitm\Assistant\Providers\Interactions;

use Twilio\Http\CurlClient as HttpCurlClient;
use Twilio\Jwt\AccessToken;

/**
 * PKCVClient
 *
 * Custom PKCV HTTP Client
 */
class PKCVClient extends HttpCurlClient
{
    public function options(
        $method,
        $url,
        $params = array(),
        $data = array(),
        $headers = array(),
        $user = null,
        $password = null,
        $timeout = null
    ) {
        $options = parent::options(...func_get_args());

        // Set the signed PKCV header here
        $account_sid = config('services.twilio.account_sid');
        $api_key = config('services.twilio.signing_key');
        $api_key_secret = config('services.twilio.signing_key_secret');
        if (!$api_key || !$api_key_secret) {
            throw new EnvironmentException('Unable to initialize cURL');
        }

        $token = new AccessToken($account_sid, $api_key, $api_key_secret);

        $options[CURLOPT_HTTPHEADER][] = "Twilio-Client-Validation: " . $token->toJWT();

        return $options;
    }
}