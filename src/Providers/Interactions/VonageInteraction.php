<?php

namespace Nitm\Assistant\Providers\Interactions;

use Vonage\Client;
use Vonage\SMS\SentSMS;
use Illuminate\Support\Arr;
use Vonage\SMS\Message\SMS;
use Vonage\Client\Credentials\Basic;
use Nitm\Assistant\Contracts\Response;
use Vonage\Client\Credentials\Keypair;
use Vonage\Client\Credentials\Container;
use Nitm\Assistant\Contracts\Interpretation;
use Vonage\Client\Credentials\SignatureSecret;
use Nitm\Assistant\Providers\Results\VonageResult;
use Nitm\Assistant\Providers\Responses\VonageResponse;
use Nitm\Assistant\Providers\Responses\BaseResponseProvider;
use Nitm\Assistant\Providers\Interpreters\VonageInterpretation;
use Nitm\Assistant\Providers\Interactions\BaseInteractionProvider;
use Nitm\Assistant\Providers\Interpreters\BaseInterpretationProvider;

class VonageInteraction extends BaseInteractionProvider
{
    /**
     * @var [type]
     */
    protected $query;

    /**
     * @var [type]
     */
    protected $intent;

    /**
     * @var [type]
     */
    protected $parameters;

    /**
     * @var BaseResponseProvider
     */
    protected $response;

    /**
     * @var BaseInterpretationProvider
     */
    protected $interpreter;

    /**
     * @var [type]
     */
    protected $input;

    /**
     * @param mixed $attributes
     *
     * @return [type]
     */
    public function newResponse($attributes = []): Response
    {
        return new VonageResponse($attributes);
    }

    /**
     * @param string $message
     * @param bool   $force
     *
     * @return [type]
     */
    public function getResponse($message = '', $force = false)
    {
        if ($force === true || !isset($this->response)) {
            $this->response = app(VonageResponse::class);
            $this->response->fill(
                [
                    'to' => $this->user->phone,
                    'message' => $message,
                    'assistant' => $this->assistant,
                    'user' => $this->user
                ]
            );
        }
        return $this->response;
    }

    /**
     * Get Interpreter
     *
     * @return Interpretation
     */
    public function getInterpreter()
    {
        if (!isset($this->interpreter)) {

            $this->interpreter =  new VonageInterpretation([], $this->assistant);
        }

        return $this->interpreter;
    }

    /**
     * @param string $message
     * @param bool   $force
     *
     * @return [type]
     */
    public function getQuery($message = '', $force = false)
    {
        if ($force === true || !isset($this->response)) {
            $this->response = app(VonageResponse::class);
            $this->response->fill(
                [
                    'to' => phone($this->user->phone, config('assistant.supportedCountries'), 'e164'),
                    'message' => $message,
                    'assistant' => $this->assistant,
                    'user' => $this->user
                ]
            );
        }
        return $this->response;
    }

    /**
     * Send a response to theAPI
     *
     * @return [type] [description]
     */
    public function send(Response $response = null)
    {
        $errorMessage = $errorCode = null;
        $response = $response ?? $this->response;
        $from = intval($response->from ?? config('services.twilio.from'));
        $text = new SMS(
            $response->get('to'),
            $from,
            $response->get('message') ?? ""
        );
        try {
            $sms = $this->getClient()->sms()->send($text)->current();
        } catch (\Exception $error) {
            $errorMessage = $error->getMessage();
            $errorCode = $error->getCode();
            if (app()->environment('dev')) {
                // print_r(
                //     [
                //     $from,
                //     $response->to,
                //     $response->message
                //     ]
                // );
                \Log::error($error);
                throw new \Exception($error);
            }
            $sms = new SentSMS(
                [
                    'status' => -1,
                    'message-id' => 'failed-message',
                    'to' => $text->getTo(),
                    'message-price' => 0.00,
                    'remaining-balance' => 0.00,
                    'network' => 'none'
                ]
            );
        }

        $this->result = new VonageResult(
            [
                "id" => $sms->getMessageId(),
                "to" => $sms->getTo(),
                "from" => $from,
                "message" => $response->get('message'),
                'sessionId' => $sms->getMessageId() . '-' . $sms->getNetwork(),
                'success' => $errorMessage == null,
                'error' => $errorMessage,
                'errorCode' => $errorCode,
                "status" => $sms->getStatus(),
                'original' => $sms
            ]
        );

        return $this->result;
    }

    /**
     * Get the API client
     *
     * @return [type] [description]
     */
    public function getClient()
    {
        if (!isset($this->client)) {
            $signatureSecret = config('services.vonage.signature_secret');
            $apiPrivateKey = config('services.vonage.api_private_key');
            $appId = config('services.vonage.application_id');
            if (!$apiPrivateKey || !$appId) {
                throw new \Error("Unable to find credentials. Please confirm the VONAGE_API_PRIVATE_KEY and VONAGE_APPLICATION_ID environment variables are set.");
            }
            $keyPair = new \Vonage\Client\Credentials\Keypair(str_replace("\\n", "\n", $apiPrivateKey), $appId);
            if (!empty($signatureSecret)) {
                $container = new Container(
                    new Basic(
                        config('services.vonage.api_key'),
                        config('services.vonage.api_secret')
                    ),
                    new SignatureSecret(
                        config('services.vonage.api_key'),
                        $signatureSecret,
                        config('services.vonage.signature_algorithm')
                    ),
                    $keyPair
                );
            } else {
                $container = new Container(
                    new Basic(
                        config('services.vonage.api_key'),
                        config('services.vonage.api_secret')
                    ),
                    $keyPair
                );
            }
            $this->client = new Client(
                $container,
                array_filter(
                    [
                        'base_api_url' => app()->environment('testing') ? 'https://example.com' : null
                    ]
                )
            );
        }
        return $this->client;
    }

    /**
     * Process the interaction
     *
     * @return array [description]
     */
    public function process(): array
    {
        $parsedInput = $this->interpreter->parse();

        $action = Arr::get($parsedInput, 'action') ?: $this->interaction->action;

        $params = $this->assistant->parseFromServer($action);
        extract($params);
        $parameters = Arr::get($parsedInput, 'parameters') ?: [];
        $attributes = array_merge($parameters, $attributes ?? []);

        // if ($intent) {
        //     $result = $this->assistant
        //         ->getInterpreter()
        //         ->query($parsedInput['input'], $intent, [
        //             Assistant::USER_CONTEXT.'.phone' => $user->phone
        //         ]);
        //     extract($result);
        // }

        return compact('user', 'action', 'message', 'messages', 'contexts', 'followUp', 'followUpEventName', 'payload', 'attributes');
    }

    /**
     * [formatResponse description]
     *
     * @param  array $result [description]
     * @param  array $params [description]
     * @return VonageResponse         [description]
     */
    public function formatResponse(array $result, array $params): BaseResponseProvider
    {
        $this->getResponse()->message = $this->extractMessage(Arr::get($result, 'response'));
        return $this->response;
    }

    /**
     * [getSessionId description]
     *
     * @return string [description]
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * Get the activity object
     *
     * @return AssistantActivity
     */
    public function completeActivity()
    {
        $model = parent::getActivity();
        $interpreter = $this->assistant->getInterpreter();
        $model->fill(
            [
                'verb' => $this->getInteractionAction(),
                'remote_type' => $this->interaction ? $this->interaction->item_type : $this->contentType,
                'remote_id' => $this->interaction ? $this->interaction->item_id : $this->contentId,
                'object' => $this->content,
                'response' => $this->getResponse(),
                'input_contexts' => $interpreter->outputContexts,
                'output_contexts' => Arr::get($this->getResponse()->getQueryParams(), 'contexts'),
                'needs_handoff' => $this->needsHandoff
            ]
        );
        return $model;
    }
}