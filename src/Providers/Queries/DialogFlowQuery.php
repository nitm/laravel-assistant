<?php

namespace Nitm\Assistant\Providers\Queries;

use Illuminate\Support\Arr;
use Nitm\Assistant\Providers\Queries\BaseQueryProvider;

class DialogFlowQuery extends BaseQueryProvider
{
    public $fulfillmentText;
    public $fulfillmentMessages = [];
    public $outputContexts = [];
    public $followupEventInput = [];
    public $payload = [];

    /**
     * Prepare a query
     *
     * @param  string $message    [description]
     * @param  array  $attributes [description]
     * @return [type]             [description]
     */
    public function prepare($message = '', $attributes = [])
    {
        $response = array_map(
            function ($attribute) {
                return empty($this->attribute) ? false : $this->$attribute;
            }, static::responseMap()
        );

        $this->queryParams = array_filter(array_combine(static::responseMap(), $response));
        $this->queryParams['intent'] = array_get($this->queryParams, 'intent', $this->interaction->action);

        $this->localParams = $attributes;
    }

    /**
     * The type of data we expect to return for the response
     *
     * @return array
     */
    protected static function responseMap(): array
    {
        return [
            'fulfillmentText',
            'fulfillmentMessages',
            'outputContexts',
            'followupEventInput',
            'payload'
        ];
    }

    /**
     * Need to return only non empty objects to DialogFlow
     *
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();
        return array_filter(
            $data, function ($prop) {
                return !empty($prop);
            }
        );
    }

    /**
     * @inheritDoc
     */
    public function setTo(string $to)
    {
        return Arr::set($this->payload, 'data.From', $to);
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public function getTo(): string
    {
        return Arr::get($this->payload, 'data.From');
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): string
    {
        return $this->fulfillmentText;
    }

    /**
     * @inheritDoc
     */
    public function setMessage(string $message)
    {
        $this->fulfillmentText = $message;
    }
}