<?php

namespace Nitm\Assistant\Providers\Queries;

use Nitm\Assistant\Contracts\Query;
use Nitm\Assistant\Contracts\Response;
use Nitm\Assistant\Providers\Responses\BaseResponseProvider;

abstract class BaseQueryProvider extends BaseResponseProvider implements Query, Response
{
    public $message;
    public $intent;
    public $contexts = [];
    public $params = [];
    protected $queryParams = [];
    protected $interaction;
    protected $localParams = [];

    /**
     * The user for this query
     * @var [type]
     */
    protected $user;

    /**
     * @param string $message
     * @param array $attributes
     *
     * @return [type]
     */
    public function prepare($message = '', $attributes = [])
    {
        $response = array_map(function ($attribute) {
            return $this->$attribute;
        }, static::responseMap());

        $this->queryParams = array_filter(array_combine(static::responseMap(), $response));

        $this->localParams = $attributes;
    }

    /**
     * @param mixed $key
     *
     * @return [type]
     */
    public function get($key)
    {
        return property_exists($this, $key) ? $this->$key : null;
    }

    /**
     * Get the raw response
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Get the local parameters
     * @return array
     */
    public function getLocalParams()
    {
        return $this->localParams;
    }

    /**
     * Get the query parameters
     * @return array
     */
    public function getQueryParams()
    {
        return $this->queryParams;
    }

    /**
     * The type of data we expect to return for the response
     * @return array
     */
    protected static function responseMap()
    {
        return [
            'message',
            'intent',
            'params',
            'contexts'
        ];
    }
}
