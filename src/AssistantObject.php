<?php

namespace Nitm\Assistant;

use ArrayAccess;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\User;
use Nitm\Assistant\Contracts\Assistant;
use Illuminate\Contracts\Support\Arrayable;

class AssistantObject implements Arrayable, ArrayAccess
{
    protected $assistant;

    protected $request;

    protected $user;

    protected $attributes = [];

    use \Nitm\Assistant\Traits\ObjectTrait;

    public function __construct($attributes = [], Assistant $assistant = null)
    {
        $this->fill($attributes);
        $this->assistant = $assistant ?? $this->assistant ?? app(Assistant::class);
        // $this->assistant->setUser($this->getUser());
    }

    /**
     * Magic getter to access attributes
     *
     * @param string $name Property to access
     * @return mixed The requested property
     * @throws Exception For unknown attributes
     */
    public function __get($name)
    {
        $method = 'get' . ucfirst(Str::camel($name));
        if (\method_exists($this, $method)) {
            return $this->$method();
        }

        if (\property_exists($this, $name)) {
            return $this->$name;
        }

        if (\array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        }

        return null;
    }

    /**
     * @param mixed $property
     * @param mixed $value
     *
     * @return [type]
     */
    public function __set($property, $value)
    {
        if (property_exists(get_class($this), $property)) {
            $this->$property = $value;
        } else {
            $this->attributes[$property] = $value;
        }
    }

    /**
     * @param mixed $property
     * @param mixed $value
     *
     * @return [type]
     */
    public function set($property, $value)
    {
        $this->__set($property, $value);
    }

    /**
     * Set the current user
     * @param User $user [description]
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Get the user object
     * @param  [type] $user [description]
     * @return [type]       [description]
     */
    public function getUser($user = null)
    {
        if ($user instanceof User) {
            $this->setUser($user);
        } else {
            if (!isset($this->user)) {
                $this->user = $this->findUser(null);
            }
        }
        return $this->user;
    }


    /**
     * Default json decoder
     *
     * @return array
     */
    public function toArray(): array
    {
        return array_merge(json_decode(json_encode($this), true), $this->attributes);
    }

    /**
     * Determine if the given attribute exists.
     *
     * @param  mixed  $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return !is_null($this->$offset);
    }

    /**
     * Get the value for a given offset.
     *
     * @param  mixed  $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    /**
     * Set the value for a given offset.
     *
     * @param  mixed  $offset
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    /**
     * Unset the value for a given offset.
     *
     * @param  mixed  $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset]);

        if (property_exists($this, $offset)) {
            $this->$offset = null;
        }
    }

    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return $this->offsetExists($key);
    }

    /**
     * Unset an attribute on the model.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        $this->offsetUnset($key);
    }
}