<?php

namespace Nitm\Assistant\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Complete a log job
 */
class LogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The log messages to write
     *
     * @var array
     */
    public $logMessages = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $channel, array $messages)
    {
        $this->logMessages = $messages;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->logMessages)) {
            foreach ($this->logMessages as $options) {
                if (is_array($options)) {
                    list($level, $message) = $options;
                } else {
                    $level = 'info';
                    $message = $options;
                }
                \Log::channel($this->channel)->$level($message);
            }
        }
    }
}
