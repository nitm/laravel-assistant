<?php

namespace Nitm\Assistant\Jobs;

use App\User;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Nitm\Assistant\Contracts\Assistant;

class ProcessAssistantInteraction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $interaction;
    protected $assistant;
    protected $message;
    protected $attributes;
    protected $userId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($interaction, $message, $attributes = [])
    {
        $this->interaction = $interaction;
        $this->message = $message;
        $this->attributes = $attributes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $class = config('assistant.userClass', 'App\User');
        $user = $this->interaction->user instanceof User ? $this->interaction->user : $class::find($this->interaction->user_id);

        if ($user instanceof $class) {
            \Log::info(__CLASS__ . ": User is " . ($user ? $user->name : '(not found)') . "\n");

            $assistant = app(Assistant::class);
            // This has to be called before any other assistant methods to properly set the context
            $assistant->setInteractionTypeStartedBy('query', 'server');
            $assistant->setUser($user);
            $result = $assistant->finish($this->interaction, $this->message, $this->attributes);
            \Log::info(__CLASS__ . ": Successfully followed up with result " . json_encode($result));
        } else {
            \Log::error(__CLASS__ . ": Missing user for " . $this->interaction->user_id);
        }
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->interaction->update(['status' => 'failed']);
        // Send user notification of failure, etc...
    }
}