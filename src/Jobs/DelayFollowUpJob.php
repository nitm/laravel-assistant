<?php

namespace Nitm\Assistant\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Nitm\Assistant\BaseInteractionProvider;
use Nitm\Assistant\Models\AssistantActivity;
use Nitm\Assistant\Contracts\Assistant;
use Illuminate\Foundation\Auth\User;

/**
 * Delay an interaction for a certain amount of time.
 */
class DelayFollowUpJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The previous activity id
     *
     * @var int
     */
    public $previousActivityId;

    /**
     * The user id
     *
     * @var int
     */
    public $userId;

    /**
     * The interaction provider
     *
     * @var BaseInteractionProvdier
     */
    public $provider;

    /**
     * THe message to be sent through the provider
     *
     * @var string
     */
    public $message;

    /**
     * The parameters to send to the provdier
     *
     * @var array
     */
    public $payload;

    /**
     * The parameters used to find the user
     *
     * @var [type]
     */
    public $userQueryParams;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $params)
    {
        $provider = $userId = $previousActivityId = $message = $payload = $userQueryParams = null;
        extract($params);
        $this->userId = $userId;
        $this->previousActivityId = $previousActivityId;
        $this->provider = $provider;
        $this->message = $message;
        $this->payload = $payload;
        $this->userQueryParams = $userQueryParams;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $class = config('assistant.userClass', 'App\User');
        $assistant = new Assistant(request());
        $user = $this->userId ? $class::find($this->userId) : null;

        /**
         * We use this here to repopulate the user based on the original user paramters received from the remote call
         * This is useful if it's an anonymous user and we wantt o respond back to that user
         */
        if (!empty($this->userQueryParams)) {
            $user->fill($this->userQueryParams);
        }
        if ($user instanceof User) {
            $assistant->setUser($user);
            $assistant->setCurrentInteraction($assistant->getLastInteraction($user));
            $provider = $assistant->getProvider($this->provider);
            if ($provider) {
                $provider->setUser($user);
                // Load the provider and the basic information
                // Need this for properly setting up the activity
                $interpreter = $assistant->getInterpreter();
                $interpreter->action = 'follow-up';
                $assistant->setInteractionType('query');
                $result = $provider->interact($this->message, $this->payload);
                $activity = $this->previousActivityId ? AssistantActivity::find($this->previousActivityId) : null;
                if ($activity) {
                    $provider->setPreviousActivity($activity);
                }
                \Log::info("Initiated interaction: Previous activity[" . $this->previousActivityId . "]. Result: " . json_encode($result));
            } else {
                \Log::error("Couldn't load provider: [$this->provider] for followup interaction");
            }
        } else {
            \Log::error("Couldn't find user: [$this->userId] for followup interaction");
        }
    }
}