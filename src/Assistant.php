<?php

namespace Nitm\Assistant;

/**
 * The base assistant class used to generate and interact with users
 */
class Assistant extends BaseAssistant
{

    /**
     * Handle a case where there is no interaction available
     *
     * @return array
     */
    public function handleNoInteraction(): array
    {
        throw new \Exception("Not implemented: " . __CLASS__ . '::' . __FUNCTION__);
    }

    /**
     * Handle a custom behavior not handled internally by the default assistant
     * @param  [type] $interaction [description]
     * @param  [type] $provider    [description]
     * @return array              [description]
     */
    public function handleFromUser($interaction = null, $provider = null, $params = []): array
    {

        throw new \Exception("Not implemented: " . __CLASS__ . '::' . __FUNCTION__);
    }

    /**
     * Custom interaction handler
     * @param  [type] $user        [description]
     * @param  [type] $interaction [description]
     * @return array              [description]
     */
    public function handleFromServer($user, $interaction): array
    {
        throw new \Exception("Not implemented: " . __CLASS__ . '::' . __FUNCTION__);
    }

    /**
     * Interact with the named intent
     * @param  string $interactionAction [description]
     * @return array array The action components
     *  Expects: [
     *    'action' => $action,
     *    'intent' => $intent,
     *    'message' => $message,
     *    'contexts' => $contexts,
     *    'followUpEventName' => $followUpEventName
     *  ]
     */
    public function parseFromServer(string $interactionAction): array
    {
        throw new \Exception("Not implemented: " . __CLASS__ . '::' . __FUNCTION__);
    }
}