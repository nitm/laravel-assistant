<?php

namespace Nitm\Assistant\Contracts;

interface Repository
{
    /**
     * Get the filters supported
     * @return array
     */
    public function getFilters(): array;
}
