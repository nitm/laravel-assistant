<?php

namespace Nitm\Assistant\Contracts;

interface Query
{
    /**
     * Get the response based on the assistant gateway being used
     * @return array
     */
    public function getQuery();

    public function prepare();
}
