<?php

namespace Nitm\Assistant\Contracts;

use Illuminate\Foundation\Auth\User;

interface Assistant
{
    /**
     * Handle a case where there is no interaction available
     *
     * @return array
     */
    public function handleNoInteraction(): array;

    /**
     * Handle a custom behavior not handled internally by the default assistant
     * @param  [type] $interaction [description]
     * @param  [type] $provider    [description]
     * @return array              [description]
     */
    public function handleFromUser($interaction = null, $provider = null, $params = []): array;

    /**
     * Custom interaction handler
     * @param  [type] $user        [description]
     * @param  [type] $interaction [description]
     * @return array              [description]
     */
    public function handleFromServer($user, $interaction): array;

    /**
     * Interact with the named intent
     * @param  string $interactionAction [description]
     * @return array array The action components
     *  Expects: [
     *    'action' => $action,
     *    'intent' => $intent,
     *    'message' => $message,
     *    'contexts' => $contexts,
     *    'followUpEventName' => $followUpEventName
     *  ]
     */
    public function parseFromServer(string $interactionAction): array;
}