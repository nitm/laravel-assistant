<?php

namespace Nitm\Assistant\Contracts;

use Nitm\Assistant\BaseInteractionProvider;

interface Interpretation
{
    /**
     * Parse the incoming interaction
     * @return array [description]
     */
    public function parse();
    /**
     * Parse the incoming interaction
     * @return array [description]
     */
    public function getParsedData();

    /**
     * Get the core provider for this interpreter
     * @return BaseInteractionProvider
     */
    public function getProvider();
}
