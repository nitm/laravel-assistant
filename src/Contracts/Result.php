<?php

namespace Nitm\Assistant\Contracts;

interface Result
{
    /**
     * Get the message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Get the media
     *
     * @return string
     */
    public function getMedia();

    /**
     * Get the to address
     *
     * @return string
     */
    public function getTo();

    /**
     * Get the from address
     *
     * @return string
     */
    public function getFrom();

    /**
     * Get the to id
     *
     * @return string
     */
    public function getId();

    /**
     * Get the error
     *
     * @return string
     */
    public function getError();

    /**
     * Get the status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Get the status
     *
     * @return string
     */
    public function getAccountId();

    /**
     * Get the status
     *
     * @return string
     */
    public function getServiceId();
}