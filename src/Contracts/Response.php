<?php

namespace Nitm\Assistant\Contracts;

interface Response
{
    /**
     * Get the response based on the assistant gateway being used
     * @return array
     */
    public function getResponse();

    public function prepare();

    /**
     * Get the message being sent
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set the message
     *
     * @param string $message
     * @return void
     */
    public function setMessage(string $message);

    /**
     * Get the to address
     *
     * @return string
     */
    public function getTo();

    /**
     * Set the to value
     *
     * @param string $to
     * @return void
     */
    public function setTo(string $to);
}
