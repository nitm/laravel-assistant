<?php

namespace Nitm\Assistant\Contracts;

use Nitm\Assistant\BaseResponseProvider;
use Nitm\Assistant\Models\AssistantActivity;

interface Interaction
{
    /**
     * Send the interaction
     * @return [type] [description]
     */
    public function send(Response $response = null);

    /**
     * Prepare theinteraction
     * @return [type] [description]
     */
    public function prepare($message, $params = []);

    /**
     * Interact with user
     * @param  [type] $message [description]
     * @return [type]          [description]
     */
    public function interact($message);

    /**
     * Find a user based on payload data
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function findUser($data);

    /**
     * Get the response object
     * @return BaseResponseProvider
     */
    public function getResponse($message, $force);

    /**
     * @param mixed $message
     * @param mixed $to
     * @param mixed $from
     *
     * @return BaseResponseProvider
     */
    public function newResponse($attribtues = []): Response;

    /**
     * Get teh response object
     * @return BaseQueryProvider
     */
    public function getQuery($message, $force);

    /**
     * Get the activity object for this activity
     *
     * @return AssistantActivity
     */
    public function getActivity();
}