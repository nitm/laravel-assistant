<?php

namespace Nitm\Assistant;

use Config;
use Route;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    // public $defer = true;

    /**
     * The handler to use for response parsing
     * @var string
     */
    public $interpreter = 'dialogFlow';

    /**
     * The provider to use for interactons
     * @var string
     */
    public $provider = 'twilio';

    /**
     * How many interactions should there be perr session
     * @var integer
     */
    public $interactionsPerSession = 5;

    /**
     * How many interactions should there be per day?
     * @var integer
     */
    public $dailySessions = 3;

    /**
     * The earliest time the assistant can start interacting
     * @var integer
     */
    public $startTime = 0;

    /**
     * The latest time the assistant can start interacting
     * @var integer
     */
    public $endTime = 0;

    /**
     * Break up thegroups into chunks of this size
     * @var [type]
     */
    public $groupCount = 20;

    /**
     * The amount of hours wiith which an item is considered due soon
     * @var integer
     */
    public $dueSoonHourDiff = 48;

    /**
     * The amount of time for an item to be considered near completion
     * @var integer
     */
    public $dueNearCompletionHourDiff = 24;

    /**
     * Countries supported by the assistant
     * @var array
     */
    public $supportedCountries = ['US'];

    /**
     * The assistant class to use
     * @var string
     */
    public $assistantClass = 'App\Assistant\Assistant';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/assistant.php');

        if ($this->app->runningInConsole()) {
            $this->commands([
                Console\Commands\TestInteractions::class,
                Console\Commands\RunInteractions::class,
                Console\Commands\PruneInteractions::class,
            ]);
        }

        /**
         * Verify that the core files have been copied over to the applcation
         * @var [type]
         */
        // foreach ([
        //     'App\Assistant\Assistant',
        //     'App\Asssitant\Models\Assistant',
        //     'App\Asssitant\Models\AssistantChoice',
        //     'App\Asssitant\Models\AssistantInteraction',
        //     'App\Asssitant\Models\AssistantInteractionResponse',
        //     'App\Asssitant\Models\AssistantSession'
        // ] as $class) {
        //     if (!class_exists($class)) {
        //         throw new \Exception("$class needs to be available in your app. Please publish the Assistant folder to your app folder. View README.md for details");
        //     }
        // }

        /**
         * Advertise the assets to be published
         */
        $this->publishesAssets();
        $this->publishesConfig();

        /**
         * Publish database files
         *
         */
        // $this->publishes([
        // __DIR__.'/../database' => base_path('database')
        // ], 'database');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->interpreter = config('assistant.interpreter') ?: $this->interpreter;
        $this->provider = config('assistant.provider') ?: $this->provider;
        $this->interactionsPerSession = config('assistant.interactionsPerSession') ?: $this->interactionsPerSession;
        $this->dailySessions = config('assistant.dailySessions') ?: $this->dailySessions;
        $this->startTime = config('assistant.startTime') ?: $this->startTime;
        $this->endTime = config('assistant.endTime') ?: $this->endTime;
        $this->groupCount = config('assistant.groupCount') ?: $this->groupCount;
        $this->dueSoonHourDiff = config('assistant.dueSoonHourDiff') ?: $this->dueSoonHourDiff;
        $this->dueNearCompletionHourDiff = config('assistant.dueNearCompletionHourDiff') ?: $this->dueNearCompletionHourDiff;
        $this->supportedCountries = config('assistant.supportedCountries') ?: $this->supportedCountries;

        $assistantClass = $this->assistantClass = config('assistant.assistantClass') ?: $this->assistantClass;
        $assistantClass = class_exists($assistantClass) ? $assistantClass : \Nitm\Assistant\Assistant::class;
        $this->app->singleton('App\Assistant\Assistant', function ($app) use ($assistantClass) {
            return new $assistantClass(request(), $this->provider);
        });

        $this->app->bind('Nitm\Assistant\Contracts\Assistant', $assistantClass);

        $ui = config('assistant.ui', 'generic');
        if ($ui == 'spark') {
            $this->loadViewsFrom(__DIR__ . '/../resources/views/spark', 'assistant');
        } elseif ($ui == 'generic') {
            $this->loadViewsFrom(__DIR__ . '/../resources/views/generic', 'assistant');
        } elseif ($ui == 'voyager') {
            $this->loadViewsFrom(__DIR__ . '/../resources/views/voyager', 'assistant');
        }
    }

    public static function getVoyagerRoutes()
    {
        $base = '\\Nitm\\Assistant\\Http\\Controllers\\';
        Route::resource('assistants', $base . 'AssistantController', ['as' => 'voyager']);
        Route::resource('assistantActivity', $base . 'AssistantActivityController', ['as' => 'voyager']);
        Route::resource('assistant-activity', $base . 'AssistantActivityController', ['as' => 'voyager']);
        Route::resource('assistantSessions', $base . 'AssistantSessionController', ['as' => 'voyager']);
        Route::resource('assistant-sessions', $base . 'AssistantSessionController', ['as' => 'voyager']);
        Route::resource('assistantQueues', $base . 'AssistantQueueController', ['as' => 'voyager']);
        Route::resource('assistant-queues', $base . 'AssistantQueueController', ['as' => 'voyager']);
        Route::resource('assistantInteractions', $base . 'AssistantInteractionController', ['as' => 'voyager']);
        Route::resource('assistant-interactions', $base . 'AssistantInteractionController', ['as' => 'voyager']);
        Route::resource('assistant-interaction-queries', $base . 'AssistantInteractionQueryController', ['as' => 'voyager']);
        Route::resource('assistantInteractionQueries', $base . 'AssistantInteractionQueryController', ['as' => 'voyager']);
    }

    protected function publishesAssets()
    {
        $this->publishes([
            __DIR__ . '/../publish/app/Assistant' => app_path('Assistant')
        ], 'assistant');
    }

    /**
     * Publish the config file
     *
     * @param  string $configPath
     */
    protected function publishesConfig()
    {
        $this->publishes([__DIR__ . '/../config/assistant.php' => config_path('assistant.php')], 'assistant-config');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    // public function provides()
    // {
    //     if (class_exists($this->assistantClass)) {
    //         return [
    //             'command.assistant.test', $this->assistantClass,
    //             $this->assistantClass
    //         ];
    //     }
    // }
}
