<?php

namespace Nitm\Assistant\Http\Controllers;

use Nitm\Assistant\Http\Controllers\AppBaseController;

class AssistantInteractionController extends AppBaseController
{
    protected $slug = 'assistant-interactions';
}
