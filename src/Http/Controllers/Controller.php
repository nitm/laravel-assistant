<?php

namespace Nitm\Assistant\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseController;
use Nitm\Assistant\Traits\AppController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, AppController;
}
