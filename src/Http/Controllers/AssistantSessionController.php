<?php

namespace Nitm\Assistant\Http\Controllers;

use Nitm\Assistant\Http\Controllers\AppBaseController;

class AssistantSessionController extends AppBaseController
{
    protected $slug = 'assistant-sessions';
}
