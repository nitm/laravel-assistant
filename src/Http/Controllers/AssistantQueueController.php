<?php

namespace Nitm\Assistant\Http\Controllers;

use Nitm\Assistant\Http\Controllers\AppBaseController;

class AssistantQueueController extends AppBaseController
{
    protected $slug = 'assistant-queues';
}
