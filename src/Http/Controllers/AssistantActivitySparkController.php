<?php

namespace Nitm\Assistant\Http\Controllers;

use Response;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Nitm\Assistant\Models\AssistantActivity;

class AssistantActivitySparkController extends SparkController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $slug = 'assistant-activity';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('assistant::activity.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        $ret_val = AssistantActivity::with(['user'])
            ->orderBy('id', 'desc')
            ->paginate(20)
            ->toArray();
        $ret_val['filters'] = $this->getFilters();
        return $ret_val;
    }

    protected function getFilters()
    {
        return [];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('assistant-activity');
    }
}
