<?php

namespace Nitm\Assistant\Http\Controllers;

use Nitm\Assistant\Http\Controllers\AppBaseController;

class AssistantController extends AppBaseController
{
    protected $slug = 'assistants';
}
