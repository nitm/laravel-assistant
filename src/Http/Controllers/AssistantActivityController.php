<?php

namespace Nitm\Assistant\Http\Controllers;

use Nitm\Assistant\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;

class AssistantActivityController extends AppBaseController
{
    protected $slug = 'assistant-activity';

    /**
     * @inheritDoc
     */

    public function index(Request $request, $options = [])
    {
        $options = array_merge([
            'orderBy' => 'id',
            'sortedBy' => 'desc'
        ], $options);
        return parent::index($request, $options);
    }
}
