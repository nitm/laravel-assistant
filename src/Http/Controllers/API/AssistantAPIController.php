<?php

namespace Nitm\Assistant\Http\Controllers\API;

use Request;
use Nitm\Assistant\Http\Requests\API\CreateAssistantAPIRequest;
use Nitm\Assistant\Contracts\Assistant;
use Response;

/**
 * Class AssistantController
 * @package Nitm\Assistant\Http\Controllers\API
 */

class AssistantAPIController extends APIBaseController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct($withAuth = false)
    {
        parent::__construct($withAuth);
    }

    /**
     * Store a newly created Assistant in storage.
     * POST /assistant
     *
     * @param CreateAssistantAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAssistantAPIRequest $request, Assistant $assistant)
    {
        return Response::json($assistant->handle($request));
    }

    /**
     * Store a newly created Assistant in storage.
     * POST /assistant
     *
     * @param CreateAssistantAPIRequest $request
     *
     * @return Response
     */
    public function create(CreateAssistantAPIRequest $request)
    {
        return $this->store($request);
    }

    /**
     * Handle creating and saving agreements
     *
     * @param Request $request
     * @param [type] $type
     * @param [type] $id
     *
     * @return void
     */
    public function agreement(Request $request, $type, $id)
    {
        $user = auth()->user();

        assert($id === $user->id);

        $user->saveAgreement($type, $request->all());

        return Response::json($user);
    }
}