<?php

namespace Nitm\Assistant\Http\Controllers;

use Nitm\Assistant\Http\Controllers\AppBaseController;

class AssistantInteractionQueryController extends AppBaseController
{
    protected $slug = 'assistant-interaction-queries';
}
