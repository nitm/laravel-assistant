<?php

namespace Nitm\Assistant\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Nitm\Assistant\Traits\AppController;

class ApiController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, AppController;

    public function __construct()
    {
        $this->repository = $this->getRepository();
    }
}
