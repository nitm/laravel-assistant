<?php

namespace Nitm\Assistant\Http\Requests\API;

use App\Assistant\Models\AssistantInteraction;
use InfyOm\Generator\Request\APIRequest;

class CreateAssistantInteractionAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return AssistantInteraction::$rules;
    }
}
