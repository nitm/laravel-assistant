<?php

namespace Nitm\Assistant\Console\Commands;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Validator;
use Nitm\Assistant\Jobs\LogJob;

class BaseCommand extends Command
{
    protected $logMessages = [];
    protected $logImmediately = true;

    public function __destruct()
    {
        $this->flushLog();
    }
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // Need to try catch due to deployment issues since DB is not available on docker image
        try {
            $class = config('assistant.userClass', 'Nitm\Assistant\Models\User');
            $user = $class::where('email', 'solidariti@solidariti.us')->first();
            if ($user instanceof User) {
                request()->merge(['user' => $user]);
                //add this
                request()->setUserResolver(function () use ($user) {
                    return $user;
                });
                auth()->login($user);
            }
        } catch (\Exception $e) {
            \Log::warning($e);
        }
        date_default_timezone_set(config('app.timezone'));
    }

    /**
     * Return a signature for this command
     *
     * @return string
     */
    public function getSignature(): string
    {
        $signature = explode(' ', $this->signature);
        return Str::slug(Str::camel($signature[0]));
    }

    /**
     * Add a line to a log
     *
     * @param string|Stringable $string
     * @param string $style
     * @param [type] $verbosity
     * @return void
     */
    public function line($string, $style = null, $verbosity = null)
    {
        return parent::line('[' . Carbon::now() . '] ' . $string, $style, $verbosity);
    }

    /**
     * Validate inputs
     *
     * @param array $params
     * @param array $rules
     * @return boolean
     */
    protected function validate(array $params = [], array $rules = []): bool
    {
        $validator = Validator::make($params, $rules);
        if ($validator->fails()) {
            $this->info("Arguments invalid. See [{$validator->errors()->count()}] error messages below:");

            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return false;
        }
        return true;
    }

    /**
     * Log a lessage
     *
     * @param string $message
     * @param boolean $verboseOnly
     * @param string $level
     * @return void
     */
    protected function log(string $message, bool $verboseOnly = false, $level = 'info')
    {
        if ($this->output->isVerbose() && $verboseOnly === true || $verboseOnly === false) {
            // show verbose messages
            if ($this->logImmediately || !$this->output->isQuiet()) {
                $this->$level($message);
            } else {
                $this->logMessages[] = [$level, $message];
            }
        }
    }

    /**
     * Flush the messge log queue
     *
     * @return void
     */
    protected function flushLog()
    {
        if (!empty($this->logMessages)) {
            LogJob::dispatch($this->signature, $this->logMessages);
        }
        $this->logMessages = [];
    }
}