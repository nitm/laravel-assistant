<?php

namespace Nitm\Assistant\Console\Commands;

use Illuminate\Support\Arr;
use Illuminate\Foundation\Auth\User;
use Nitm\Assistant\Contracts\Assistant;

class TestInteractions extends BaseCommand
{
    /**
     * The assistant handler model
     * @var \Nitm\Assisstant\Assistant
     */
    protected $assistant;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nitm-assistant:test {user? : The ID or email for the user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test interactions for a user';

    /**
     * [__construct description]
     * @param Assistant $assistant [description]
     */
    public function __construct(Assistant $assistant)
    {
        parent::__construct();
        $this->assistant = $assistant;
    }

    protected function getAssistant()
    {
        if (!isset($this->assistant)) {
            $this->assistant = app('App\Assistant\Assistant');
        }
        return $this->assistant;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->assistant) {
            $this->error('Assistant class is not found. Did you publish the vendor files?');
        }
        $this->info("Assistant handling interaction...");
        $user = $this->argument('user') ?: $this->ask("Enter the username/email/id");
        $class = config('assistant.userClass', 'App\User');
        $query = $class::customWith()->search($user, true);
        $model = $query->first();
        if ($model instanceof User) {
            $this->info("Found: " . $model->name);
            $result = $this->assistant->interactWith($model, true);
            if (Arr::get($result, 'noInteraction') === true) {
                $this->warn("Didn't send message: " . array_get($result, 'error', "No interaction available"));
            } elseif (Arr::get($result, 'success') != true) {
                $this->error("Couldn't send message: " . array_get($result, 'error', "Unspecified Error"));
            } else {
                $this->info("Sent message!\n" . json_encode($result['result'], JSON_PRETTY_PRINT));
            }
        } else {
            $this->warn("Couldn't find user [$user]");
        }
    }
}