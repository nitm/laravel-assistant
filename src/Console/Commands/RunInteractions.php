<?php

namespace Nitm\Assistant\Console\Commands;

use Illuminate\Support\Arr;
use Illuminate\Foundation\Auth\User;
use Nitm\Assistant\Contracts\Assistant;

class RunInteractions extends BaseCommand
{
    /**
     * The assistant handler user
     * @var \Nitm\Assisstant\Assistant
     */
    protected $assistant;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nitm-assistant:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run all available interactions';

    /**
     * [__construct description]
     * @param Assistant $assistant [description]
     */
    public function __construct()
    {
        parent::__construct();
        $this->assistant = $this->getAssistant();
        $this->logImmediately = false;
    }

    protected function getAssistant()
    {
        if (!isset($this->assistant)) {
            $this->assistant = app('App\Assistant\Assistant');
        }
        return $this->assistant;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->assistant) {
            $this->error('Assistant class is not found. Did you publish the vendor files?');
        }
        $logMessages = [];
        /**
         * TODO: Filter users more itnelligently. Maybe should get this from the Assistant object?
         */
        $class = config('assistant.userClass', 'App\User');
        $users = $class::whereHas('smsAgreement', function ($query) {
            $query->isActive();
        })->customWith()->get();
        $users->map(function ($user) use ($logMessages) {
            $result = $this->assistant->interactWith($user, true);
            $this->log("Found: " . $user->name, true);
            if (Arr::get($result, 'noInteraction') === true) {
                $this->log("Didn't send message: " . Arr::get($result, 'error', "No interaction available"), true, 'warn');
            } elseif (Arr::get($result, 'success') != true) {
                $this->log("Couldn't send message: " . Arr::get($result, 'error', "Unspecified Error"), false, 'error');
            } else {
                if (empty($this->logMessages)) {
                    $this->log("Assistant handling interactions...");
                }
                $this->log("Sent message to {$user->name} ({$user->email} | {$user->phone}): {$this->assistant->getResponse()->getMessage()}");
            }
            $this->assistant->reset();
        });
    }
}