<?php

namespace Nitm\Assistant\Console\Commands;

use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Nitm\Assistant\Contracts\Assistant;
use Nitm\Assistant\Models\AssistantInteraction;

class PruneInteractions extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nitm-assistant:prune-interactions
        {unit=1 : The number of [days, hours,...etc} {interval=hour : The interval [day|hours|minutes]}
        {userUnit=1 : The number of [days, hours,...etc} {userInterval=hour : The interval [day|hours|minutes]}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire stale interactions. By default clears those older than the last hour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $status = AssistantInteraction::STATUS_IN_PROGRESS;
        $unit = $this->argument('unit', 1);
        $interval = $this->argument('interval', 'hour');
        $userUnit = $this->argument('userUnit', 4);
        $userInterval = $this->argument('userInterval', 'hours');
        date_default_timezone_set(config('app.timezone'));
        if ($this->validate([
            'unit' => $unit,
            'interval' => $interval,
            'userUnit' => $unit,
            'userInterval' => $interval
        ], [
            'unit' => 'required|integer|min:1',
            'interval' => [
                'required',
                Rule::in($this->getIntervals())
            ],
            'userUnit' => 'required|integer|min:1',
            'userInterval' => [
                'required',
                Rule::in($this->getIntervals())
            ]
        ])) {
            $createdAt = "assistant_interactions.created_at";
            $expiresAt = "assistant_interactions.created_at + interval '{$unit} {$interval}'";
            $userExpiresAt = "assistant_interactions.created_at + interval '{$unit} {$interval}'";
            $now = 'now()';
            $query = AssistantInteraction::select([
                'assistant_interactions.id',
                'assistant_interactions.status',
                'assistant_interactions.user_id',
                \DB::raw("{$createdAt} as user_interaction_created_at"),
                \DB::raw("{$expiresAt} as expires_at"),
                \DB::raw("{$now} as now")
            ])
                ->where(
                    function ($query) use ($now, $expiresAt) {
                        /**
                         * Prune server interactions greate than $unit $interval
                         */
                        $query->whereStartedBy(Assistant::SERVER_CONTEXT)
                            ->where(function ($query) use ($now, $expiresAt) {
                                $query->where(\DB::raw($now), '>=', \DB::raw($expiresAt))
                                    ->orWhere(\DB::raw('now()'), '>=', \DB::raw($expiresAt));
                            });
                    }
                )
                ->orWhere(
                    function ($query) use ($now, $userExpiresAt) {
                        /**
                         * Prune user interactions greater than $userUnit $userInterval
                         */
                        $query->whereStartedBy(Assistant::USER_CONTEXT)
                            ->where(function ($query) use ($now, $userExpiresAt) {
                                $query->where(\DB::raw($now), '>=', \DB::raw($userExpiresAt))
                                    ->orWhere(\DB::raw('now()'), '>=', \DB::raw($userExpiresAt));
                            });
                    }
                )
                ->where(
                    'assistant_interactions.status',
                    $status
                );

            $count = $query->count();

            if ($count) {
                $query->update(['status' => AssistantInteraction::STATUS_EXPIRED]);
                $this->log("Expired {$count} [$status] interactions older than {$unit} {$interval}");
            } else {
                $this->log("No interactions to clear");
            }
        }
    }

    /**
     * Helper function to print the interactions to be expired
     *
     * @param [type] $query
     * @return void
     */
    protected function printTable($query)
    {
        $data = $query->get()->transform(function ($i) {
            return $i->toArray();
        });
        $this->table(array_keys((array)$data->get(0)), $data->toArray());
    }

    /**
     * Get the supported intervals
     *
     * @return array
     */
    protected function getIntervals(): array
    {
        $intervals = ['second', 'minute', 'hour', 'day', 'week', 'month', 'year'];
        return array_merge($intervals, array_map([Str::class, 'plural'], $intervals));
    }
}