<?php

namespace Nitm\Assistant\Traits;

use Nitm\Assistant\Models\AssistantInteraction;

trait AssistantItemTrait
{
    /**
     * Get all of the interactions for this item
     */
    public function assistantInteractions()
    {
        return $this->morphMany(AssistantInteraction::class, 'item', null, null, 'item_class');
    }
}
