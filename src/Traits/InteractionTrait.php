<?php

namespace Nitm\Assistant\Traits;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Nitm\Assistant\Models\AssistantInteraction;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Traits for use on a content model
 * @author Malcolm Paul <malcolm@ninjasitm.com>
 */

trait InteractionTrait
{
    /**
     * Get all interactions
     *
     * @return MorphMany
     */
    public function assistantInteractions()
    {
        return $this->morphMany(AssistantInteraction::class, 'item', 'item_class');
    }

    /**
     * Get all interactions in progress
     *
     * @return MorphMany
     */
    public function assistantInteractionsInProgress()
    {
        return $this->assistantInteractions()
            ->whereStatus(AssistantInteraction::STATUS_IN_PROGRESS);
    }

    /**
     * Get all interactions completed
     *
     * @return MorphMany
     */
    public function assistantInteractionsCompleted()
    {
        return $this->assistantInteractions()
            ->whereStatus(AssistantInteraction::STATUS_COMPLETE);
    }

    /**
     * Get all interactions expired
     *
     * @return MorphMany
     */
    public function assistantInteractionsExpired()
    {
        return $this->assistantInteractions()
            ->whereStatus(AssistantInteraction::STATUS_EXPIRED);
    }

    /**
     * Get the response rate distribution for goals
     *
     * @param [type] $query
     * @return void
     */
    public function scopeAssistantInteractionRate($query, $itemClass = null)
    {
        $table = $this->getTable();
        $itemClass = $itemClass ?: get_class($this);
        $query->selectRaw('COUNT(assistant_interactions.id) as response_count')
            ->join("assistant_interactions", function ($join) use ($table, $itemClass) {
                $join->on("assistant_interactions.item_id", "=", $table.".id")
                    ->where("item_class", $itemClass);
            })
            ->groupBy($table.".id");
    }

    /**
     * Group the ionteraction rate by day
     *
     * @param [type] $query
     * @return void
     */
    public function scopeAssistantInteractionRateByDay($query, $itemClass = null)
    {
        $query->assistantInteractionRate($itemClass)
            ->selectRaw("to_char(assistant_interactions.created_at, 'YYYY-MM-DD') as rate_interval")
            ->groupBy('rate_interval');
    }

    /**
     * Group the ionteraction rate by month
     *
     * @param [type] $query
     * @return void
     */
    public function scopeAssistantInteractionRateByMonth($query, $itemClass = null)
    {
        $query->assistantInteractionRate($itemClass)
            ->selectRaw("to_char(assistant_interactions.created_at, 'YYYY-MM') as rate_interval")
            ->groupBy('rate_interval');
    }

    /**
     * Group the ionteraction rate by year
     *
     * @param [type] $query
     * @return void
     */
    public function scopeAssistantInteractionRateByYear($query, $itemClass = null)
    {
        $query->assistantInteractionRate($itemClass)
            ->selectRaw("to_char(assistant_interactions.created_at, 'YYYY') as rate_interval")
            ->groupBy('rate_interval');
    }

    public function scopeAssistantInteractionInterval($query, $itemClass = null, array $range, $interval = [1, 'day'])
    {
        $start = array_get($range, 'start', Carbon::now()->sub(1, 'month'));
        $end = array_get($range, 'end', Carbon::now());

        list($intervalUnit, $intervalInterval) = $interval;

        switch ($intervalInterval) {
            case 'year':
            $intervalFormat = 'YYYY';
            break;

            case 'month':
            $intervalFormat = 'YYYY-MM';
            break;

            default:
            $intervalFormat = 'YYYY-MM-DD';
            break;
        }

        $intervalInterval = $intervalUnit > 1 ? Str::plural($intervalInterval) : Str::singular($intervalInterval);

        $query->assistantInteractionRate($itemClass)
            ->selectRaw("to_char(assistant_interactions.created_at, '{$intervalFormat}') as rate_interval")
            ->fromSub(
                function ($query) use ($start, $end, $intervalUnit, $intervalInterval) {
                    $query->from(
                        \DB::raw(
                            "SELECT generate_series(timestamp '{$start}',
                            timestamp '{$end}',
                            interval '{$intervalUnit} {$intervalInterval}')"
                        )
                    );
                },
                'response_ts'
            )
        ->groupBy('response_ts.created_at')
        ->orderBy('response_ts.created_at', 'asc');
    }
}
