<?php

namespace Nitm\Assistant\Traits;

use Carbon\Carbon;
use Nitm\Assistant\Models\Assistant;
use Nitm\Assistant\Models\AssistantActivity;
use Nitm\Assistant\Models\AssistantInteraction;
use Nitm\Assistant\Models\AssistantChoice;
use Nitm\Assistant\Models\AssistantAgreement;

/**
 * Traits for use on an Authenticable or User class
 *
 * @author Malcolm Paul <malcolm@ninjasitm.com>
 */
trait AssistantRelationsTrait
{
    /**
     * Get the assistant by either getting the user choice or creating one if it doesn't exist
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ensureAssistant() : Assistant
    {
        $assistant = $this->assistantChoice ? $this->assistantChoice : ($this->assistantChoice()->count() ? $this->assistantChoice()->first() : $this->assistantChoice()->create(
            [
            'assistant_id' => Assistant::select('id')->first()->id
            ]
        ));
        return $assistant->assistant;
    }

    /**
     * Get the user's assistant
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function assistant()
    {
        return $this->hasOneThrough(
            Assistant::class,
            AssistantChoice::class,
            'user_id',
            'id',
            'id',
            'assistant_id'
        );
    }

    /**
     * Get the user's assistant choice
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function assistantChoice()
    {
        return $this->hasOne(AssistantChoice::class, 'user_id')
            ->with(['assistant']);
    }

    /**
     * Get the users interaction activity
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assistantActivity()
    {
        return $this->hasMany(AssistantActivity::class);
    }

    /**
     * Get all interactions
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assistantInteractions()
    {
        return $this->hasMany(AssistantInteraction::class);
    }

    /**
     * Get completed interactions
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assistantInteractionsCompleted()
    {
        return $this->assistantInteractions()
            ->whereStatus(AssistantInteraction::STATUS_COMPLETE);
    }

    /**
     * Get in progress interactions
     *
     * @return Illuminate\Database\Eloquent\Relations\HasHasMany
     */
    public function assistantInteractionsInProgress()
    {
        return $this->assistantInteractions()
            ->whereStatus(AssistantInteraction::STATUS_IN_PROGRESS);
    }

    /**
     * Get expired interactions
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assistantInteractionsExpired()
    {
        return $this->assistantInteractions()
            ->whereStatus(AssistantInteraction::STATUS_EXPIRED);
    }

    /**
     * Get the assistant config
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function assistantSessionConfig()
    {
        return $this->hasOne(AssistantSessionConfig::class, 'user_id');
    }

    /**
     * Get the last interaction
     *
     * @param array $excludeActions
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastInteraction($excludeActions = ['none'])
    {
        return $this->hasOne(AssistantInteraction::class)
            ->whereNotIn('action', (array)$excludeActions)
            ->whereNotNull('action')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the last interaction
     *
     * @param array $excludeActions
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastInteractionForTheDay($excludeActions = ['none'])
    {
        return $this->hasOne(AssistantInteraction::class)
            ->whereNotIn('action', (array)$excludeActions)
            ->whereNotNull('action')
            ->where(
                function ($query) {
                    $query->where('created_at', '>=', Carbon::now()->subDays(1))
                        ->orWhere(
                            function ($query) {
                                $query->whereNotNull('updated_at')
                                    ->where('updated_at', '>=', Carbon::now()->subDays(1));
                            }
                        );
                }
            )
            ->orderBy('id', 'desc');
    }

    /**
     * Get the interaction sessions
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function assisstantSessions()
    {
        return $this->hasMany(AssistantSession::class);
    }

    /**
     * Return the user's journeys
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assistantAgreements()
    {
        return $this->hasMany(AssistantAgreement::class);
    }

    /**
     * Return the user's journeys
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function smsAgreement()
    {
        return $this->hasOne(AssistantAgreement::class)
            ->where('agreement_type', 'sms');
    }

    /**
     * Save the agreement for the user
     *
     * @param  string $type
     * @param  array  $data
     * @return AssistantAgreement
     */
    public function saveAgreement(string $type, array $data = [])
    {
        $agreement = $this->assistantAgreements()
            ->whereAgreementType($type)
            ->first();
        if (!($agreement instanceof AssistantAgreement)) {
            $agreement = new AssistantAgreement(
                array_merge(
                    [
                    'agreement_type' => $type,
                    'signature' => $signature ?? $this->name,
                    'ip' => request()->ip(),
                    'created_at' => Carbon::now()
                    ], $data
                )
            );
            $this->assistantAgreements()->save($agreement);
        } else if($agreement instanceof AssistantAgreement) {
            $agreement->fill($data);
            $agreement->save();
        }
        return $agreement;
    }

    /**
     * Properly parse and set the phone number
     *
     * @param  string|int $number
     * @return void
     */
    public function setPhoneAttribute($number)
    {
        if (strlen($number)) {
            $this->attributes['phone'] = app()->environment('testing') ? '+'.ltrim($number, '+') : phone($number, config('assistant.supportedCountries'), 'e164');
        }
    }

    /**
     * Get the user current timestamp in their timezone
     *
     * @return Carbon
     */
    public function getNow(): Carbon
    {
        return Carbon::now($this->timezone);
    }

    /**
     * Get the current hour for the user
     *
     * @return Carbon
     */
    public function getNowHour(): string
    {
        return $this->getNow()->format('H');
    }
}
