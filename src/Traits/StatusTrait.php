<?php

namespace Nitm\Assistant\Traits;

use Nitm\Assistant\Models\AssistantInteraction;

trait StatusTrait
{

    /**
     * Set the priority for the attribute
     *
     * @param string $value
     * @return string
     */
    public function setPriorityAttribute(string $value = null)
    {
        $this->attributes['priority'] = static::getPriorityOptions($value);
    }

    /**
     * Set the status for the attribute
     *
     * @param string $value
     * @return string
     */
    public function setStatusAttribute(string $value = null)
    {
        $this->attributes['status'] = static::getStatusOptions($value);
    }

    /**
     * Get the right priority
     *
     * @param string $key
     * @return string
     */
    public static function getPriorityOptions(string $key = null)
    {
        $options = [
            'low' => 'low', 'medium' => 'medium', 'high' => 'high'
        ];

        return $key ? array_get($options, $key, 'medium') : $options;
    }

    /**
     * Get the right status
     *
     * @param string $key
     * @return void
     */
    public static function getStatusOptions(string $key = null, $returnNull = false)
    {
        $options = [
            AssistantInteraction::STATUS_IN_PROGRESS,AssistantInteraction::STATUS_COMPLETE,
            AssistantInteraction::STATUS_FAILED,
            AssistantInteraction::STATUS_EXPIRED
        ];

        $options= array_combine($options, $options);

        return $key ? array_get($options, $key, AssistantInteraction::STATUS_IN_PROGRESS) : ($returnNull ? null : $options);
    }

    /**
     * Undocumented function
     *
     * @param [type] $query
     * @param [type] $status
     * @return void
     */
    public function scopeFilterByStatus($query, $statuses, bool $inclusive = false)
    {
        $method = $inclusive ? 'orWhereIn' : 'whereIn';
        $statuses = array_filter((array)$statuses, function ($key) {
            return static::getStatusOptions($key, true);
        });
        $query->$method('status', $statuses);
    }
}
