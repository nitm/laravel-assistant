<?php

namespace Nitm\Assistant\Traits;

trait ObjectTrait
{
    /**
     * Fill the model with an array of attributes.
     *
     * @param  array $attributes
     * @return $this
     */
    public function fill(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            // The developers may choose to place some attributes in the "fillable" array
            // which means only those attributes may be set through mass assignment to
            // the model, and all others will just get ignored for security reasons.
            $this->$key = $value;
        }
        return $this;
    }
}
