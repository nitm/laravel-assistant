<?php

namespace Nitm\Assistant\Traits;

use Response;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\DataType;
use InfyOm\Generator\Utils\ResponseUtil;

/**
 * Traits for Nyco Content Model.
 */
trait AppController
{
    protected $slug;
    protected $repository;

    /**
     * Send a json response
     *
     * @param [type] $result
     * @param [type] $message
     * @return Response
     */
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    /**
     * Send an error
     *
     * @param [type] $error
     * @param integer $code
     * @return Response
     */
    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    /**
     * Get the view slug
     *
     * @param [type] $slug
     * @return void
     */
    protected function getViewSlug($slug)
    {
        return Str::replace(['-'], ['_'], Str::plural(Str::kebab($slug)));
    }

    /**
     * Get the slug
     *
     * @param Request $request
     * @param [type] $plural
     * @return void
     */
    public function getSlug(Request $request, $plural = null, $slug = null)
    {
        if (\App::runningInConsole()) {
            return $slug ?: $this->slug ?: '';
        }
        if (!$slug && isset($this->slug)) {
            $slug = $this->slug;
        } elseif (!$slug) {
            $parts = explode('.', $request->route()->getName());
            $slug = array_pop($parts);
            $slug = in_array($slug, [
                'index', 'create', 'edit', 'update', 'show', 'destroy', 'delete', 'store'
            ]) ? array_pop($parts) : $slug;
        }

        return $plural === true ? Str::plural($slug) : ($plural === false ? Str::singular($slug) : $slug);
    }

    /**
     * get the data type
     *
     * @param [type] $slug
     * @return DataType
     */
    protected function getDataType($slug)
    {
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        if (!$dataType) {
            $dataType = new DataType([
                'name' => $slug,
                'slug' => $slug,
                'display_name_singular' => $slug,
                'display_name_plural' => $slug,
                'icon' => '',
                'model_name' => $this->getModelClass($slug)
            ]);
        }
        // $parts = explode('-', $dataType->slug);
        // $dataType->slug = count($parts) ? array_pop($parts) : $dataType->slug;

        $dataType->slug = Str::plural(Str::replace(['_'], ['-'], ($dataType->slug)));
        $dataType->display_name_singular = Str::title($dataType->display_name_singular);
        $dataType->display_name_plural = Str::plural(Str::title($dataType->display_name_plural));
        return $dataType;
    }

    /**
     * Get the model class
     *
     * @param [type] $slug
     * @return string
     */
    protected function getModelClass($slug)
    {
        if (isset($this->modeClass)) {
            return $this->modelClass;
        }

        $modelClass = '\\App\\Models\\'.ucfirst(Str::camel(Str::singular($slug)));
        return $modelClass;
    }

    /**
     * Get the repository
     *
     * @param [type] $slug
     * @return Repository
     */
    protected function getRepository($slug = null)
    {
        $slug = $slug ?: $this->getSlug(request());
        $modelClass = $this->getModelClass($slug);
        $repositoryClass = '\\Nitm\\Assistant\\Repositories\\'.class_basename($modelClass).'Repository';
        if (class_exists($repositoryClass)) {
            return app()->make($repositoryClass);
        }
        if(!app()->runningInConsole()) {
            throw new \Exception("Repository doesn't exist: ".$repositoryClass);
        }
    }
}
