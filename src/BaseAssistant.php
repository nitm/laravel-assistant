<?php

namespace Nitm\Assistant;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticable;
use Illuminate\Eloquent\Collection;
use Illuminate\Foundation\Auth\User;
use Nitm\Assistant\Models\AssistantSession;
use Nitm\Assistant\Models\AssistantInteraction;
use Nitm\Assistant\Models\Assistant as AssistantModel;
use Nitm\Assistant\Jobs\ProcessAssistantInteraction as Job;
use Nitm\Assistant\Providers\Interactions\VonageInteraction;
use Nitm\Assistant\Providers\Interactions\TwilioInteraction;
use Nitm\Assistant\Providers\Interpreters\TwilioInterpretation;
use Nitm\Assistant\Providers\Interpreters\VonageInterpretation;
use Nitm\Assistant\Providers\Interactions\DialogFlowInteraction;
use Nitm\Assistant\Providers\Interactions\BaseInteractionProvider;
use Nitm\Assistant\Providers\Interpreters\DialogFlowInterpretation;
use Nitm\Assistant\Providers\Interpreters\BaseInterpretationProvider;

/**
 * The base assistant class used to generate and interact with users
 */
abstract class BaseAssistant implements Contracts\Assistant
{
    const USER_CONTEXT = 'user';
    const SERVER_CONTEXT = 'server';
    const QUERY_INTERACTION_CONTEXT = 'query';
    const RESPONSE_INTERACTION_CONTEXT = 'response';
    const NO_INTERACTION = 'none';

    /**
     * The language for the assistant
     *
     * @var string
     */
    public $languageCode = 'en-US';

    /**
     * Who started this interaction?
     *
     * @var string
     */
    protected $startedBy = 'server';

    /**
     * The interaction method
     *
     * @var string
     */
    protected $method = 'sms';

    /**
     * The request
     *
     * @var Request
     */
    protected $request;

    /**
     * The interpreter that handes the request
     *
     * @var BaseInterpretationProvider
     */
    protected $interpreter;

    /**
     * The interpreter that handes the request
     *
     * @var BaseInteractionProvider
     */
    protected $provider;

    /**
     * String the type of the interpreter
     *
     * @var string
     */
    protected $interpreterType;

    /**
     * The type of the interaction
     *
     * @var string
     */
    protected $interactionType;

    /**
     * AssistantSession
     *
     * @var AssistantSession
     */
    protected $session;

    /**
     * The user
     *
     * @var Authenticable
     */
    protected $user;

    /**
     * Can we continue intneractions on finishing the curent one?
     *
     * @var boolean
     */
    protected $canContinueInteractions = false;

    /**
     * Can we continue intneractions on finishing the curent one?
     *
     * @var boolean
     */
    protected $shouldSkipCurrentInteraction = false;

    /**
     * AssistantInteraction
     *
     * @var AssistantInteraction
     */
    public $interaction;

    /**
     * The next interaction
     *
     * @var AssistantInteraction
     */
    public $nextInteraction;

    /**
     * The last interaction the user performed. THis is fetched directly from the atabase
     *
     * @var AssistantInteraction
     */
    public $lastInteraction;

    /**
     * The previous interaction
     *
     * @var AssistantInteraction
     */
    public $previousInteraction;

    public function __construct(Request $request, string $interpreterType = null)
    {
        $this->request = $request;
        $this->interpreterType = $interpreterType;
    }

    /**
     * Handle translation strings
     *
     * @param  string $messageKey
     * @param  array  $attributes
     * @return string
     */
    public static function __($messageKey, $attributes = []): string
    {
        $from = trans($messageKey);
        $message = $from ?: $messageKey;
        if (is_array($from)) {
            $message = $from[rand(0, count($from) - 1)];
        }
        return __($message, $attributes);
    }

    /**
     * Handle a response from an API request
     *
     * @return [type] [description]
     */
    public function handle()
    {
        $this->setInteractionTypeStartedBy(static::RESPONSE_INTERACTION_CONTEXT, static::USER_CONTEXT);
        $provider = $this->getProvider();
        $params = $provider->process();
        $interaction = $this->getCurrentInteraction(null, Arr::get($params, 'action', $provider->getUnknownInput()));
        \Log::info("Original params: " . json_encode(request()->all()));
        if ($interaction instanceof AssistantInteraction) {
            $result = [];

            /**
             * Parse the request and extract relavant information using the response $provider
             */

            /**
             * Find the latest sessions that hasn't been completed for the number in the message
             */
            $id = Arr::get($params, 'id', -1);
            $attributes = Arr::get($params, 'attributes', []);
            switch ($params['action']) {
                case Actions::SEND_SUMMARY:
                    $this->sendSummary();
                    break;

                case Actions::CONTINUE_SESSION:
                    $result = $this->continueSession($params);
                    break;

                case Actions::END_SESSION:
                    $result = $this->completeSession($params);
                    break;

                    /**
                     * Most likely an unknown input or a custom input
                     *
                     * @var [type]
                     */
                default:
                    $result = $this->handleFromUser($interaction, $provider, $params);
                    $result = empty($result) ? [
                        static::RESPONSE_INTERACTION_CONTEXT => Arr::get($params, 'message')
                    ] : $result;
                    break;
            }

            extract($result);

            if (!$provider->isUnknownInput(Arr::get($result, 'action'))) {
                $this->updateInteraction(
                    [
                        'action' => Arr::get($result, 'action')
                    ]
                );
            }

            if (Arr::get($result, 'endInteraction') === true) {
                $result = array_merge($result, $this->completeInteraction());
            }

            if (Arr::get($result, 'endSession') === true) {
                $result = array_merge($result, $this->completeSession($params));
            }

            /**
             * Should we respond directly to the user?
             */
            if (isset($response) && $response) {
                \App\Helpers\DebugHelper::backtrace([__CLASS__, __LINE__, $response]);
                \Log::error("Interaction generated an unwanted response:\n" . $response);
                $retVal = $provider->interact($response, $params);
            } else {
                /**
                 * Send the  action to the response $provider so that it knows how to format the result
                 */
                $retVal = $provider->formatResponse($result, $params, true);
            }
            if ($this->isInteractionCompleted()) {
                $this->setCanContinueInteractions();
                $retVal = $this->processNextInteractionIfPossible($retVal);
            }
        } else {
            $retVal = $provider->formatResponse($this->handleNoInteraction(), []);
        }

        return $retVal;
    }

    /**
     * Start inteactions for a collection of users
     *
     * @param  Collection $users [description]
     * @return [type]            [description]
     */
    public static function startInteractionFor(Collection $users)
    {
        /**
         * TODO: Test beginning interaction. Need to figure out how to associate a message with the user. May possible do it using the message ID
         *
         * @var [type]
         */
        // $assistant = new static(request());
        // $users->split(config('assistant.groupCount'))->map(function ($user) use($assistant) {
        //     $assistant->interactWith($user);
        // });
    }

    /**
     * Can the interactions continue?
     *
     * @return boolean
     */
    protected function canContinueInteractions()
    {
        return $this->canContinueInteractions === true;
    }

    /**
     * Set that the interactions can continue after the current interaction is finished
     *
     * @return boolean
     */
    protected function setCanContinueInteractions()
    {
        $this->canContinueInteractions = true;
    }

    /**
     * Set that the interactions cannot continue after the current interaction is finished
     *
     * @return boolean
     */
    protected function setCannotContinueInteractions()
    {
        $this->canContinueInteractions = false;
    }

    /**
     * Can the interactions continue?
     *
     * @return boolean
     */
    public function shouldSkipCurrentInteraction()
    {
        return $this->shouldSkipCurrentInteraction === true;
    }

    /**
     * Set that the interactions can continue after the current interaction is finished
     *
     * @return boolean
     */
    public function setShouldSkipCurrentInteraction()
    {
        $this->shouldSkipCurrentInteraction = true;
    }

    /**
     * Set that the interactions cannot continue after the current interaction is finished
     *
     * @return boolean
     */
    public function setShouldNotSkipCurrentInteraction()
    {
        $this->shouldSkipCurrentInteraction = false;
    }

    /**
     * [interactWith description]
     *
     * @param  User  $user              The user
     * @param  bool  $finishImmediately complete the interaction right away?
     * @param  array $params            The params to use for generating interaction response
     * @param  bool  $returnResult      Return the result instead
     * @return mixed
     */
    public function interactWith($user = null, bool $finishImmediately = false, array $params = [], bool $returnResult = false)
    {
        $this->setInteractionTypeStartedBy(static::QUERY_INTERACTION_CONTEXT, static::SERVER_CONTEXT);
        $user = $this->resolveUser($user);
        if (!empty($options = Arr::get($params, 'interpreter'))) {
            $this->getInterpreter()->fill($options);
        }
        $this->nextInteraction = null;
        $interaction = $this->getCurrentInteraction($user);
        $result = $attributes = [];
        $message = null;
        if (!$interaction) {
            $finishImmediately = true;
            $message = __('assistant.sessionComplete', []);
        } else {
            $result = $this->handleFromServer($user, $interaction);
            // Do this here as any updated interaction should be set by deriving classes otherwise the original interaction may not propagate
            $interaction = $this->getCurrentInteraction();
            if (!$interaction) {
                $this->setShouldSkipCurrentInteraction();
            } else if ($interaction->item) {
                if (!empty($updateInteraction)) {
                    $this->updateInteraction(
                        [
                            'item_type' => class_basename($interaction->item),
                            'item_class' => get_class($interaction->item),
                            'item_id' => $interaction->id
                        ]
                    );
                }
            }
        }

        if (Arr::get($result, 'endInteraction') === true) {
            $result = array_merge($result, $this->completeInteraction());
        }

        if (Arr::get($result, 'endSession') === true) {
            $result = array_merge($result, $this->completeSession($params));
        }

        if ($this->shouldSkipCurrentInteraction() || empty($result)) {
            $this->reset();
            return [
                'noInteraction' => true
            ];
        }

        extract($result);

        if ($finishImmediately) {
            $this->getInterpreter()->fill($attributes);
            if (!($interaction instanceof AssistantInteraction) || $interaction && $interaction->action === static::NO_INTERACTION) {
                return [
                    'error' => "No interaction available",
                    'noInteraction' => true
                ];
            } else {
                return $this->finish($interaction, $message, $attributes);
            }
        } else if (!$returnResult) {
            Job::dispatch($interaction, $message, $attributes);
        }
        return compact('message', 'attributes', 'interaction');
    }

    /**
     * Reset the interaction in order to support multiple users
     */
    public function reset()
    {
        $this->interaction = $this->nextInteraction = $this->previousInteraction = $this->user = $this->provider = $this->interpreter = null;
        $this->setShouldNotSkipCurrentInteraction();
        $this->setCannotContinueInteractions();
    }

    /**
     * Add activity information to the interaction
     *
     * @param  array $params
     * @return void
     */
    public function addActivityParams(array $params)
    {
        foreach ($params as $property => $value) {
            $this->getProvider()->set($property, $value);
        }
    }

    /**
     * Set that this is a server interaction
     *
     * @param  string $type      The interaction type
     * @param  string $startedBy Who started the interaction
     * @return void
     */
    public function setInteractionTypeStartedBy(string $type, string $startedBy)
    {
        $this->interactionType = $type === static::QUERY_INTERACTION_CONTEXT ? static::QUERY_INTERACTION_CONTEXT : static::RESPONSE_INTERACTION_CONTEXT;
        $this->startedBy = $startedBy === static::SERVER_CONTEXT ? static::SERVER_CONTEXT : static::USER_CONTEXT;
    }

    /**
     * Is this a server interaction?
     *
     * @return boolean
     */
    protected function isServerInteraction()
    {
        return $this->interactionType === static::QUERY_INTERACTION_CONTEXT;
    }

    /**
     * Is this a user interaction?
     *
     * @return boolean
     */
    protected function isUserInteraction()
    {
        return $this->interactionType === static::RESPONSE_INTERACTION_CONTEXT;
    }

    /**
     * Was this initiated by the server
     *
     * @param  $getLastInteraction
     * @return boolean
     */
    protected function interactionIsServerInitiated(bool $getLastInteraction = false)
    {
        $interaction = $getLastInteraction ? $this->getLastInteraction() : $this->getCurrentInteraction();
        return $interaction instanceof AssistantInteraction && $interaction->started_by === static::SERVER_CONTEXT;
    }

    /**
     * Was this initiated by the user?
     *
     * @param  $getLastInteraction
     * @return boolean
     */
    protected function interactionIsUserInitiated(bool $getLastInteraction = false)
    {
        $interaction = $getLastInteraction ? $this->getLastInteraction() : $this->getCurrentInteraction();
        return $interaction instanceof AssistantInteraction && $interaction->started_by === static::USER_CONTEXT;
    }

    /**
     * This allows the interactions to continue if an interaction is completed and there are more interactions
     *
     * @param  string $message The original message to append to
     * @return string
     */
    protected function processNextInteractionIfPossible($message = null)
    {
        if ($this->canContinueInteractions() && $this->hasNextInteraction() && $this->interactionIsUserInitiated(true) && $this->isUserInteraction()) {
            $this->setCannotContinueInteractions();
            $result = $this->interactWith($this->user, false, [], true);
            if (is_array($result) && Arr::get($result, 'message') !== null) {
                if (is_string($message)) {
                    $message .= ". {$result['message']}";
                    return $message;
                } else if ($message instanceof \Nitm\Assistant\BaseResponseProvider) {
                    $message->appendMessage($result['message']);
                }
            }
        }
        /**
         * TODO: Should we followup an ask if we can help if the user initiated the original interaction?
         */
        return $message;
    }

    /**
     * Finish the interaction
     *
     * @param AsssistantInteraction $interaction
     * @param string                $message
     * @param array                 $attributes
     *
     * @return array
     */
    public function finish(AssistantInteraction $interaction, string $message, array $attributes = []): array
    {
        if ($interaction) {
            if ($interaction->action === static::NO_INTERACTION) {
                return [
                    'noInteractions' => true
                ];
            }

            // try {
            $attributes = $this->getProvider()->interact($message, $attributes);
            // } catch(\Exception $e) {
            // \Log::error($e);
            // return [];
            // }
            $this->getProvider()->finish();
            $attributes['proivder_class'] = Arr::get($attributes, 'provider_class') ?: get_class($this->getProvider());
            $attributes['started_by'] = Arr::get($attributes, 'started_by', $this->startedBy);
            $interaction->fill($attributes);
            $interaction->save();
            $this->interaction = $interaction;
            $this->processNextInteractionIfPossible();
            return $this->getProvider()->getResult();
        }
        return [];
    }

    public function setInteractionType(string $type)
    {
        $this->interactionType = $type;
    }

    /**
     * Get the interaction type
     *
     * @return string
     */
    public function getInteractionType(): string
    {
        return $this->interactionType ?: static::RESPONSE_INTERACTION_CONTEXT;
    }

    /**
     * Does the assistant have the specificed interpreter
     */
    public function hasInterpreter($name): bool
    {
        $name = $name ?: static::NO_INTERACTION;
        return !empty(Arr::get(static::getInterpreters(), $name, null));
    }

    /**
     * Load an interpreter
     *
     * @param  [type] $name
     * @return BaseInterpretationProvider
     */
    public function loadInterpreter(string $name = null): BaseInterpretationProvider
    {
        $interpreterName = $name ?: config('assistant.interpreter');
        $params = Arr::get(static::getInterpreters(), $interpreterName, null);
        list('class' => $interpreterClass) = $params;
        if ($interpreterClass && class_exists($interpreterClass)) {
            $interpreter =  new $interpreterClass([], $this);

            return $interpreter;
        } else {
            abort(404, "Couldn't find the interpreter (${interpreterClass}) for this request");
        }
    }

    /**
     * Get the assistant interpreter
     *
     * @return BaseInterpretationProvider
     */
    public function getInterpreter(): BaseInterpretationProvider
    {
        if (!isset($this->interpreter)) {
            $this->interpreter = $this->loadInterPreter();
            $this->interpreter->fill(
                [
                    'interaction' => $this->getCurrentInteraction()
                ]
            );
        }
        return $this->interpreter;
    }

    /**
     * Does the specified provider exist?
     *
     * @param  [type] $name
     * @return bool
     */
    public function hasProvider($name): bool
    {
        $name = $name ?: static::NO_INTERACTION;
        return !empty(Arr::get(static::getProviders(), $name, null));
    }

    /**
     * Load a provider
     *
     * @param  [type] $name
     * @param  array  $attributes
     * @return BaseInteractionProvider || Error
     */
    public function loadProvider(string $name = null, $attributes = []): BaseInteractionProvider
    {
        $this->interaction = null;
        $providerName = $name ?: ($this->interactionType === static::QUERY_INTERACTION_CONTEXT ? config('assistant.queryProvider') : config('assistant.provider'));

        $params = Arr::get(static::getProviders(), $providerName, null);
        list('class' => $providerClass) = $params;
        if ($providerClass && class_exists($providerClass)) {
            $provider = new $providerClass([], $this);
            $provider->fill(
                array_merge(
                    [
                        'assistant' => $this
                    ],
                    $attributes
                )
            );
            return $provider;
        } else {
            abort(404, "Couldn't find the provider (${providerClass}) for this request");
        }
    }

    /**
     * Get the assistant interaction provider
     *
     * @return [type] [description]
     */
    public function getProvider($provider = null)
    {
        if (!isset($this->provider)) {
            $this->provider = $this->loadProvider($provider);
            $user = $this->resolveUser();
            if ($user instanceof User) {
                $this->setUser($user);
                if (isset($this->user)) {
                    $this->provider->setUser($this->user);
                }
            }

            $this->provider->fill(
                [
                    'interaction' => $this->getCurrentInteraction(),
                    'interpreter' => $this->getInterpreter()
                ]
            );
        }
        return $this->provider;
    }

    /**
     * Get the response
     *
     * @return BaseResponseProvider
     */
    public function getResponse(): BaseResponseProvider
    {
        return $this->getProvider()->getResponse();
    }

    /**
     * Get the current session for the user
     *
     * @param  [type] $user [description]
     * @return [type]       [description]
     */
    public function getSession($user = null)
    {
        $user = $this->resolveUser($user);
        $assistant = $user->assistant ?: $this->getAssistantPersona($user);
        $hour = $user->getNowHour();
        $existingSession = $assistant->sessions()->where(
            [
                'user_id' => $user->id,
                'session_date' => date('Y-m-d'),
                'session_start' => AssistantSession::getStartTime(),
                'session_end' => AssistantSession::getEndTime()
            ]
        )->first();

        $ret_val = null;
        if ($existingSession) {
            $ret_val = $existingSession;
        } else {
            $session =  $assistant->sessions()->create(
                [
                    'user_id' => $user->id,
                ]
            );
            $assistant->sessions()->save($session);
            $ret_val = $session;
        }

        $ret_val->setRelation(static::USER_CONTEXT, $user);
        return $ret_val;
    }


    /**
     * [resolveUser description]
     *
     * @param  [type] $user [description]
     * @return [type]       [description]
     */
    public function resolveUser($user = null)
    {
        if ($user instanceof User) {
            $this->setUser($user);
        }
        return $this->getUser();
    }

    /**
     * Set the current user
     *
     * @param User $user [description]
     */
    public function setUser(User $user)
    {
        request()->setUserResolver(
            function () use ($user) {
                return $user;
            }
        );
        auth()->setUser($user);
        $this->user = $user;
        $this->getProvider()->setUser($user);
    }

    /**
     * Get the user object
     *
     * @return User The current user
     */
    public function getUser(): User
    {
        if (!isset($this->user)) {
            $user = auth()->user() ?: $this->getProvider()->getUser();
            if (!($user instanceof User)) {
                throw new \Exception("User not found: " . dump($user));
            }
            $this->setUser($user);
        }

        if (!$this->user && \App::environment(['dev', 'local', 'staging'])) {
            // Lets return the first user for kicks
            // Need to select random user from developers!
            // Shoud add it to application config with user's email
            $class = config('assistant.userClass', 'App\User');
            $this->user = $class::where(['email' => config('assistant.userWhenNoneFound')])->first();
        }

        if (!$this->user) {
            \Log::error(new \Exception('User not found'));
            abort(404, 'User not found');
        }

        return $this->user;
    }

    /**
     * Get the assistant the user wants to use
     *
     * @param  [type] $user [description]
     * @return AssistantModel
     */
    public function getAssistantPersona($user = null): AssistantModel
    {
        $user = $this->resolveUser($user);

        /**
         * Get the assistant for the user
         */
        $assistant = $user->assistant;
        if (!$assistant) {
            $assistant = AssistantModel::first();
            $user->assistantChoice()->create(['assistant_id' => $assistant->id]);
            return $assistant;
        } else {
            return $assistant;
        }
    }

    /**
     * Check for unknown inputs an get the params, originalParams and the action
     *
     * @param BaseInteractionProvider $provider
     * @param array                   $originalParams
     *
     * @return array
     */
    protected function checkForUnknownInputAndHandle(AssistantInteraction $interaction, BaseInteractionProvider $provider, array $originalParams = []): array
    {
        $params = $originalParams;
        if ($provider->isUnknownInput(Arr::get($params, 'action'))) {
            /**
             * Suffix the action with '-event' to distinguish that we are initiating an event
             * We will prefix the text here with the event name to properly trigger the ntent on dialogflow using just text detection.
             * This means that all phrases that can trigger the intent from the api should contain a similar intent prefixed by the action name. i.e:
             * Yes I am
             * check-goal-near-completion:Yes I am
             */
            $this->ensureLastInteraction($interaction);
            $originalQueryText = $params['queryText'];
            $queryText = str_replace(['none:', $interaction->action . ':', '::'], ['', '', ''], $originalQueryText);
            $queryText = $interaction->action . ':' . $queryText;
            $contexts = array_merge([$interaction->action], Arr::get($params, 'contexts', collect([]))->all());
            $originalParams = $params;
            $params = $provider->query(
                $queryText,
                $interaction->action,
                compact('contexts')
            );
            $params['wasInputUnknown'] = true;
            $action = Arr::get($params, 'action', $interaction->action);
            $interaction->action = $action;
        }

        return compact('params', 'originalParams', 'action');
    }

    /**
     * Should we be performing any more interactions with the user?
     *
     * @return boolean
     */
    public function hasNextInteraction()
    {
        return $this->nextInteraction instanceof AssistantInteraction;
    }

    /**
     * Set the current interaction
     *
     * @param  AssistantInteraction $interaction
     * @return void
     */
    protected function setCurrentInteraction(AssistantInteraction $interaction)
    {
        $this->interaction = $interaction;
        /**
         * Make sure the interaction is saved
         */
        if (!$this->interaction->assisistant_session_id) {
            $this->getSession()->interactions()->save($this->interaction);
        }
    }

    /**
     * Get the current itneraction for the user. If there is no interaction we will create a default one IF the interaction was instantiated by the user and not the server
     *
     * @param [type] $user
     * @param string $action
     *
     * @return void
     */
    protected function getCurrentInteraction($user = null, $action = 'none')
    {
        $user = $this->resolveUser($user);
        if (!isset($this->interaction)) {
            $session = $this->getSession($user);
            $this->interaction = $session->getNextInteraction(null, $action, $this->startedBy) ?: null;
            /**
             * Make sure we're not dealing with an expired interaction
             */
            if ($this->interaction && $this->interaction->isExpired()) {
                $this->interaction->expire();
                $this->interaction = $session->getNextInteraction(null, $action, $this->startedBy);
            }
            if ($this->interaction) {
                $this->interaction->ensureStartedBy($this->startedBy);
                !$this->interaction->action && $this->interaction->ensureAction($this->getLastInteraction());
                \Log::info("Found interaction {$this->interaction->id} for {$user->name}({$user->id})");
            }
        }

        return $this->interaction;
    }

    /**
     * Get the last interaction for the user across sessions where the action is not static::NO_INTERACTION
     *
     * @param [type] $user
     *
     * @return void
     */
    protected function getLastInteraction($user = null)
    {
        $user = $this->resolveUser($user);
        if (!isset($this->lastInteraction)) {
            // It makes sense that we get the last interaction if there is no last interaction for the day
            $this->lastInteraction = $user->lastInteractionForTheDay ?: $user->lastInteraction ?: $this->interaction;
            if ($this->lastInteraction instanceof AssistantInteraction) {
                $this->lastInteraction->ensureStartedBy($this->startedBy);
            }
        }

        return $this->lastInteraction;
    }

    /**
     * Update an interaction
     *
     * @param array $params
     *
     * @return void
     */
    public function updateInteraction($params = [])
    {
        if (!empty($params)) {
            $this->getCurrentInteraction()->fill($params);
            $this->getCurrentInteraction()->save();
        }
    }

    /**
     * We're makning sure the last interactin is brougt up to par with the given interaction
     * This is necessary for example when we have empty interactions
     *
     * @param \Nitm\Assistant\Models\AssistantInteraction $interaction
     *
     * @return void
     */
    public function ensureLastInteraction(AssistantInteraction $interaction)
    {
        if (empty($interaction->action) || $interaction->action === static::NO_INTERACTION) {
            $lastInteraction = $this->getLastInteraction();
            if ($lastInteraction instanceof AssistantInteraction) {
                $interaction->fill(Arr::only($lastInteraction->getAttributes(), ['action', 'item_type', 'item_id', 'item_class']));
                $interaction->provider_class = get_class($this->getProvider());
            }
        }
    }

    /**
     * Get the available interpreters
     *
     * @return array
     */
    protected static function getInterpreters(): array
    {
        return [
            'dialogFlow' => [
                'class' => DialogFlowInterpretation::class
            ],
            'twilio' => [
                'class' => TwilioInterpretation::class
            ],
            'vonage' => [
                'class' => VonageInterpretation::class
            ]
        ];
    }

    /**
     * Get the available providers
     *
     * @return array
     */
    protected static function getProviders(): array
    {
        return [
            'vonage' => [
                'class' => VonageInteraction::class
            ],
            'twilio' => [
                'class' => TwilioInteraction::class
            ],
            'dialogFlow' => [
                'class' => DialogFlowInteraction::class
            ]
        ];
    }

    /**
     * Get the next interaction based on teh current interaaction
     *
     * @param  string               $action      [description]
     * @param  AssistantInteraction $interaction [description]
     * @return string              [description]
     */
    protected static function resolveAction($action, $interaction): string
    {
        $interaction->action = $action === 'input.unknown' ? $interaction->action : $action;
        return $interaction->action;
    }

    public function continueSession()
    {
        return [];
    }

    /**
     * Get the user current timestamp in their timezone
     *
     * @param User $user
     *
     * @return Carbon
     */
    public function getUserNow(User $user = null): Carbon
    {
        $user = $user ?: $this->getUser();
        if ($user instanceof User) {
            return $user->getNow();
        }
        return Carbon::now();
    }

    /**
     * Get the current hour for the user
     *
     * @param User $user
     *
     * @return string
     */
    public function getUserNowHour(User $user = null): string
    {
        return $this->getUserNow($user)->format('H');
    }

    /**
     * Complete the session
     *
     * @return array
     */
    public function completeSession(): array
    {
        /**
         * TODO: Determine if we should send a response here
         */
        $this->getSession()->complete();
        $next = $this->getSession()->getNext($this->getUserNowHour());
        $first = $this->getSession()->getFirst($this->getUserNowHour());
        $fromNow = null;
        $carbon = Carbon::createFromTimeString($next);
        if ($next) {
            $fromNow = $carbon->fromNow();
        } else if ($first) {
            $carbon = Carbon::createFromTimeString($first);
            $fromNow = $carbon->addDays(1)->fromNow();
        }
        $messageKey = $fromNow ? 'assistant.sessionCompleteWithNext' : 'assistant.sessionComplete';
        return [
            'message' => $this->__(
                $messageKey,
                [
                    'name' => $this->getUser()->first_name,
                    'firstName' => $this->getUser()->first_name,
                    'lastName' => $this->getUser()->last_name,
                    'fromNow' => $fromNow
                ]
            )
        ];
    }

    /**
     * Complete the interaction
     *
     * @return void
     */
    public function completeInteraction()
    {
        /**
         * We need to switch tot he server context here in order to properly denote who started the interaction
         */
        $this->setInteractionTypeStartedBy(static::QUERY_INTERACTION_CONTEXT, static::SERVER_CONTEXT);
        if ($this->getCurrentInteraction()) {
            $lastLinkedInteraction = $this->getCurrentInteraction()->lastInProgressInteractionFrom(static::SERVER_CONTEXT)->first();
            if ($lastLinkedInteraction) {
                $lastLinkedInteraction->complete();
            }
            $this->getCurrentInteraction()->complete();
            $this->interaction = null;
            if ($this->getCurrentInteraction()) {
                $this->nextInteraction = $this->interaction;
            }
        }

        $next = $this->getCurrentInteraction();

        if ($next instanceof AssistantInteraction) {
            $this->previousInteraction = $this->interaction;
            $this->interaction = $next;
            return [
                'interaction ' => $next
            ];
        } else {
            return [
                'endSession' => true
            ];
        }
    }

    /**
     * Is the interaction completed?
     *
     * @return boolean
     */
    public function isInteractionCompleted()
    {
        return $this->previousInteraction instanceof AssistantInteraction;
    }
}