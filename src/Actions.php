<?php

namespace Nitm\Assistant;

/**
 * Extracted the constants for the app in order to cleanup the code
 */

class Actions
{
    /**
     * Create a summary for the user. THe summary will be based on the period/day of the week
     * @var string
     */
    const SEND_SUMMARY = 'send-summary';

    /**
     * Should the session continue with more interactions?
     * @var string
     */
    const CONTINUE_SESSION = 'continue-session';

    /**
     * End the session when this condition is met
     * @var string
     */
    const END_SESSION = 'end-session';

    /**
     * This stores user context information as needed
     * @var string
     */
    const USER_CONTEXT = 'assistant-user';
}
