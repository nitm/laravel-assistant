<?php

namespace Nitm\Assistant\Repositories;

use Nitm\Assistant\Models\AssistantInteraction;
use InfyOm\Generator\Common\BaseRepository;
use Nitm\Assistant\Contracts\Repository;

/**
 * Class AssistantInteractionRepository
 * @package Nitm\Assistant\Repositories
 * @version March 30, 2018, 2:16 pm UTC
 *
 * @method AssistantInteraction findWithoutFail($id, $columns = ['*'])
 * @method AssistantInteraction find($id, $columns = ['*'])
 * @method AssistantInteraction first($columns = ['*'])
*/
class AssistantInteractionRepository extends BaseRepository implements Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'assistant_session_id',
        'user_id',
        'request',
        'uuid',
        'has_multiple_responses',
        'is_complete',
        'response_types',
        'sequence'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AssistantInteraction::class;
    }

    /**
     * @inheritDoc
     *
     */
    public function getFilters(): array
    {
        $userClass = config('assistant.userClass', '\App\User');
        return [
        ];
    }
}
