<?php

namespace Nitm\Assistant\Repositories;

use Nitm\Assistant\Models\AssistantActivity;
use InfyOm\Generator\Common\BaseRepository;
use Nitm\Assistant\Contracts\Repository;
use Nitm\Assistant\Contracts\Assistant;

/**
 * Class AssistantRepository
 * @package Nitm\Assistant\Repositories
 * @version March 30, 2018, 1:14 pm UTC
 *
 * @method Assistant findWithoutFail($id, $columns = ['*'])
 * @method Assistant find($id, $columns = ['*'])
 * @method Assistant first($columns = ['*'])
 */
class AssistantActivityRepository extends BaseRepository implements Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'verb',
        'remote_type',
        'remote_id',
        'input_action',
        'input_intent',
        'needs_handoff',
        'request',
        'response',
        'target'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AssistantActivity::class;
    }

    /**
     * @inheritDoc
     *
     */
    public function getFilters(): array
    {
        $userClass = config('assistant.userClass', '\App\User');
        return [
            'orderBy' => [
                'id' => 'ID',
                'user' => 'User',
                'action' => 'Action'
            ],
            'users' => $userClass::select(['id', 'name'])
                ->whereIn(
                    'id',
                    function ($query) {
                        $query->from($this->makeModel()->getTable())
                            ->select('user_id')
                            ->distinct('user_id');
                    }
                )
                ->orderBy('name', 'asc')
                ->get()
                ->keyBy('id'),
            'startedBy' => [
                Assistant::SERVER_CONTEXT => Assistant::SERVER_CONTEXT,
                Assistant::USER_CONTEXT => Assistant::USER_CONTEXT
            ],
            'actions' => $this->makeModel()
                ->select('verb')
                ->distinct('verb')
                ->orderBy('verb', 'asc')
                ->get()
                ->pluck('verb', 'verb'),
            'types' => $this->makeModel()
                ->select('remote_type')
                ->distinct('remote_type')
                ->orderBy('remote_type', 'asc')
                ->get()
                ->pluck('remote_type', 'remote_type')
        ];
    }
}