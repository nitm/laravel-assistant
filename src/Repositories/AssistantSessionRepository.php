<?php

namespace Nitm\Assistant\Repositories;

use Nitm\Assistant\Models\AssistantSession;
use InfyOm\Generator\Common\BaseRepository;
use App\Reports\Contracts\Repository;

/**
 * Class AssistantSessionRepository
 * @package Nitm\Assistant\Repositories
 * @version March 30, 2018, 2:12 pm UTC
 *
 * @method AssistantSession findWithoutFail($id, $columns = ['*'])
 * @method AssistantSession find($id, $columns = ['*'])
 * @method AssistantSession first($columns = ['*'])
*/
class AssistantSessionRepository extends BaseRepository implements Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'assistant_id',
        'user_id',
        'session_date',
        'sequence',
        'item_type',
        'item_id',
        'completed_on'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AssistantSession::class;
    }

    /**
     * @inheritDoc
     *
     */
    public function getFilters(): array
    {
        $userClass = config('assistant.userClass', '\App\User');
        return [
        ];
    }
}
