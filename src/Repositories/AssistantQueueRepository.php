<?php

namespace Nitm\Assistant\Repositories;

use Nitm\Assistant\Models\AssistantQueue;
use InfyOm\Generator\Common\BaseRepository;
use Nitm\Assistant\Contracts\Repository;

/**
 * Class AssistantQueueRepository
 * @package Nitm\Assistant\Repositories
 * @version March 30, 2018, 2:14 pm UTC
 *
 * @method AssistantQueue findWithoutFail($id, $columns = ['*'])
 * @method AssistantQueue find($id, $columns = ['*'])
 * @method AssistantQueue first($columns = ['*'])
*/
class AssistantQueueRepository extends BaseRepository implements Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'interaction_date',
        'sequence',
        'item_type',
        'item_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AssistantQueue::class;
    }

    /**
     * @inheritDoc
     *
     */
    public function getFilters(): array
    {
        $userClass = config('assistant.userClass', '\App\User');
        return [
        ];
    }
}
