<?php

namespace Nitm\Assistant\Repositories;

use Nitm\Assistant\Models\AssistantInteractionQuery;
use InfyOm\Generator\Common\BaseRepository;
use Nitm\Assistant\Contracts\Repository;


/**
 * Class AssistantInteractionResponseRepository
 * @package Nitm\Assistant\Repositories
 * @version March 30, 2018, 2:18 pm UTC
 *
 * @method AssistantInteractionResponse findWithoutFail($id, $columns = ['*'])
 * @method AssistantInteractionResponse find($id, $columns = ['*'])
 * @method AssistantInteractionResponse first($columns = ['*'])
*/
class AssistantInteractionQueryRepository extends BaseRepository implements Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'assistant_interaction_id',
        'user_id',
        'response',
        'uuid',
        'is_final'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AssistantInteractionQuery::class;
    }

    /**
     * @inheritDoc
     *
     */
    public function getFilters(): array
    {
        $userClass = config('assistant.userClass', '\App\User');
        return [
        ];
    }
}
