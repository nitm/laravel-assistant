<?php

namespace Nitm\Assistant\Repositories;

use Nitm\Assistant\Models\Assistant as Model;
use InfyOm\Generator\Common\BaseRepository;
use Nitm\Assistant\Contracts\Repository;
use Nitm\Assistant\Contracts\Assistant;

/**
 * Class AssistantRepository
 * @package Nitm\Assistant\Repositories
 * @version March 30, 2018, 1:14 pm UTC
 *
 * @method Assistant findWithoutFail($id, $columns = ['*'])
 * @method Assistant find($id, $columns = ['*'])
 * @method Assistant first($columns = ['*'])
 */
class AssistantRepository extends BaseRepository implements Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'bio'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Model::class;
    }

    /**
     * @inheritDoc
     *
     */
    public function getFilters(): array
    {
        $userClass = config('assistant.userClass', '\App\User');
        return [];
    }
}