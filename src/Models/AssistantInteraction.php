<?php

namespace Nitm\Assistant\Models;

use DB;
use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Ramsey\Uuid\Uuid;
use Nitm\Assistant\Assistant as AssistantService;

/**
 * Class AssistantInteraction
 * @package Nitm\Assistant\Models
 * @version March 30, 2018, 2:16 pm UTC
 *
 * @property \Nitm\Assistant\Models\AssistantSession assistantSession
 * @property \Illuminate\Foundation\Auth\User user
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection AssistantInteractionResponse
 * @property \Illuminate\Database\Eloquent\Collection profiles
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property \Illuminate\Database\Eloquent\Collection choices
 * @property \Illuminate\Database\Eloquent\Collection choicesLinkedTo
 * @property \Illuminate\Database\Eloquent\Collection chatThreads
 * @property \Illuminate\Database\Eloquent\Collection chatMessages
 * @property \Illuminate\Database\Eloquent\Collection groupMembers
 * @property \Illuminate\Database\Eloquent\Collection meetingAttendance
 * @property \Illuminate\Database\Eloquent\Collection journeys
 * @property \Illuminate\Database\Eloquent\Collection milestones
 * @property \Illuminate\Database\Eloquent\Collection assistantSessions
 * @property integer assistant_session_id
 * @property integer user_id
 * @property string request
 * @property string uuid
 * @property boolean has_multiple_querys
 * @property boolean is_complete
 * @property string query_types
 * @property integer sequence
 */
class AssistantInteraction extends Model implements Contracts\AssistantInteraction
{
    use \Nitm\Assistant\Traits\StatusTrait;
    use SoftDeletes;

    const STATUS_IN_PROGRESS = 'in-progress';
    const STATUS_COMPLETE = 'complete';
    const STATUS_EXPIRED = 'expired';
    const STATUS_FAILED = 'expired';

    public $table = 'assistant_interactions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at', 'completed_on', 'created_at', 'updated_at'];


    public $fillable = [
        'assistant_session_id',
        'provider_session_id',
        'user_id',
        'request',
        'uuid',
        'has_multiple_queries',
        'is_complete',
        'query_types',
        'sequence',
        'item_class',
        'item_type',
        'item_id',
        'action',
        'provider_class',
        'started_by',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'assistant_session_id' => 'integer',
        'provider_session_id' => 'string',
        'user_id' => 'integer',
        'request' => 'array',
        'uuid' => 'string',
        'has_multiple_querys' => 'boolean',
        'is_complete' => 'boolean',
        'query_types' => 'array',
        'sequence' => 'integer',
        'action' => 'string',
        'item_class' => 'string',
        'provider_class' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            // $model->provider_session_id = $model->provider_session_id ?: 'local-'.uniqid();
            $model->request = $model->request ?: request()->all();
            $model->uuid = Uuid::uuid4()->toString();
            $model->query_types = $model->query_types ?: ['text'];
        });

        static::saving(function ($model) {
            if ($model->exists && $model->sequence == null && $model->session) {
                $model->sequence = $model->session->interactions()->count() + 1;
            }
        });
    }

    public function isComplete()
    {
        return $this->is_complete === true;
    }

    /**
     * @inheritDoc
     *
     * @param array $attributes
     * @param boolean $exists
     *
     * @return static
     */
    public function newInstance($attributes = [], $exists = false) {
        $model= parent::newInstance($attributes, $exists);
        $model->forceFill([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        return $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function session()
    {
        return $this->belongsTo(AssistantSession::class, 'assistant_session_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function lastInteractionFrom($from = null) 
    {
        $from = $from ?: AssistantService::SERVER_CONTEXT;
        return $this->belongsTo(static::class, 'assistant_session_id', 'assistant_session_id')
            ->whereStatus(static::STATUS_IN_PROGRESS)
            ->whereStartedBy($from);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function lastInProgressInteractionFrom($from = null) 
    {
        return $this->lastInteractionFrom($from)
            ->whereStatus(static::STATUS_IN_PROGRESS);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function lastCompletedInteractionFrom($from = null) 
    {
        return $this->lastInteractionFrom($from)
            ->whereStatus(static::STATUS_COMPLETE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(config('assistant.userClass', 'App\User'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function queries()
    {
        return $this->hasMany(AssistantInteractionQuery::class);
    }

    public function item()
    {
        return $this->morphTo('item', 'item_class');
    }

    /**
     * Complete the interaction
     */
    public function complete()
    {
        if (!$this->is_complete) {
            $this->status = static::STATUS_COMPLETE;
            $this->is_complete = true;
            $this->completed_on = Carbon::now();
            $this->save();
            if (!$this->session) {
                $this->load('session');
            }
            $this->session
                ->interactions()
                ->whereSequence($this->sequence)
                ->whereAction('none')
                ->whereStartedBy(AssistantService::SERVER_CONTEXT)
                ->update([
                    'status' => static::STATUS_COMPLETE,
                    'is_complete' => true
                ]);
        }
    }

    /**
     * Expire the interaction
     */
    public function expire() {
        $this->status = static::STATUS_EXPIRED;
        $this->save();
    }
    
    /**
     * Is this interaction expired?
     * TODO: Extrapolate the time value to an app configurable service
     * 
     * @return boolean
     */
    public function isExpired(): bool {
        return $this->status == static::STATUS_IN_PROGRESS && Carbon::now()->diffInSeconds($this->created_at) >= (60 * 60);
    }

    // public function getItemAttribute()
    // {
    //     if(!isset($this->_item)) {
    //         $itemClass = $this->item_class;
    //         $this->_item = class_exists($itemClass) ? $itemClass::find($this->item_id) : null;
    //     }
    //     return $this->_item;
    // }

    /**
     * Make sure that the started by value is correct
     *
     * @param string $startedBy
     * @return void
     */
    public function ensureStartedBy(string $startedBy, string $providerClass= null)
    {
        if ($this->started_by === 'none' || !$this->started_by) {
            $this->started_by = $startedBy;
            $this->started_by->provider_calss = $providerClass;
            $this->save();
        }
    }

    /**
     * Sure the action is set propery
     */
    public function ensureAction(AssistantInteraction $lastInteraction) {
        // We need to ensure this interaction action is setup properly
        if (!$this->action) {
            foreach (['action', 'item_type',  'item_id', 'item_class'] as $required) {
                $interaction->$required = $interaction->$required ?: $lastInteraction->$required;
            }
            $interaction->save();
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $query
     * @param integer $count
     * @param string $interval
     * @param string $whereOperator For comparison
     * @param string $operator For interval comparison
     * @return void
     */
    public function scopeWithinTheLast($query, $count = 24, $interval = 'hours', $whereOperator = '>', $operator = '-')
    {
        $updatedAt = 'timezone(users.timezone, '.$this->getTable().'.updated_at)';
        $now = 'timezone(users.timezone, now())';
        $query->where(
            DB::raw($updatedAt),
            $whereOperator,
            DB::raw($now." {$operator} interval '{$count} {$interval}'")
        )
            ->join("users", "users.id", "=", "user_id");
    }

    /**
     * Undocumented function
     *
     * @param [type] $query
     * @param integer $count
     * @param string $interval
     * @return void
     */
    public function scopeOlderThan($query, $count = 24, $interval = 'hours')
    {
        $query->scopeWithinTheLast($count, $interval, '<', '-');
    }

    /**
     * Get questions for the specified user
     *
     * @param [type] $query
     * @param [type] $user
     * @return void
     */
    public function scopeForUser($query, $user = null)
    {
        $user = $user ?: auth()->user();
        $user = is_array($user) ? $user[0] : $user;
        if ($user instanceof Authenticatable) {
            $query->where('user_id', $user->id);
        }
    }

    public function scopeInteractionInterval($query, $itemClass = null, array $range, $interval = [1, 'day'])
    {
        $start = array_get($range, 'start', Carbon::now()->sub(1, 'month'));
        $end = array_get($range, 'end', Carbon::now());

        list($intervalUnit, $intervalInterval) = $interval;

        switch ($intervalInterval) {
            case 'year':
            $intervalFormat = 'YYYY';
            break;

            case 'month':
            $intervalFormat = 'YYYY-MM';
            break;

            default:
            $intervalFormat = 'YYYY-MM-DD';
            break;
        }

        $intervalInterval = $intervalUnit > 1 ? Str::plural($intervalInterval) : Str::singular($intervalInterval);

        $table = $this->getTable();
        $updatedAt = DB::raw($table.'.updated_at');
        // $updatedAt = DB::raw('timezone(users.timezone, '.$table.'.created_at)');
        $query->interactionRate($itemClass)
            ->selectRaw("to_char(series_ts, '{$intervalFormat}') as rate_interval")
            ->fromRaw(
                \DB::raw(
                    "generate_series(
                        '{$start}'::timestamp,
                        '{$end}'::timestamp,
                        interval '{$intervalUnit} {$intervalInterval}'
                    ) response_ts(series_ts)"
                )
            )
            ->leftJoin(
                "assistant_interactions",
                function ($join) use ($updatedAt, $intervalInterval, $intervalUnit) {
                    $join->on($updatedAt, '>=', "response_ts.series_ts")
                        ->whereRaw("{$updatedAt} < response_ts.series_ts + interval '{$intervalUnit} {$intervalInterval}'")
                        ->groupBy(\DB::raw('1'));
                }
            )
            ->groupBy('series_ts')
            ->orderBy('series_ts', 'asc');
    }


    /**
     * Get the response rate distribution for goals
     *
     * @param [type] $query
     * @return void
     */
    public function scopeInteractionRate($query, $itemClass = null)
    {
        $table = $this->getTable();
        $itemClass = $itemClass ?: get_class($this);
        $query->selectRaw(
            'COUNT(DISTINCT(assistant_interactions.id)) as response_count'
        )
        ->where("item_class", $itemClass);
    }
}
