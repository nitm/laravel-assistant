<?php

namespace Nitm\Assistant\Models;

use Eloquent as Model;

/**
 * Class AssistantChoice
 * @package Nitm\Assistant\Models
 * @version April 16, 2018, 2:20 am UTC
 *
 */
class AssistantChoice extends Model
{
    public $table = 'assistant_choices';

    public $fillable = [
        'user_id', 'assistant_id'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'exists:users,id',
        'assistant_id' => 'exists:assistants,id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function assistant()
    {
        return $this->belongsTo(\Nitm\Assistant\Models\Assistant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(config('assistant.userClass', 'App\User'));
    }
}
