<?php

namespace Nitm\Assistant\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AssistantActivity
 * @package App\Models
 * @version August 19, 2018, 8:14 pm UTC
 *
 * @property \Illuminate\Foundation\Auth\User user
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection profiles
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property \Illuminate\Database\Eloquent\Collection choices
 * @property \Illuminate\Database\Eloquent\Collection choicesLinkedTo
 * @property \Illuminate\Database\Eloquent\Collection chatThreads
 * @property \Illuminate\Database\Eloquent\Collection chatMessages
 * @property \Illuminate\Database\Eloquent\Collection groupMembers
 * @property \Illuminate\Database\Eloquent\Collection meetingAttendance
 * @property \Illuminate\Database\Eloquent\Collection journeys
 * @property \Illuminate\Database\Eloquent\Collection milestones
 * @property \Illuminate\Database\Eloquent\Collection assistantChoices
 * @property \Illuminate\Database\Eloquent\Collection assistantSessions
 * @property \Illuminate\Database\Eloquent\Collection assistantInteractionQueries
 * @property \Illuminate\Database\Eloquent\Collection assistantInteractions
 * @property integer user_id
 * @property string verb
 * @property string remote_type
 * @property integer remote_id
 * @property string input_action
 * @property string input_intent
 * @property string request
 * @property string response
 * @property string object
 * @property string target
 * @property string user
 * @property string input_contexts
 * @property string output_contexts
 * @property boolean needs_handoff
 */
class AssistantActivity extends Model
{
    use SoftDeletes;

    public $table = 'assistant_activity';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'verb',
        'remote_type',
        'remote_id',
        'input_action',
        'input_intent',
        'request',
        'response',
        'object',
        'target',
        'user',
        'input_contexts',
        'output_contexts',
        'needs_handoff',
        'previous_activity_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'verb' => 'string',
        'remote_type' => 'string',
        'remote_id' => 'integer',
        'input_action' => 'string',
        'input_intent' => 'array',
        'request' => 'array',
        'response' => 'array',
        'object' => 'array',
        'target' => 'array',
        'user' => 'array',
        'input_contexts' => 'array',
        'output_contexts' => 'array',
        'needs_handoff' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public $with = ['user'];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->remote_type = $model->remote_type ?: 'none';
            $model->remote_id = $model->remote_id ?: -1;
        });
        static::updating(function ($model) {
            $model->remote_type = $model->remote_type ?: 'none';
            $model->remote_id = $model->remote_id ?: -1;
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(config('assistant.userClass', 'App\User'), 'user_id');
    }

    public function getUserAttribute()
    {
        $user = array_get($this->relations, 'user');
        $class = config('assistant.userClass', 'App\User');
        return $user ?: new $class(array_get($this->attributes, 'user', []));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function previousActivity()
    {
        return $this->belongsTo(static::class);
    }
}
