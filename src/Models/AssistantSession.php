<?php

namespace Nitm\Assistant\Models;

use Eloquent as Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Collection;
use App\Assistant\Models\AssistantInteraction as AppAssistantInteraction;
use Nitm\Assistant\Assistant as AssistantService;

/**
 * Class AssistantSession
 *
 * @package Nitm\Assistant\Models
 * @version March 30, 2018, 2:12 pm UTC
 *
 * @property \Nitm\Assistant\Models\Assistant assistant
 * @property \Illuminate\Foundation\Auth\User user
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection assistantInteractionResponses
 * @property \Illuminate\Database\Eloquent\Collection profiles
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property \Illuminate\Database\Eloquent\Collection choices
 * @property \Illuminate\Database\Eloquent\Collection choicesLinkedTo
 * @property \Illuminate\Database\Eloquent\Collection chatThreads
 * @property \Illuminate\Database\Eloquent\Collection chatMessages
 * @property \Illuminate\Database\Eloquent\Collection groupMembers
 * @property \Illuminate\Database\Eloquent\Collection meetingAttendance
 * @property \Illuminate\Database\Eloquent\Collection journeys
 * @property \Illuminate\Database\Eloquent\Collection milestones
 * @property \Illuminate\Database\Eloquent\Collection AssistantInteraction
 * @property integer assistant_id
 * @property integer user_id
 * @property date session_date
 * @property integer sequence
 * @property string item_type
 * @property bigInteger item_id
 * @property string|\Carbon\Carbon completed_on
 */
class AssistantSession extends Model
{
    use SoftDeletes;

    public static $chunks;

    public $table = 'assistant_sessions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at', 'created_at', 'session_date'];


    public $fillable = [
        'assistant_id',
        'user_id',
        'session_date',
        'sequence',
        'item_type',
        'item_id',
        'completed_on'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'assistant_id' => 'integer',
        'user_id' => 'integer',
        'session_date' => 'date',
        'sequence' => 'integer',
        'item_type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'message_id' => 'required',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(
            function ($model) {
                $model->session_date = Carbon::now()->format('Y-m-d');
                $model->session_start = static::getStartTime();
                $model->session_end = static::getEndTime();
                $model->sequence = static::getSequence();
            }
        );
    }

    public static function getStartTime($hour = null)
    {
        $currentChunk = static::getCurrent($hour);
        return date('H:i', strtotime($currentChunk->first().':00'));
    }

    public static function getEndTime($hour = null)
    {
        $currentChunk = static::getCurrent($hour);
        return date('H:i', strtotime($currentChunk->last().':59:59'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function assistant()
    {
        return $this->belongsTo(\App\Assistant\Models\Assistant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(config('assistant.userClass', 'App\User'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function interactions()
    {
        return $this->hasMany(AppAssistantInteraction::class);
    }

    /**
     * Update the sequence for the session
     *
     * @return void
     */
    public function updateSequence()
    {
        $this->sequence++;
        if ($this->shouldEnd()) {
            $this->completed_on = Carbon::now();
        }
        $this->save();
    }

    /**
     * Prepare a time
     *
     * @param  integer $hour
     * @return string
     */
    protected static function prepareTime(int $hour, bool $use24Hr=true, $minutes='00'): string
    {
        return date($use24Hr ? 'H' : 'h', strtotime($hour . ':'.$minutes));
    }

    /**
     * Prepare a time chunk by formatting the time
     *
     * @param  Collection $chunk
     * @return Collection
     */
    protected static function prepareTimeChunk(Collection $chunk): Collection
    {
        return $chunk->map(
            function ($hour) {
                return static::prepareTime($hour, true);
            }
        );
    }

    /**
     * Get session chunk for the specified human redable range
     *
     * @param  string $timeOfDay
     * @param  bool   $asTime
     * @return void
     */
    public static function getFor(string $timeOfDay, bool $asTime = false): Collection
    {
        $chunk = collect([]);
        if (static::hasTimeOfDay($timeOfDay)) {
            $chunk = static::prepareTimeChunk(static::getNamedChunks($timeOfDay));
        }
        return $asTime ? $chunk->map(
            function ($time) {
                return $time.':00';
            }
        ) : $chunk;
    }

    /**
     * Does the given chunk exist in our supported chunks?
     *
     * @param  string $timeOfDay
     * @return string
     */
    public static function hasTimeOfDay(string $timeOfDay): string
    {
        return static::getNamedChunkNames()->has($timeOfDay);
    }

    /**
     * Get the named chunks with their ranges
     *
     * @param  string $chunk
     * @return Collection
     */
    public static function getNamedChunks(string $chunk = null): COllection
    {
        $chunks =  [
            'early-morning' => range(4, 7),
            'morning' => range(8, 11),
            'afternoon' => range(12, 15),
            'evening' => range(16, 19),
            'night' => range(20, 23),
            'late-night' => range(0, 3)
        ];
        return collect($chunk ? array_get($chunks, $chunk) : $chunks);
    }

    /**
     * Get a human readable formatted named chunks with times
     *
     * @param  string $chunk
     * @return Collection
     */
    public static function getNamedChunksFormatted(string $chunk = null): Collection
    {
        $chunks = static::getNamedChunks();
        return $chunks->transform(
            function ($value, $timeOfDay) {
                $range = collect($value)->map(
                    function ($hour) {
                        return (new Carbon(static::prepareTime($hour) . ':00'))->format('gA');
                    }
                );
                return [
                'id' => $timeOfDay,
                'label' => title_case($timeOfDay),
                'text' => title_case($timeOfDay),
                'value' => $range->all(),
                'range' => [
                    (new Carbon(static::prepareTime(current($value)) . ':00'))->format('g:00 A'),
                    (new Carbon(static::prepareTime(end($value)) . ':00'))->format('g:59 A')
                ]
                ];
            }
        );
    }

    /**
     * Get the named chunks for common sessions
     *
     * @return Collection
     */
    public static function getNamedChunkNames(): Collection
    {
        $names = static::getNamedChunks()->keys();
        return collect(array_combine($names->all(), array_map('title_case', $names->all())));
    }

    /**
     * Get the time chunks that can be used for sessions
     *
     * @return Collection
     */
    public static function getChunks($start = null, $end = null): Collection
    {
        if (!isset(static::$chunks)) {
            $maxSessions = config('assistant.dailySessions');
            $start = $start ?? config('assistant.startTime');
            $end = $end ?? config('assistant.endTime');
            static::$chunks = collect(
                array_chunk(
                    array_map(
                        function ($hour) {
                            return static::prepareTime($hour);
                        }, range($start, $end)
                    ), $maxSessions
                )
            );
        }

        return static::$chunks;
    }

    /**
     * Get the current session time chunk
     *
     * @return Collection
     */
    public static function getCurrent($hour = null): Collection
    {
        $hour = $hour ?: date('H');
        $chunks = static::getChunks();
        $filtered = $chunks->filter(
            function ($chunk) use ($hour) {
                return in_array($hour, $chunk);
            }
        );

        return collect($filtered->pop());
    }

    /**
     * Get the first session time chunk
     *
     * @return array
     */
    public static function getFirst()
    {
        $chunks = static::getChunks();
        $chunk = $chunks->shift();

        return array_shift($chunk);
    }

    /**
     * Get the last session time chunk
     *
     * @return array
     */
    public static function getLast()
    {
        $chunks = static::getChunks();
        $chunk = $chunks->shift();

        return $chunk->pop();
    }

    /**
     * Get the next session time chunk
     *
     * @return array
     */
    public static function getNext($hour = null)
    {
        $chunks = static::getChunks();
        $hour = $hour ?: date('H');
        $filtered = $chunks->filter(
            function ($chunk) use ($hour) {
                return $hour > max($chunk);
            }
        );

        $filtered = $filtered->count() ? collect($chunks->first()) : $filtered;

        return $filtered->shift($filtered);
    }

    /**
     * Get the label for the curent time of day
     *
     * @param  string $hour The hour to search for in format: 00:00
     * @param  User   $user The user we're checking the time of day for. Defaults to the current auth user
     * @return string
     */
    public static function getCurrentTimeOfDayLabel($hour = null, User $user = null): string
    {
        $label = 'unknown';
        $chunks = static::getNamedChunks();
        $hour = $hour ?: date('H');
        foreach ($chunks as $timeOfDay=>$chunk) {
            if (in_array($hour, $chunk)) {
                $label = $timeOfDay;
                break;
            }
        }
        return $label;
    }

    /**
     * Get the current sequence for the session
     *
     * @return [type] [description]
     */
    protected static function getSequence()
    {
        return static::getChunks()->search(static::getCurrent()) + 1;
    }

    public function complete()
    {
        if (!$this->completed_on) {
            $this->completed_on = Carbon::now();
            $this->save();
        }
    }
    /**
     * Get the next interaction. Could be a goal, question or encouragement
     *
     * @param  User $user [description]
     * @return Interaction       [description]
     */
    public function getEmptyInteraction(User $user = null, string $action = 'none', $startedBy = 'none')
    {
        $user = $user ?: $this->user;
        $interaction = $this->interactions()->create(
            [
            'user_id' => $user->id,
            'action' => $action,
            'started_by' => $startedBy,
            'created_at' => Carbon::now()
            ]
        );
        return $interaction;
    }

    public function getNextInteraction(User $user = null, string $action='none', $startedBy = 'none')
    {
        $user = $user ?: $this->user;
        $interaction = $this->interactions()
            ->whereStartedBy($startedBy)
            ->whereStatus(AssistantInteraction::STATUS_IN_PROGRESS)
            ->first();

        /**
         * If the interaction was last started by the server and it's less than the application service threshold
         * Return a null interaction to indicate there's no interaction available
         */
        if($interaction instanceof AssistantInteraction
            && $startedBy === AssistantService::SERVER_CONTEXT
            && Carbon::now()->diffInMinutes($interaction->created_at) < 60
            && $interaction->action !== AssistantService::NO_INTERACTION
        ) {
            return null;
        }

        if (!($interaction instanceof AssistantInteraction)) {
            $maxInteractions = config('assistant.interactionsPerSession');
            // We set this to server becuase the server has a limited number of sessions it can start
            $interactionCount = $this->interactions()
                ->whereStartedBy(AssistantService::SERVER_CONTEXT)
                ->whereIsComplete(true)
                ->count();

            if ($interactionCount <= $maxInteractions || $startedBy === AssistantService::USER_CONTEXT) {
                $query = $this->getInteractionsQuery(true, $user);
                $interaction = $query->whereStatus(AssistantInteraction::STATUS_IN_PROGRESS)
                    ->whereStartedBy($startedBy)
                    ->first();

                if (!$interaction && $interactionCount < $maxInteractions || $startedBy === AssistantService::USER_CONTEXT) {
                    /**
                     * TODO: How we get the next interaction doesn't make sense
                     * Need a more logical way of doing this
                     * Possible moving to a singleton?
                     */
                    if(method_exists(AppAssistantInteraction::class, 'getNext')) {
                        $interaction = AppAssistantInteraction::getNext($user, $startedBy) ?: $this->getEmptyInteraction($user, $action, $startedBy);
                        $interaction->sequence = $this->interactions()->count() + 1;
                        $this->interactions()->save($interaction);
                    }
                }
            }
        }

        return $interaction;
    }

    public function getLastInteraction(User $user = null)
    {
        $user = $user ?: $this->user;
        $query = $this->interactions()
            ->where(
                [
                'user_id' => $user->id,
                // 'provider_session_id' => $this->getHandler()->getSessionId()
                ]
            )
            ->where('action', '!=', 'none');
        return $query->orderBy('id', 'desc')->first();
    }


    /**
     * Get all of the interactions for a specified user
     *
     * @return [type] [description]
     */
    public function getInteractions($user = null)
    {
        $user = $user ?: $this->user;
        $query = $this->getInteractionsQuery(false, $user);
        if (!$query->count()) {
            $nextInteraction = $this->getNextInteraction();
            if ($nextInteraction instanceof AppAssistantInteraction) {
                $interactions = collect([$nextInteraction]);
            }
        } else {
            $interactions = $query->get();
        }
        return $interactions;
    }

    /**
     * [getInteractionsQuery description]
     *
     * @param  boolean $inCompleteOnly [description]
     * @param  [type]  $user           [description]
     * @return [type]                [description]
     */
    protected function getInteractionsQuery(bool $inCompleteOnly = true, $user = null)
    {
        $user = $user ?: $this->user;
        $query = $this->interactions()
            ->where(
                [
                'user_id' => $user->id,
                // 'provider_session_id' => $this->getHandler()->getSessionId()
                ]
            )
            ->where('sequence', "<=", (int)config('assistant.interactionsPerSession'))
            ->orderBy('sequence', 'desc');
        if ($inCompleteOnly) {
            $query->where(
                [
                'is_complete' => false,
                // 'provider_session_id' => $this->getHandler()->getSessionId()
                ]
            );
        }
        return $query;
    }
}