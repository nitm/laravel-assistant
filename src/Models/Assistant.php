<?php

namespace Nitm\Assistant\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Assistant
 * @package Nitm\Assistant\Models
 * @version March 30, 2018, 1:14 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection assistantInteractionResponses
 * @property \Illuminate\Database\Eloquent\Collection profiles
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property \Illuminate\Database\Eloquent\Collection choices
 * @property \Illuminate\Database\Eloquent\Collection choicesLinkedTo
 * @property \Illuminate\Database\Eloquent\Collection chatThreads
 * @property \Illuminate\Database\Eloquent\Collection chatMessages
 * @property \Illuminate\Database\Eloquent\Collection groupMembers
 * @property \Illuminate\Database\Eloquent\Collection meetingAttendance
 * @property \Illuminate\Database\Eloquent\Collection journeys
 * @property \Illuminate\Database\Eloquent\Collection milestones
 * @property \Illuminate\Database\Eloquent\Collection assistantInteractions
 * @property \Illuminate\Database\Eloquent\Collection Assistantession
 * @property string name
 * @property string bio
 */
class Assistant extends Model
{
    use SoftDeletes;

    public $table = 'assistants';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'bio'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'bio' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function sessions()
    {
        return $this->hasMany(\Nitm\Assistant\Models\AssistantSession::class);
    }
}
