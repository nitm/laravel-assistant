<?php

namespace Nitm\Assistant\Models;

use Illuminate\Database\Eloquent\Model;

class SigningKey extends Model
{
    /**
     * @var string
     */
    protected $table = 'signing_keys';

    protected $fillable = ['sid', 'friendly_name', 'secret'];
}