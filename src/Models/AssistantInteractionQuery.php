<?php

namespace Nitm\Assistant\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

/**
 * Class AssistantInteractionResponse
 * @package Nitm\Assistant\Models
 * @version March 30, 2018, 2:18 pm UTC
 *
 * @property \Nitm\Assistant\Models\AssistantInteraction assistantInteraction
 * @property \Illuminate\Foundation\Auth\User user
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection profiles
 * @property \Illuminate\Database\Eloquent\Collection userRoles
 * @property \Illuminate\Database\Eloquent\Collection choices
 * @property \Illuminate\Database\Eloquent\Collection choicesLinkedTo
 * @property \Illuminate\Database\Eloquent\Collection chatThreads
 * @property \Illuminate\Database\Eloquent\Collection chatMessages
 * @property \Illuminate\Database\Eloquent\Collection groupMembers
 * @property \Illuminate\Database\Eloquent\Collection meetingAttendance
 * @property \Illuminate\Database\Eloquent\Collection journeys
 * @property \Illuminate\Database\Eloquent\Collection milestones
 * @property \Illuminate\Database\Eloquent\Collection assistantInteractions
 * @property \Illuminate\Database\Eloquent\Collection assistantSessions
 * @property integer assistant_interaction_id
 * @property integer user_id
 * @property string response
 * @property string uuid
 * @property boolean is_final
 */
class AssistantInteractionQuery extends Model
{
    use SoftDeletes;

    public $table = 'assistant_interaction_queries';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'assistant_interaction_id',
        'provider_response_id',
        'user_id',
        'text',
        'type',
        'result',
        'uuid',
        'is_final'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'assistant_interaction_id' => 'integer',
        'provider_response_id' => 'integer',
        'user_id' => 'integer',
        'text' => 'string',
        'type' => 'string',
        'result' => 'array',
        'uuid' => 'string',
        'is_final' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->uuid = Uuid::uuid4()->toString();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function interaction()
    {
        return $this->belongsTo(\Nitm\Assistant\Models\AssistantInteraction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(config('assistant.userClass', 'App\User'));
    }
}
