<?php

namespace Nitm\Assistant\Models;

use Illuminate\Database\Eloquent\Model;

class AssistantAgreement extends Model
{
    const STATUS_ACTIVE = 'active';
    const STATUS_PAUSED = 'paused';
    const STATUS_INACTIVE = 'in-active';

    protected $table = 'assistant_agreements';

    protected $fillable = ['agreement_type', 'user_id', 'ip', 'signature', 'status'];

    protected $dates = ['created_at', 'udpated_at', 'deleted_at', 'paused_until'];

    protected $appends = ['is_active', 'is_inactive', 'is_paused'];

    /**
     * Relations
     */

    public function user()
    {
        return $this->belongsTo( config('assistant.userClass', 'App\User'));
    }

    /**
     * Attributes
     */

    public function getIsActiveAttribute() {
        return $this->status === static::STATUS_ACTIVE;
    }

    public function getIsInActiveAttribute() {
        return $this->status === static::STATUS_INACTIVE;
    }

    public function getIsPausedAttribute() {
        return $this->status === static::STATUS_PAUSED;
    }

    /**
     * Scopes
     */
    public function scopeIsActive($query) {
        $query->whereStatus(static::STATUS_ACTIVE);
    }

    public function scopeIsInActive($query) {
        $query->whereStatus(static::STATUS_INACTIVE);
    }

    public function scopeIsPaused($query) {
        $query->whereStatus(static::STATUS_PAUSED);
    }

    /**
     * Logic
     */
    public function setActive() {
        $this->status = static::STATUS_ACTIVE;
        $this->paused_until = null;
        $this->save();
    }

    public function setInActive() {
        $this->status = static::STATUS_INACTIVE;
        $this->save();
    }

    public function setPaused($until) {
        $this->status = static::STATUS_PAUSED;
        $this->paused_until = $until;
        $this->save();
    }
}
