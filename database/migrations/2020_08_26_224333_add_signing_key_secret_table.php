<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSigningKeySecretTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schem::hasTable('signing_keys')) {
            Schema::create('signing_keys', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->text('sid');
                $table->text('secret')->nullable();
                $table->text('friendly_name');

                $table->timestamps();
            });
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signing_keys');
    }
}