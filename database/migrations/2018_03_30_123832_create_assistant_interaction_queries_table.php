<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantInteractionQueriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistant_interaction_queries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assistant_interaction_id');
            $table->string('provider_response_id', 255);
            $table->string('type', 32);
            $table->integer('user_id');
            $table->text('text');
            $table->text('result');
            $table->uuid('uuid');
            $table->boolean('is_final')->default(false);
            $table->timestamps();
            $table->softDeletes();

            if (\DB::getDriverName() != 'mysql') {
                $table->foreign('assistant_interaction_id')
                    ->references('id')
                    ->on('assistant_interactions')
                    ->onDelete('cascade');
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistant_interaction_queries');
    }
}