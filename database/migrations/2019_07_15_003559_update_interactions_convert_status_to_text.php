<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInteractionsConvertStatusToText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assistant_interactions', function (Blueprint $table) {
            $table->string('new_status')->default('in-progress');
        });

        Schema::table('assistant_interactions', function (Blueprint $table) {
            DB::table($table->getTable())
                ->whereNull('new_status')
                ->update(['new_status' => DB::raw('status')]);
            $table->renameColumn('status', 'old_status');
        });

        Schema::table('assistant_interactions', function (Blueprint $table) {
            $table->renameColumn('new_status', 'status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}