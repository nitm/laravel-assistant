<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAssistantAgreementsAddStatusField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assistant_agreements', function (Blueprint $table) {
            $table->text('status')->nullable();
            $table->timestamp('paused_until')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assistant_agreements', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('paused_until');
        });
    }
}