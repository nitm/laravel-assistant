<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistant_activity', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('verb');
            $table->string('remote_type');
            $table->integer('remote_id');
            $table->string('input_action')->nullable();
            $table->string('input_intent')->nullable();
            $table->json('request')->nullable();
            $table->json('response')->nullable();
            $table->json('object')->nullable();
            $table->json('target')->nullable();
            $table->json('user')->nullable();
            $table->json('input_contexts')->nullable();
            $table->json('output_contexts')->nullable();
            $table->boolean('needs_handoff')->default(false);
            $table->timestamps();
            $table->softDeletes();

            if (\DB::getDriverName() != 'mysql') {
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistant_activity');
    }
}