<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantChoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistant_choices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('assistant_id');
            $table->timestamps();

            if (\DB::getDriverName() != 'mysql') {
                $table->foreign('assistant_id')
                    ->references('id')
                    ->on('assistants')
                    ->onDelete('cascade');
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistant_choices');
    }
}
