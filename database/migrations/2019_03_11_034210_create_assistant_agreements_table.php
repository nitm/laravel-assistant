<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistant_agreements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('agreement_type')->default('sms');
            $table->string('ip');
            $table->string('signature')->nullable();
            $table->timestamps();

            if (\DB::getDriverName() != 'mysql') {
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistant_agreements');
    }
}
