<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAssistantActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assistant_activity', function (Blueprint $table) {
            $table->integer('previous_activity_id')->nullable();

            if (\DB::getDriverName() != 'mysql') {
                $table->foreign('previous_activity_id')
                    ->references('id')
                    ->on('assistant_activity')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assistant_activity', function (Blueprint $table) {
            $table->dropColumn('previous_activity_id');
        });
    }
}
