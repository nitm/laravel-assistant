<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('bio')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        \DB::table('assistants')->insert([
            ['id' => 1,    'name' => 'Dari', 'bio' => 'Female assitant persona'],
            ['id' => 2,    'name' => 'Sol', 'bio' => 'Male assistant persona']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistants');
    }
}