<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantInteractionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistant_interactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assistant_session_id');
            $table->integer('user_id');
            $table->string('provider_session_id', 255);
            $table->text('request');
            $table->uuid('uuid');
            $table->string('status')->default('in-progress');
            $table->boolean('has_multiple_queries')->default(false);
            $table->boolean('is_complete')->default(false);
            $table->string('query_types', 64)->nullable();
            $table->integer('sequence')->default(1);
            $table->string('action')->nullable();
            $table->string('item_class')->nullable();
            $table->string('item_type')->nullable();
            $table->integer('item_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            if (\DB::getDriverName() != 'mysql') {
                $table->foreign('assistant_session_id')
                    ->references('id')
                    ->on('assistant_sessions')
                    ->onDelete('cascade');
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistant_interactions');
    }
}