<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistant_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assistant_id');
            $table->integer('user_id');
            $table->date('session_date');
            $table->time('session_start');
            $table->time('session_end');
            $table->integer('sequence')->default(1);
            $table->timestamp('completed_on')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('assistant_sessions', function (Blueprint $table) {
            if (\DB::getDriverName() != 'mysql') {
                $table->foreign('assistant_id')
                    ->references('id')
                    ->on('assistants')
                    ->onDelete('cascade');
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistant_sessions');
    }
}
