<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSessionTableMakeSessionUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assistant_sessions', function (Blueprint $table) {
            /**
             * TODO: Go through sessions and update interactions that are using a duplicate session
             */
            $sessions = DB::table('assistant_sessions')
                ->whereIn('id', function ($query) {
                    $query->from('assistant_sessions')
                        ->selectRaw('MIN(id)')
                        ->groupBy('user_id')
                        ->groupBy('session_date')
                        ->groupBy('session_start')
                        ->groupBy('session_end');
                })
                ->orderBy('id', 'desc')
                ->get();
            $sessions->map(function ($session) {
                $query = DB::table("assistant_sessions")->where([
                    'user_id' => $session->user_id,
                    'session_date' => $session->session_date,
                    'session_start' => $session->session_start,
                    'session_end' => $session->session_end
                ])
                    ->where('id', '!=', $session->id);
                $duplicate = $query->get();
                DB::table('assistant_interactions')
                    ->whereIn('assistant_session_id', $duplicate->pluck('id'))
                    ->update(['assistant_session_id' => $session->id]);
                $query->delete();
            });
            $table->unique(['user_id', 'session_date', 'session_start', 'session_end'], 'unique_assistant_session');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assistant_sessions', function (Blueprint $table) {
            $table->dropUnique(['user_id', 'session_date', 'session_start', 'session_end'], 'unique_assistant_session');
        });
    }
}