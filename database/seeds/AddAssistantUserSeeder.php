<?php

use Illuminate\Database\Seeder;

class AddAssistantUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create the default Assistant User
        // This is needed for cases where a non user attempted to contact the system
        $class = config('assistant.userClass', 'App\User');
        $user = $class::where(['email' => 'assistant@app.local'])->first();
        if(!($user instanceof User)) {
            $class::create([
                'name' => 'Assistant',
                'password' => bcrypt(str_random(20)),
                'email' => 'assistant@app.local'
            ]);
        }
    }
}
