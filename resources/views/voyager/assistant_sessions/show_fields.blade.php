<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $model->id !!}</p>
</div>

<!-- Assistant Id Field -->
<div class="form-group">
    {!! Form::label('assistant_id', 'Assistant Id:') !!}
    <p>{!! $model->assistant_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $model->user_id !!}</p>
</div>

<!-- Session Date Field -->
<div class="form-group">
    {!! Form::label('session_date', 'Session Date:') !!}
    <p>{!! $model->session_date !!}</p>
</div>

<!-- Sequence Field -->
<div class="form-group">
    {!! Form::label('sequence', 'Sequence:') !!}
    <p>{!! $model->sequence !!}</p>
</div>

<!-- Item Type Field -->
<div class="form-group">
    {!! Form::label('item_type', 'Item Type:') !!}
    <p>{!! $model->item_type !!}</p>
</div>

<!-- Item Id Field -->
<div class="form-group">
    {!! Form::label('item_id', 'Item Id:') !!}
    <p>{!! $model->item_id !!}</p>
</div>

<!-- Completed On Field -->
<div class="form-group">
    {!! Form::label('completed_on', 'Completed On:') !!}
    <p>{!! $model->completed_on !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $model->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $model->updated_at !!}</p>
</div>

