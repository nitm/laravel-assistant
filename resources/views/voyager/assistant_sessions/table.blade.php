<table class="table table-responsive" id="assistant-sessions-table">
    <thead>
        <th>Assistant</th>
        <th>User</th>
        <th>Session Date</th>
        <th>Sequence</th>
        <th>Item Type</th>
        <th>Item Id</th>
        <th>Completed On</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($models as $model)
        <tr>
            <td>{!! $model->assistant->name !!}</td>
            <td>{!! $model->user->name !!}</td>
            <td>{!! $model->session_date !!}</td>
            <td>{!! $model->sequence !!}</td>
            <td>{!! $model->item_type !!}</td>
            <td>{!! $model->item_id !!}</td>
            <td>{!! $model->completed_on ?: '(not completed)' !!}</td>
            <td>
                {!! Form::open(['route' => ['voyager.assistant-sessions.destroy', $model->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('voyager.assistant-sessions.show', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('voyager.assistant-sessions.edit', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
