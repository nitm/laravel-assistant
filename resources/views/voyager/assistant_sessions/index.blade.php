@extends('voyager::master')

@include('partials.content-index-header')

@section('content')
    <div class="page-content container-fluid browse">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('assistant::assistant_sessions.table')
            </div>
        </div>
    </div>
@endsection
