@extends('voyager::master')

@section('content')
    <section class="content-header">
        <h1>
            Assistant
        </h1>
    </section>
    <div class="page-content container-fluid browse">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('voyager.assistants.show_fields')
                    <a href="{!! route('voyager.assistants.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
