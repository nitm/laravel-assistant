<table class="table table-responsive" id="assistants-table">
    <thead>
        <th>Name</th>
        <th>Bio</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($models as $model)
        <tr>
            <td>{!! $model->name !!}</td>
            <td>{!! $model->bio !!}</td>
            <td>
                {!! Form::open(['route' => ['voyager.assistants.destroy', $model->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('voyager.assistants.show', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('voyager.assistants.edit', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
