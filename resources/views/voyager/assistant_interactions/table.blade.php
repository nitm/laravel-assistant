<table class="table table-responsive" id="assistant-interactions-table">
    <thead>
        <th>Assistant Session Id</th>
        <th>User Id</th>
        <th>Request</th>
        <th>Uuid</th>
        <th>Has Multiple Responses</th>
        <th>Is Complete</th>
        <th>Response Types</th>
        <th>Sequence</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($models as $model)
        <tr>
            <td>{!! $model->assistant_session_id !!}</td>
            <td>{!! $model->user_id !!}</td>
            <td>{!! '' !!}</td>
            <td>{!! $model->uuid !!}</td>
            <td>{!! $model->has_multiple_responses !!}</td>
            <td>{!! $model->is_complete !!}</td>
            <td>{!! $model->response_types !!}</td>
            <td>{!! $model->sequence !!}</td>
            <td>
                {!! Form::open(['route' => ['voyager.assistant-interactions.destroy', $model->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('voyager.assistant-interactions.show', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('voyager.assistant-interactions.edit', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
