<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $model->id !!}</p>
</div>

<!-- Assistant Session Id Field -->
<div class="form-group">
    {!! Form::label('assistant_session_id', 'Assistant Session Id:') !!}
    <p>{!! $model->assistant_session_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $model->user_id !!}</p>
</div>

<!-- Request Field -->
<div class="form-group">
    {!! Form::label('request', 'Request:') !!}
    <p>{!! $model->request !!}</p>
</div>

<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <p>{!! $model->uuid !!}</p>
</div>

<!-- Has Multiple Responses Field -->
<div class="form-group">
    {!! Form::label('has_multiple_responses', 'Has Multiple Responses:') !!}
    <p>{!! $model->has_multiple_responses !!}</p>
</div>

<!-- Is Complete Field -->
<div class="form-group">
    {!! Form::label('is_complete', 'Is Complete:') !!}
    <p>{!! $model->is_complete !!}</p>
</div>

<!-- Response Types Field -->
<div class="form-group">
    {!! Form::label('response_types', 'Response Types:') !!}
    <p>{!! $model->response_types !!}</p>
</div>

<!-- Sequence Field -->
<div class="form-group">
    {!! Form::label('sequence', 'Sequence:') !!}
    <p>{!! $model->sequence !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $model->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $model->updated_at !!}</p>
</div>

