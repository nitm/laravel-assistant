<!-- Assistant Session Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('assistant_session_id', 'Assistant Session Id:') !!}
    {!! Form::number('assistant_session_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Request Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('request', 'Request:') !!}
    {!! Form::textarea('request', null, ['class' => 'form-control']) !!}
</div>

<!-- Uuid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uuid', 'Uuid:') !!}
    {!! Form::text('uuid', null, ['class' => 'form-control']) !!}
</div>

<!-- Has Multiple Responses Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_multiple_responses', 'Has Multiple Responses:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_multiple_responses', false) !!}
        {!! Form::checkbox('has_multiple_responses', 1, null) !!} $VALUE$
    </label>
</div>

<!-- Is Complete Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_complete', 'Is Complete:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_complete', false) !!}
        {!! Form::checkbox('is_complete', 1, null) !!} $VALUE$
    </label>
</div>

<!-- Response Types Field -->
<div class="form-group col-sm-6">
    {!! Form::label('response_types', 'Response Types:') !!}
    {!! Form::text('response_types', null, ['class' => 'form-control']) !!}
</div>

<!-- Sequence Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sequence', 'Sequence:') !!}
    {!! Form::number('sequence', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('voyager.assistant-interactions.index') !!}" class="btn btn-default">Cancel</a>
</div>
