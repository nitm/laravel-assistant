@extends('voyager::master')

@include('partials.content-edit-add-header')

@section('content')
    <div class="page-content container-fluid browse">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'assistantInteractions.store']) !!}

                        @include('assistant::assistant_interactions.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
