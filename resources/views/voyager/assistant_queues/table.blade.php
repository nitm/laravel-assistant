<table class="table table-responsive" id="assistantQueues-table">
    <thead>
        <th>User Id</th>
        <th>Interaction Date</th>
        <th>Sequence</th>
        <th>Item Type</th>
        <th>Item Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($models as $model)
        <tr>
            <td>{!! $model->user_id !!}</td>
            <td>{!! $model->interaction_date !!}</td>
            <td>{!! $model->sequence !!}</td>
            <td>{!! $model->item_type !!}</td>
            <td>{!! $model->item_id !!}</td>
            <td>
                {!! Form::open(['route' => ['assistantQueues.destroy', $model->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('voyager.assistantQueues.show', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('voyager.assistantQueues.edit', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>