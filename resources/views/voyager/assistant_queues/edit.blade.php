@extends('voyager::master')

@include('partials.content-edit-add-header')

@section('content')
    <div class="page-content container-fluid browse">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($model, ['route' => ['assistantQueues.update', $model->id], 'method' => 'patch']) !!}

                        @include('assistant::assistant_queues.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection
