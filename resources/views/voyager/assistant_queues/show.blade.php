@extends('voyager::master')

@section('content')
    <section class="content-header">
        <h1>
            AssistantQueue
        </h1>
    </section>
    <div class="page-content container-fluid browse">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('assistant::assistant_queues.show_fields')
                    <a href="{!! route('voyager.assistant-queues.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
