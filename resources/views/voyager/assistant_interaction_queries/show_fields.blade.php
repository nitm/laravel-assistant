<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $model->id !!}</p>
</div>

<!-- Assistant Interaction Id Field -->
<div class="form-group">
    {!! Form::label('assistant_interaction_id', 'Assistant Interaction Id:') !!}
    <p>{!! $model->assistant_interaction_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $model->user_id !!}</p>
</div>

<!-- Response Field -->
<div class="form-group">
    {!! Form::label('response', 'Response:') !!}
    <p>{!! $model->response !!}</p>
</div>

<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <p>{!! $model->uuid !!}</p>
</div>

<!-- Is Final Field -->
<div class="form-group">
    {!! Form::label('is_final', 'Is Final:') !!}
    <p>{!! $model->is_final !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $model->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $model->updated_at !!}</p>
</div>

