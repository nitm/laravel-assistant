<!-- Assistant Interaction Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('assistant_interaction_id', 'Assistant Interaction Id:') !!}
    {!! Form::number('assistant_interaction_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Response Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('response', 'Response:') !!}
    {!! Form::textarea('response', null, ['class' => 'form-control']) !!}
</div>

<!-- Uuid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uuid', 'Uuid:') !!}
    {!! Form::text('uuid', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Final Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_final', 'Is Final:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_final', false) !!}
        {!! Form::checkbox('is_final', 1, null) !!} $VALUE$
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('voyager.assistant-interaction-queries.index') !!}" class="btn btn-default">Cancel</a>
</div>
