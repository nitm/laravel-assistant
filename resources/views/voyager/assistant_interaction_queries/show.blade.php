@extends('voyager::master')

@section('content')
    <section class="content-header">
        <h1>
            AssistantInteractionResponse
        </h1>
    </section>
    <div class="page-content container-fluid browse">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('assistant::assistant_interaction_queries.show_fields')
                    <a href="{!! route('voyager.assistant-interaction-queries.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
