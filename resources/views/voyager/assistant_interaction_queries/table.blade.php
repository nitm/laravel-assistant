<table class="table table-responsive" id="assistant-interaction-queries-table">
    <thead>
        <th>Assistant Interaction Id</th>
        <th>User Id</th>
        <th>Response</th>
        <th>Uuid</th>
        <th>Is Final</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($models as $model)
        <tr>
            <td>{!! $model->assistant_interaction_id !!}</td>
            <td>{!! $model->user_id !!}</td>
            <td>{!! $model->response !!}</td>
            <td>{!! $model->uuid !!}</td>
            <td>{!! $model->is_final !!}</td>
            <td>
                {!! Form::open(['route' => ['voyager.assistant-interaction-queries.destroy', $model->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('voyager.assistant-interaction-queries.show', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('voyager.assistant-interaction-queries.edit', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
