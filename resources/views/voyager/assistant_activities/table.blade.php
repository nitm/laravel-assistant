<?php
    $detailFields = [
        'request', 'response', 'object', 'user', 'target', 'input_contexts', 'output_contexts'
    ];
?>
<table class="table table-responsive" id="assistant-activities-table">
    <thead>
        <th>ID</th>
        <th>Started By</th>
        <th>User</th>
        <th>Action</th>
        <th>Remote Type</th>
        <th>Created</th>
        <th colspan="3">Actions</th>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td>
                <select id="filter_started_by" name="filter[started_by]"">
                    @foreach ($filters['startedBy'] as $key=>$value)
                        <option value="{{ $key }}" {{ $search->orderBy == $key ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <select id="filter_user_id" name="filter[user_id]"">
                    @foreach ($filters['users'] as $key=>$value)
                        <option value="{{ $key }}" {{ $search->orderBy == $key ? 'selected' : '' }}>{{ $value->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <select id="filter_action" name="filter[action]"">
                    @foreach ($filters['actions'] as $key=>$value)
                        <option value="{{ $key }}" {{ $search->orderBy == $key ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <select id="filter_remote_type" name="filter[remote_type]"">
                    @foreach ($filters['types'] as $key=>$value)
                        <option value="{{ $key }}" {{ $search->orderBy == $key ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        @foreach($models as $model)
        <tr>
            <td>{!! $model->id !!}</td>
            <td><strong>{!! $model->object ? $model->object['started_by'] : 'unknown' !!}</strong></td>
            <td>{!! $model->user ? $model->user->name : '(not set)'!!}</td>
            <td>{!! $model->verb !!}</td>
            <td>{!! $model->remote_type !!}</td>
            <td>{!! $model->created_at !!}</td>
            <td>
                <div class='btn-group'>
                    <a href="#" onclick="$('#details-{{ $model->id }}').slideToggle(); return false" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                </div>
            </td>
        </tr>
        <tr id="details-{{ $model->id }}" style="display: none">
            <td colspan="8">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    @foreach ($detailFields as $idx=>$field)
                    <li role="presentation" class="{{$idx == 0 ? 'active' : ''}}">
                        <a href="#{{$field}}-detail-{{$model->id}}" aria-controls="{{$field}}-detail-{{$model->id}}" role="tab" data-toggle="tab">
                        {{title_case($field)}}
                    </a>
                    </li>
                    @endforeach
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    @foreach ($detailFields as $idx=>$field)
                    <div role="tabpanel" class="tab-pane active" id="{{$field}}-detail-{{$model->id}}">
                        <button class="btn btn-info" style="position: absolute; margin-right: 60px; z-index: 10; right: 0px; margin-top: 6px;"
                            onclick="copyToClipboard(this)"
                            data-target="#json-value-{{$idx}}"> copy
                        </button>
                        <pre id="json-value-{{$idx}}" style="position: relative; white-space:pre-wrap">{{ json_encode($model->$field, JSON_PRETTY_PRINT)}}</pre>
                    </div>
                    @endforeach
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script type="text/javascript">
    const copyToClipboard = (button) => {
        const elem = document.querySelector(button.dataset.target);
        const str = elem.innerHTML;
        const el = document.createElement('textarea');  // Create a <textarea> element
        el.value = str;                                 // Set its value to the string that you want copied
        el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
        el.style.position = 'absolute';
        el.style.left = '-9999px';                      // Move outside the screen to make it invisible
        document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
        const selected =
            document.getSelection().rangeCount > 0        // Check if there is any content selected previously
            ? document.getSelection().getRangeAt(0)     // Store selection if found
            : false;                                    // Mark as false to know no selection existed before
        el.select();                                    // Select the <textarea> content
        document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
        const originalText = button.innerText;
        button.innerText = 'copied!';
        button.classList.add('btn-disabled');
        button.disabled = true;
        setTimeout(() => {
            button.innerText = originalText
            button.classList.remove('btn-disabled');
            button.disabled = false;
        }, 5000);
        document.body.removeChild(el);                  // Remove the <textarea> element
        if (selected) {                                 // If a selection existed before copying
            document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
            document.getSelection().addRange(selected);   // Restore the original selection
        }
    };
</script>
