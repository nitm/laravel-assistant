@extends('voyager::master')

@section('content')
    <section class="content-header">
        <h1>
            Assistant
        </h1>
    </section>
    <div class="page-content container-fluid browse">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('assistant::assistant_activities.show_fields')
                    <a href="{!! route('voyager.assistant-activity.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
