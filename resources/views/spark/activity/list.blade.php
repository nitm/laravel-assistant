<nitm-laravel-assistant-activity-list inline-template>
    <div>
        <!-- Activity controls -->
        <b-card title="Filter">
            <b-row>
                <b-col md="12" class="my-1">
                    <b-input-group prepend="Search">
                        <b-form-input v-model="filter" placeholder="Type to Search" />
                        <b-btn slot="append" :disabled="!filter" @click="filter = ''">Clear</b-btn>
                    </b-input-group>
                </b-col>
                <b-col md="9" class="my-1">
                    <b-input-group prepend="Sort">
                        <b-form-select v-model="sortBy" :options="sortOptions">
                            <option slot="first" :value="null">-- none --</option>
                        </b-form-select>
                        <b-form-select :disabled="!sortBy" v-model="sortDesc" slot="append">
                            <option :value="false">Asc</option>
                            <option :value="true">Desc</option>
                        </b-form-select>
                    </b-input-group>
                </b-col>
                <b-col class="my-1">
                            <b-input-group prepend="Per Page">
                                <b-form-select :options="pageOptions" v-model="perPage" />
                            </b-input-group>
                </b-col>
            </b-row>
        </b-card>
        <hr/>
        <!-- Activity -->
        <div class="table-responsive" v-if="hasAny">
            <b-table :items="items.data"
                :fields="fields"
                :current-page="currentPage"
                :per-page="perPage"
                :filter="filter"
                :sort-by.sync="sortBy"
                :sort-desc.sync="sortDesc"
                :sort-direction="sortDirection"
                @filtered="onFiltered">
                <template slot="actions" slot-scope="row">
                    <a :href="'/assistant/activity'+row.item.id+'/edit'">
                        <b-button class="btn btn-outline-primary">
                            <i class="fa fa-cog"></i>
                        </b-button>
                    </a>
                </template>
            </b-table>
        </div>
        <div v-else>
            <p class="alert alert-disabled">No assistant activity yet...</p>
        </div>
    </div>
</nitm-laravel-assistant-activity-list>