<!-- Tabs -->
<aside>
    <h3 class="nav-heading ">
        {{__('Activity')}}
    </h3>
    <ul class="nav flex-column mb-4" role="tablist">
        <li class="nav-item">
            <a class="nav-link {{ in_array(Route::currentRouteName(), ['assistant.activity.index']) ? 'active' : ''}}" id="assistant-activity-list-tab" href="#assistant-activity-list" aria-controls="assistant-activity-list" role="tab" data-toggle="tab" aria-selected="true">
                <i class="fa fa-fw text-left fa-btn fa-list"></i> {{__('List')}}
            </a>
        </li>
    </ul>
</aside>