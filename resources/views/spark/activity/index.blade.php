@extends('spark::layouts.app')

@section('content')
    <nitm-laravel-assistant-activity-index :user="user" inline-template>
        <div class="spark-screen container">
            <div class="row">
                <div class="col-md-3 col-lg-2 spark-settings-tabs">
                    @include('assistant::activity.menu')
                </div>
                <!-- Tab cards -->
                <div class="col-md-9 col-lg-10 tab-content" id="assistant-activity-tab-content">
                    <!-- List -->
                    <div role="tabpanel" class="tab-pane fade {{ in_array(Route::currentRouteName(), ['activity.index']) ? 'show active' : ''}}" id="assistant-activity-list" aria-labelled-by="assistant-activity-list-tab">
                        @include('assistant::activity.list')
                    </div>
                </div>
            </div>
        </div>
    </nitm-laravel-assistant-activity-index>
@endsection