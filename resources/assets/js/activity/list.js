Vue.component('nitm-laravel-assistant-activity-list', {
    data() {
        return {
            items: {},
            filter: null,
            fields: [{
                    key: 'user.name',
                    label: "User",
                    sortable: false
                },
                {
                    key: 'verb',
                    label: 'Action',
                    sortable: true
                },
                {
                    key: 'remote_type',
                    label: 'Remote Type',
                    sortable: true
                },
                {
                    key: 'remote_id',
                    label: 'Remote Id',
                },
                {
                    key: 'intent_action',
                    label: 'Intent Action',
                    sortable: true
                },
                {
                    key: 'needs_handoff',
                    label: 'Handed Off?',
                    sortable: true
                },
                {
                    key: 'created_at',
                    label: 'Created',
                    sortable: true
                }
            ],
            currentPage: 1,
            perPage: 20,
            pageOptions: [ 20, 50, 100 ],
            sortBy: null,
            sortDesc: false,
            sortDirection: 'asc',
        }
    },

    methods: {
        /**
         * Get the current items.
         */
        getItems() {
            axios.get('/assistant/activity/all')
                .then(response => {
                    this.items = response.data;
                    this.filter = this.filter || this.items.filter;
                    console.log("Laravel-Assistant: Activity", this.items);
                });
        },
        totalRows() {
            return this.items ? this.items.total : 0;
        },
        onFiltered (filteredItems) {
          // Trigger pagination to update the number of buttons/pages due to filtering
          this.totalRows = filteredItems.length
          this.currentPage = 1
        }
    },

    computed: {
        hasAny: function () {
            return this.items.data && this.items.data.length >= 1
        },
        sortOptions () {
          // Create an options list from our fields
          return this.fields
            .filter(f => f.sortable)
            .map(f => { return { text: f.label, value: f.key } })
        }
    },

    mounted() {
        console.log("Laravel-Assistant: Mounted");
        //
        this.getItems();
    }
});
